package com.rivues.module.jobs.web.handler;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.rivues.module.platform.web.handler.Handler;
import com.rivues.module.platform.web.interceptor.LogIntercreptorHandler;
  
@Controller  
@RequestMapping("/{orgi}/jobs")  
public class JobsController extends Handler{  
	private final Logger log = LoggerFactory.getLogger(LogIntercreptorHandler.class); 
    @RequestMapping(value="/index" , name="index" , type="jobs",subtype="welcome")
    public ModelAndView index(HttpServletRequest request , @PathVariable String orgi) throws Exception{  
    	return request(super.createManageResponse("/pages/jobs/index") , orgi) ;
    }
} 