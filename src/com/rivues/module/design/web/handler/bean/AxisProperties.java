package com.rivues.module.design.web.handler.bean;

public class AxisProperties {
	private String xname;//x轴名称
	private String[] dimensions;//维度
	private String[] measures;//指标
	public String getXname() {
		return xname;
	}
	public void setXname(String xname) {
		this.xname = xname;
	}
	public String[] getDimensions() {
		return dimensions;
	}
	public void setDimensions(String[] dimensions) {
		this.dimensions = dimensions;
	}
	public String[] getMeasures() {
		return measures;
	}
	public void setMeasures(String[] measures) {
		this.measures = measures;
	}
}
