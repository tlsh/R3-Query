package com.rivues.module.platform.web.exception;

public class BusinessException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7459728088056500702L;
	
	public BusinessException(String msg,Throwable t){
		super(msg,t);
	}
	
}
