package com.rivues.module.platform.web.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name = "rivu5_monitoraccessstatistics")
@org.hibernate.annotations.Proxy(lazy = false)
public class MonitorAccessStatistics implements Serializable{
	private String id;
	private String name;
	private String code;
	private String orgi ; 
	
	private Date createtime;//创建时间
	private String publishtype;//报表/模型发布
	private String publishstatus = "1";//发布状态（1表示未发布  2表示已发布）
	private String reporttype; //个人报表/公用报表
	private String status; //隐藏hide：1/禁用 disable：0(报表，模型)
	private long totalproducts; //总访问量 
	private int minproducts;  //最少访问量
	private int maxproducts;//最大访问量 
	private long avgaccesstime;//平均访问时间（秒）
	private int accesserrornum;//访问错误次数

	private int calculatenum;//结算个数(已结算:1/结算失败:0)
	private int modeldbnum;//模型数据库个数
	
	private int reportms1;//0-1 ms
	private int reportms2;//1-10 ms
	private int reportms3;//10-100 ms
	private int reports1;//100ms-1s
	private int reports2;//1-10 s
	private int reports3;//10-100 s
	private int reports4;//100-1000 s
	private int reports5;// >1000 s
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getCalculatenum() {
		return calculatenum;
	}
	public void setCalculatenum(int calculatenum) {
		this.calculatenum = calculatenum;
	}
	public long getTotalproducts() {
		return totalproducts;
	}
	public void setTotalproducts(long totalproducts) {
		this.totalproducts = totalproducts;
	}
	public int getMinproducts() {
		return minproducts;
	}
	public void setMinproducts(int minproducts) {
		this.minproducts = minproducts;
	}
	public int getMaxproducts() {
		return maxproducts;
	}
	public void setMaxproducts(int maxproducts) {
		this.maxproducts = maxproducts;
	}
	public long getAvgaccesstime() {
		return avgaccesstime;
	}
	public void setAvgaccesstime(long avgaccesstime) {
		this.avgaccesstime = avgaccesstime;
	}
	public int getAccesserrornum() {
		return accesserrornum;
	}
	public void setAccesserrornum(int accesserrornum) {
		this.accesserrornum = accesserrornum;
	}
	public int getModeldbnum() {
		return modeldbnum;
	}
	public void setModeldbnum(int modeldbnum) {
		this.modeldbnum = modeldbnum;
	}
	public String getPublishtype() {
		return publishtype;
	}
	public void setPublishtype(String publishtype) {
		this.publishtype = publishtype;
	}
	public String getReporttype() {
		return reporttype;
	}
	public void setReporttype(String reporttype) {
		this.reporttype = reporttype;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public int getReportms1() {
		return reportms1;
	}
	public void setReportms1(int reportms1) {
		this.reportms1 = reportms1;
	}
	public int getReportms2() {
		return reportms2;
	}
	public void setReportms2(int reportms2) {
		this.reportms2 = reportms2;
	}
	public int getReportms3() {
		return reportms3;
	}
	public void setReportms3(int reportms3) {
		this.reportms3 = reportms3;
	}
	public int getReports1() {
		return reports1;
	}
	public void setReports1(int reports1) {
		this.reports1 = reports1;
	}
	public int getReports2() {
		return reports2;
	}
	public void setReports2(int reports2) {
		this.reports2 = reports2;
	}
	public int getReports3() {
		return reports3;
	}
	public void setReports3(int reports3) {
		this.reports3 = reports3;
	}
	public int getReports4() {
		return reports4;
	}
	public void setReports4(int reports4) {
		this.reports4 = reports4;
	}
	public int getReports5() {
		return reports5;
	}
	public void setReports5(int reports5) {
		this.reports5 = reports5;
	}
	public String getPublishstatus() {
		return publishstatus;
	}
	public void setPublishstatus(String publishstatus) {
		this.publishstatus = publishstatus;
	}
}
