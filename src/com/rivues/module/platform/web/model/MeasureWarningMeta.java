package com.rivues.module.platform.web.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "rivu5_measurewarningmeta")
@org.hibernate.annotations.Proxy(lazy = false)
public class MeasureWarningMeta implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id ;
	private String modelid;
	private String dataid ;
	private String dataname ; 	//指标名称显示报表的名称  判断报表列数据是否为预警数据 
	private String paramtype ;
	private double regionmin;
	private double regionmax;
	private String tdstylevar;
	private String html;
	private String imgclass ;
	private String orgi ;
	private String reportid ;
	private String flag;//标志版本    0:old   1:new
	private Set<MeasureRuleData> measureRuleData;
	
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getModelid() {
		return modelid;
	}
	public void setModelid(String modelid) {
		this.modelid = modelid;
	}
	public String getDataid() {
		return dataid;
	}
	public void setDataid(String dataid) {
		this.dataid = dataid;
	}
	public String getParamtype() {
		return paramtype;
	}
	public void setParamtype(String paramtype) {
		this.paramtype = paramtype;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getReportid() {
		return reportid;
	}
	public void setReportid(String reportid) {
		this.reportid = reportid;
	}
	public String getDataname() {
		return dataname;
	}
	public void setDataname(String dataname) {
		this.dataname = dataname;
	}
	public double getRegionmin() {
		return regionmin;
	}
	public void setRegionmin(double regionmin) {
		this.regionmin = regionmin;
	}
	public double getRegionmax() {
		return regionmax;
	}
	public void setRegionmax(double regionmax) {
		this.regionmax = regionmax;
	}
	public String getHtml() {
		return html;
	}
	public void setHtml(String html) {
		this.html = html;
	}
	public String getImgclass() {
		return imgclass;
	}
	public void setImgclass(String imgclass) {
		this.imgclass = imgclass;
	}
	public String getTdstylevar() {
		return tdstylevar;
	}
	public void setTdstylevar(String tdstylevar) {
		this.tdstylevar = tdstylevar;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "warningId")
	public Set<MeasureRuleData> getMeasureRuleData() {
		return measureRuleData;
	}
	public void setMeasureRuleData(Set<MeasureRuleData> measureRuleData) {
		this.measureRuleData = measureRuleData;
	}
	
	
}
