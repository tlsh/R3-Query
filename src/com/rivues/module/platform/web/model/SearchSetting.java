package com.rivues.module.platform.web.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "rivu5_searchsetting")
@org.hibernate.annotations.Proxy(lazy = false)
public class SearchSetting  implements java.io.Serializable{
	 
	private static final long serialVersionUID = 1L;
	private String id                    ;
	private int    startdelay            ;//预警次数
	private String moreparam             ;
	private String hightlight            ;
	private String fieldmaxlength        ;
	private String hlsnippets            ;//阀值类型
	private String hlhtmlstart           ;//阀值
	private String hlhtmlend             ;//阀值
	private String facet                 ;
	private String defaultpagesize       ;
	private String defaultoperator       ;
	private String dateformat            ;
	private int    uploadimagesize       ;
	private int    uploadjarsize         ;
	private String defaultuploadimagepath;
	private String defaultuploadjarpath  ;
	private String defaulttempletpath    ;
	private String facttemplet           ;
	private String relatedtemplet        ;
	private String factsystemfield       ;
	private String sortsystemfield       ;
	private String sorttemplet           ;
	private String spellchecktemplet     ;
	private String spellcheck            ;//预警复选框是否选中
	private String suggestareatemplet    ;
	private String sitename              ;//预警项
	private String logonimagepath        ;
	private String sitedesc              ;//预警频率(时、日、月、年)
	private String copyright             ;
	private String userwelcometemplet    ;
	private String searchboxtemplet      ;
	private String factsearchquery       ;
	private String factdate              ;
	private String facetdategap          ;
	private String siteemail             ;//预警的发送方式
	private String searchtype            ;//预警级别
	private String defaulttermsfield     ;
	private String orgi                  ;//orgi
	private String localserver           ;
	private String r3cloudserver         ;//预警服务器(IP地址)

	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getStartdelay() {
		return startdelay;
	}
	public void setStartdelay(int startdelay) {
		this.startdelay = startdelay;
	}
	public String getMoreparam() {
		return moreparam;
	}
	public void setMoreparam(String moreparam) {
		this.moreparam = moreparam;
	}
	public String getHightlight() {
		return hightlight;
	}
	public void setHightlight(String hightlight) {
		this.hightlight = hightlight;
	}
	public String getFieldmaxlength() {
		return fieldmaxlength;
	}
	public void setFieldmaxlength(String fieldmaxlength) {
		this.fieldmaxlength = fieldmaxlength;
	}
	public String getHlsnippets() {
		return hlsnippets;
	}
	public void setHlsnippets(String hlsnippets) {
		this.hlsnippets = hlsnippets;
	}
	public String getHlhtmlstart() {
		return hlhtmlstart;
	}
	public void setHlhtmlstart(String hlhtmlstart) {
		this.hlhtmlstart = hlhtmlstart;
	}
	public String getHlhtmlend() {
		return hlhtmlend;
	}
	public void setHlhtmlend(String hlhtmlend) {
		this.hlhtmlend = hlhtmlend;
	}
	public String getFacet() {
		return facet;
	}
	public void setFacet(String facet) {
		this.facet = facet;
	}
	public String getDefaultpagesize() {
		return defaultpagesize;
	}
	public void setDefaultpagesize(String defaultpagesize) {
		this.defaultpagesize = defaultpagesize;
	}
	public String getDefaultoperator() {
		return defaultoperator;
	}
	public void setDefaultoperator(String defaultoperator) {
		this.defaultoperator = defaultoperator;
	}
	public String getDateformat() {
		return dateformat;
	}
	public void setDateformat(String dateformat) {
		this.dateformat = dateformat;
	}
	public int getUploadimagesize() {
		return uploadimagesize;
	}
	public void setUploadimagesize(int uploadimagesize) {
		this.uploadimagesize = uploadimagesize;
	}
	public int getUploadjarsize() {
		return uploadjarsize;
	}
	public void setUploadjarsize(int uploadjarsize) {
		this.uploadjarsize = uploadjarsize;
	}
	public String getDefaultuploadimagepath() {
		return defaultuploadimagepath;
	}
	public void setDefaultuploadimagepath(String defaultuploadimagepath) {
		this.defaultuploadimagepath = defaultuploadimagepath;
	}
	public String getDefaultuploadjarpath() {
		return defaultuploadjarpath;
	}
	public void setDefaultuploadjarpath(String defaultuploadjarpath) {
		this.defaultuploadjarpath = defaultuploadjarpath;
	}
	public String getDefaulttempletpath() {
		return defaulttempletpath;
	}
	public void setDefaulttempletpath(String defaulttempletpath) {
		this.defaulttempletpath = defaulttempletpath;
	}
	public String getFacttemplet() {
		return facttemplet;
	}
	public void setFacttemplet(String facttemplet) {
		this.facttemplet = facttemplet;
	}
	public String getRelatedtemplet() {
		return relatedtemplet;
	}
	public void setRelatedtemplet(String relatedtemplet) {
		this.relatedtemplet = relatedtemplet;
	}
	public String getFactsystemfield() {
		return factsystemfield;
	}
	public void setFactsystemfield(String factsystemfield) {
		this.factsystemfield = factsystemfield;
	}
	public String getSortsystemfield() {
		return sortsystemfield;
	}
	public void setSortsystemfield(String sortsystemfield) {
		this.sortsystemfield = sortsystemfield;
	}
	public String getSorttemplet() {
		return sorttemplet;
	}
	public void setSorttemplet(String sorttemplet) {
		this.sorttemplet = sorttemplet;
	}
	public String getSpellchecktemplet() {
		return spellchecktemplet;
	}
	public void setSpellchecktemplet(String spellchecktemplet) {
		this.spellchecktemplet = spellchecktemplet;
	}
	public String getSpellcheck() {
		return spellcheck;
	}
	public void setSpellcheck(String spellcheck) {
		this.spellcheck = spellcheck;
	}
	public String getSuggestareatemplet() {
		return suggestareatemplet;
	}
	public void setSuggestareatemplet(String suggestareatemplet) {
		this.suggestareatemplet = suggestareatemplet;
	}
	public String getSitename() {
		return sitename;
	}
	public void setSitename(String sitename) {
		this.sitename = sitename;
	}
	public String getLogonimagepath() {
		return logonimagepath;
	}
	public void setLogonimagepath(String logonimagepath) {
		this.logonimagepath = logonimagepath;
	}
	public String getSitedesc() {
		return sitedesc;
	}
	public void setSitedesc(String sitedesc) {
		this.sitedesc = sitedesc;
	}
	public String getCopyright() {
		return copyright;
	}
	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}
	public String getUserwelcometemplet() {
		return userwelcometemplet;
	}
	public void setUserwelcometemplet(String userwelcometemplet) {
		this.userwelcometemplet = userwelcometemplet;
	}
	public String getSearchboxtemplet() {
		return searchboxtemplet;
	}
	public void setSearchboxtemplet(String searchboxtemplet) {
		this.searchboxtemplet = searchboxtemplet;
	}
	public String getFactsearchquery() {
		return factsearchquery;
	}
	public void setFactsearchquery(String factsearchquery) {
		this.factsearchquery = factsearchquery;
	}
	public String getFactdate() {
		return factdate;
	}
	public void setFactdate(String factdate) {
		this.factdate = factdate;
	}
	public String getFacetdategap() {
		return facetdategap;
	}
	public void setFacetdategap(String facetdategap) {
		this.facetdategap = facetdategap;
	}
	public String getSiteemail() {
		return siteemail;
	}
	public void setSiteemail(String siteemail) {
		this.siteemail = siteemail;
	}
	public String getSearchtype() {
		return searchtype;
	}
	public void setSearchtype(String searchtype) {
		this.searchtype = searchtype;
	}
	public String getDefaulttermsfield() {
		return defaulttermsfield;
	}
	public void setDefaulttermsfield(String defaulttermsfield) {
		this.defaulttermsfield = defaulttermsfield;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getLocalserver() {
		return localserver;
	}
	public void setLocalserver(String localserver) {
		this.localserver = localserver;
	}
	public String getR3cloudserver() {
		return r3cloudserver;
	}
	public void setR3cloudserver(String r3cloudserver) {
		this.r3cloudserver = r3cloudserver;
	}
	
}
