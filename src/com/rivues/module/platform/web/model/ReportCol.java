package com.rivues.module.platform.web.model;

public class ReportCol implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1072808550826849939L;
	private String type ="colrow" ;
	private String id ;
	private String formatstr  =  "###" ;
	private String name ;
	private boolean group ;
	private String grouptype ;
	private String groupdim ;
	private String express ;
	private boolean posdef ;	//插入的列位置自定义
	private String colpos ; 	//插入列的位置 标题
	private String colpositem ;	//插入列的下级 子项之后
	private String colpostype ; //列的插入模式 ， 当前列插入还是下级列插入
	private boolean colgroup ; //取值方式 是组内还是 所有列
	
	private String style ;
	private int rowindex ;
	private String coltype;
	public ReportCol(){}
	public ReportCol(String name){
		this.name = name;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getExpress() {
		return express;
	}
	public void setExpress(String express) {
		this.express = express;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFormatstr() {
		return formatstr;
	}
	public void setFormatstr(String formatstr) {
		this.formatstr = formatstr;
	}
	public boolean isGroup() {
		return group;
	}
	public void setGroup(boolean group) {
		this.group = group;
	}
	public String getGrouptype() {
		return grouptype;
	}
	public void setGrouptype(String grouptype) {
		this.grouptype = grouptype;
	}
	public String getGroupdim() {
		return groupdim;
	}
	public void setGroupdim(String groupdim) {
		this.groupdim = groupdim;
	}
	public int getRowindex() {
		return rowindex;
	}
	public void setRowindex(int rowindex) {
		this.rowindex = rowindex;
	}
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public String getColtype() {
		return coltype;
	}
	public void setColtype(String coltype) {
		this.coltype = coltype;
	}
	public boolean isPosdef() {
		return posdef;
	}
	public void setPosdef(boolean posdef) {
		this.posdef = posdef;
	}
	public String getColpos() {
		return colpos;
	}
	public void setColpos(String colpos) {
		this.colpos = colpos;
	}
	public String getColpostype() {
		return colpostype;
	}
	public void setColpostype(String colpostype) {
		this.colpostype = colpostype;
	}
	public String getColpositem() {
		return colpositem;
	}
	public void setColpositem(String colpositem) {
		this.colpositem = colpositem;
	}
	public boolean isColgroup() {
		return colgroup;
	}
	public void setColgroup(boolean colgroup) {
		this.colgroup = colgroup;
	}
}
