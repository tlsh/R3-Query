/**
 * Licensed to the Rivulet under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *     webapps/LICENSE-Rivulet-1.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.rivues.module.platform.web.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * @author jaddy0302 Rivulet Blacklist.java 2010-3-17
 * //改变用处，此表功能为保存 报表授权信息
 */
@Entity
@Table(name = "rivu5_templetmapping")
@org.hibernate.annotations.Proxy(lazy = false)
public class TempletMapping implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5808307365882240027L;
	private String id ;
	private String taskid ;					//改变用处， 用于报表授权，此字段功能为 保存 角色 或用户ID
	private String listblockid ;			//改变用处，用于报表授权，此字段功能为保存报表ID
	private String previewtempletid ;		//改变用处，用于报表授权，此字段功能为保存授权类型
	public TempletMapping(){}
	/**
	 * 
	 * @param taskid
	 * @param listblockid
	 * @param previewtempletid
	 */
	public TempletMapping(String taskid , String listblockid , String previewtempletid){
		this.taskid = taskid ;
		this.listblockid = listblockid ;
		this.previewtempletid = previewtempletid ;
	}
	/**
	 * @return the id
	 */
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")	
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	public String getTaskid() {
		return taskid;
	}
	public void setTaskid(String taskid) {
		this.taskid = taskid;
	}
	public String getListblockid() {
		return listblockid;
	}
	public void setListblockid(String listblockid) {
		this.listblockid = listblockid;
	}
	public String getPreviewtempletid() {
		return previewtempletid;
	}
	public void setPreviewtempletid(String previewtempletid) {
		this.previewtempletid = previewtempletid;
	}
		
	
}
