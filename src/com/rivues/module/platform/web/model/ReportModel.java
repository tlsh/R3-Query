package com.rivues.module.platform.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "rivu5_analyzereportmodel")
@org.hibernate.annotations.Proxy(lazy = false)
public class ReportModel implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id ;
	private String name ;
	private String code ;
	private String reportid ;
	private String cube;
	private String modeltype ;
	private int sortindex ;
	private String stylestr;
	private String labeltext ;
	private String cssclassname ;
	private String mposleft;	//指标位置
	private String mpostop ;	//指标位置
	private String title ;
	private boolean exchangerw = false ;	//行列转换
	private String rowdimension ;	//逗号分隔
	private String coldimension ;	//逗号分隔
	private String measure ;		//逗号分隔
	private String dstype ;		//数据源类型			, 修改用处，用于存放 publishedcubeid
	private String orgi ;
	private String objectid ;
	private Date createtime ;
	
	
	
	/**
	 * @return the id
	 */
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getReportid() {
		return reportid;
	}
	public void setReportid(String reportid) {
		this.reportid = reportid;
	}
	public String getModeltype() {
		return modeltype;
	}
	public void setModeltype(String modeltype) {
		this.modeltype = modeltype;
	}
	public int getSortindex() {
		return sortindex;
	}
	public void setSortindex(int sortindex) {
		this.sortindex = sortindex;
	}
	public String getStylestr() {
		return stylestr;
	}
	public void setStylestr(String stylestr) {
		this.stylestr = stylestr;
	}
	public String getLabeltext() {
		return labeltext;
	}
	public void setLabeltext(String labeltext) {
		this.labeltext = labeltext;
	}
	public String getCssclassname() {
		return cssclassname;
	}
	public void setCssclassname(String cssclassname) {
		this.cssclassname = cssclassname;
	}
	public String getMposleft() {
		return mposleft;
	}
	public void setMposleft(String mposleft) {
		this.mposleft = mposleft;
	}
	public String getMpostop() {
		return mpostop;
	}
	public void setMpostop(String mpostop) {
		this.mpostop = mpostop;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public boolean isExchangerw() {
		return exchangerw;
	}
	public void setExchangerw(boolean exchangerw) {
		this.exchangerw = exchangerw;
	}
	public String getRowdimension() {
		return rowdimension;
	}
	public void setRowdimension(String rowdimension) {
		this.rowdimension = rowdimension;
	}
	public String getColdimension() {
		return coldimension;
	}
	public void setColdimension(String coldimension) {
		this.coldimension = coldimension;
	}
	public String getMeasure() {
		return measure;
	}
	public void setMeasure(String measure) {
		this.measure = measure;
	}
	public String getDstype() {
		return dstype;
	}
	public void setDstype(String dstype) {
		this.dstype = dstype;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getObjectid() {
		return objectid;
	}
	public void setObjectid(String objectid) {
		this.objectid = objectid;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	
	public String getCube() {
		return cube;
	}
	public void setCube(String cube) {
		this.cube = cube;
	}
}