package com.rivues.module.platform.web.model;

import java.util.ArrayList;
import java.util.List;

import com.rivues.module.platform.web.model.chart.DataSeries;

public class ChartProperties implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5895269406109068301L;
	
	private String chartype ;
	private String id ;
	private String xaxis ;			//如果支持分组，则 xaxis显示为 X轴数据，例如：xAxis: {
									//   											 categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
									//										   }
	private String xaxis_type ;
	private String xaxis_title ;
	private String xaxis_label_rotation ;
	private String xaxis_label_fontstyle ;
	private String xaxis_label_fontfamily ;
	
	private String yaxis_title ;
	private double yaxis_min = 0;
	
	private boolean legen ;
	private boolean credits ;
	private boolean exporting;
	
	private boolean useseries ; 	//是否分组
	private List<DataSeries> series = new ArrayList<DataSeries>() ;		//Y轴数据
	private List<DataSeries> yaxis = new ArrayList<DataSeries>();
	public String getChartype() {
		return chartype;
	}
	public void setChartype(String chartype) {
		this.chartype = chartype;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getXaxis() {
		return xaxis;
	}
	public void setXaxis(String xaxis) {
		this.xaxis = xaxis;
	}
	public String getXaxis_type() {
		return xaxis_type;
	}
	public void setXaxis_type(String xaxis_type) {
		this.xaxis_type = xaxis_type;
	}
	public String getXaxis_title() {
		return xaxis_title;
	}
	public void setXaxis_title(String xaxis_title) {
		this.xaxis_title = xaxis_title;
	}
	public String getXaxis_label_rotation() {
		return xaxis_label_rotation;
	}
	public void setXaxis_label_rotation(String xaxis_label_rotation) {
		this.xaxis_label_rotation = xaxis_label_rotation;
	}
	public String getXaxis_label_fontstyle() {
		return xaxis_label_fontstyle;
	}
	public void setXaxis_label_fontstyle(String xaxis_label_fontstyle) {
		this.xaxis_label_fontstyle = xaxis_label_fontstyle;
	}
	public String getXaxis_label_fontfamily() {
		return xaxis_label_fontfamily;
	}
	public void setXaxis_label_fontfamily(String xaxis_label_fontfamily) {
		this.xaxis_label_fontfamily = xaxis_label_fontfamily;
	}
	public boolean isLegen() {
		return legen;
	}
	public void setLegen(boolean legen) {
		this.legen = legen;
	}
	public boolean isCredits() {
		return credits;
	}
	public void setCredits(boolean credits) {
		this.credits = credits;
	}
	public boolean isExporting() {
		return exporting;
	}
	public void setExporting(boolean exporting) {
		this.exporting = exporting;
	}
	public boolean isUseseries() {
		return useseries;
	}
	public void setUseseries(boolean useseries) {
		this.useseries = useseries;
	}
	public List<DataSeries> getSeries() {
		return series;
	}
	public void setSeries(List<DataSeries> series) {
		this.series = series;
	}
	
	public List<DataSeries> getYaxis() {
		return yaxis!=null ? yaxis : (yaxis = new ArrayList<DataSeries>());
	}
	public void setYaxis(List<DataSeries> yaxis) {
		this.yaxis = yaxis;
	}
	public String getYaxis_title() {
		return yaxis_title;
	}
	public void setYaxis_title(String yaxis_title) {
		this.yaxis_title = yaxis_title;
	}
	public double getYaxis_min() {
		return yaxis_min;
	}
	public void setYaxis_min(double yaxis_min) {
		this.yaxis_min = yaxis_min;
	}
}
