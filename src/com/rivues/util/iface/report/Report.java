package com.rivues.util.iface.report;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import com.rivues.module.platform.web.model.AnalyzerReport;
import com.rivues.module.platform.web.model.DataDic;
import com.rivues.module.platform.web.model.User;
import com.rivues.module.report.web.model.PublishedReport;
import com.rivues.util.exception.DicExistEception;
import com.rivues.util.exception.DicNotExistException;
import com.rivues.util.exception.ReportException;

public interface Report extends java.io.Serializable{
	/**
	 * 根据目录获取该级目录下发布的所有报表列表
	 * @return
	 */
	public List<PublishedReport> getReportList(String dicid , String orgi , User user , String tabid,String reporttype) ;
	
	/**
	 * 根据目标id查询所有的相关联的报表
	 * @param targetReportid
	 * @param orgi
	 * @param tabid
	 * @param reporttype
	 * @return
	 */
	public List<PublishedReport> getReportList(String targetReportid , String orgi, String tabid,String reporttype) ;
	
	/**
	 * 
	 * @param dicid		目录ID
	 * @param type		搜索类型，在所有目录中搜索还是在当前目录中搜索
	 * @param name		搜索的关键词
	 * @param orgi		orgi
	 * @param user		当前登陆用户
	 * @param tabid		报表所在目录， 公共文件夹 /个人文件夹
	 * @param reporttype	//报表类型
	 * @param pritype		个人文件夹类型，我创建的报表，我保存的报表
	 * @return
	 */
	public List<PublishedReport> searchReportList(String dicid, String type , String name , String orgi , User user , String tabid,String reporttype , String pritype) ;
	
	
	/**
	 * 创建/发布 报表
	 * @param report
	 * @return
	 */
	public boolean createReport(PublishedReport report , User user) throws ReportException;
	
	
	/**
	 * 创建/发布 报表
	 * @param report
	 * @return
	 */
	public void putReport(PublishedReport report , User user) throws ReportException;
	
	/**
	 * 更新/发布 报表
	 * @param report
	 * @return
	 */
	public boolean updateReport(PublishedReport report, User user) throws ReportException;
	
	/**
	 * 根据报表ID获取报表
	 * @param user
	 * @param orgi
	 * @return
	 */
	public PublishedReport getReportByID(User user , String orgi , String reportid)throws ReportException ;
	
	
	/**
	 * 根据报表ID获取报表
	 * @param user
	 * @param orgi
	 * @return
	 */
	public boolean getReport(User user , String orgi , String reportid) ;
	
	/**
	 * 根据报表ID获取报表
	 * @param user
	 * @param orgi
	 * @return
	 */
	public PublishedReport getReportByID(User user , String orgi , String reportid , boolean userSessionID) ;
	
	
	/**
	 * 根据报表ID获取报表
	 * @param user
	 * @param orgi
	 * @return
	 */
	public PublishedReport getReportByName(User user , String orgi , String reportid) ;
	
	/**
	 * 根据目录ID删除目录
	 * @param user
	 * @param orgi
	 * @return
	 */
	public void rmDataDicByID(User user , String orgi , String dicid)  throws DicNotExistException ;
	
	
	public void rmReportByID(String reportId,User user , String orgi)  throws ReportException ;
	
	/**
	 * 获取所有的报表列表
	 * @return
	 */
	public List<DataDic> getReportDicList(String orgi , User user , String parentid , String tabid) ;
	
	/**
	 * 获取所有的报表列表
	 * @return
	 */
	public List<DataDic> getAllReportDicList(String orgi , User user , String tabid) ;
	
	/**
	 * 创建DataDic
	 */
	public void createReportDic(DataDic dataDic , User user) throws DicExistEception ;
	
	/**
	 * 修改Datadic
	 * @param dataDic
	 * @param user
	 * @throws DicExistEception
	 */
	public void updateReportDic(DataDic dataDic , User user) throws DicExistEception ;
	
	/**
	 * 根据ID获取DataDic
	 * @param id
	 * @return
	 * @throws DicNotExistException
	 */
	public DataDic getReportDic(String id , String orgi , User user) throws DicNotExistException;
	
	/**
	 * 获取数量，根据条件
	 * @param criteria
	 * @param user
	 * @return
	 */
	public int getCount(DetachedCriteria criteria,User user);
	
	/**
	 * 直接从数据库中获取
	 * @param user
	 * @param orgi
	 * @param reportid
	 * @return
	 */
	public PublishedReport getReportByIDFromDB(User user, String orgi , String reportid);
	
}
