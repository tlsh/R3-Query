package com.rivues.util.sms;

import java.io.Serializable;
import java.util.List;
/**
 * 短信发送接口
 * @author Administrator
 *
 */
public interface MessageSend extends Serializable {
	
	public void send() throws Exception;
	
	public void send(String receiver,String content) throws Exception;
	
	public void send(List<String> receivers,String content) throws Exception;
	
}
