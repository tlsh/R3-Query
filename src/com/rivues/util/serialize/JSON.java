/*
 * Copyright 1999-2101 Alibaba Group.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.rivues.util.serialize;

import java.beans.PropertyDescriptor;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.net.util.Base64;

import com.rivues.util.RivuTools;

/**
 * @author wenshao<szujobs@hotmail.com>
 */
public abstract class JSON {

	public static String DEFFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static int    DEFAULT_GENERATE_FEATURE;
    
    public static String DEFAULT_TYPE_KEY     = "@type";

    public static int    DEFAULT_PARSER_FEATURE;
    
    public static final String toJSONString(Object object) {
    	try{
	        byte[] data = RivuTools.toBytes(object) ;
	        Base64 base64 = new Base64();
	        return  base64.encodeToString(data);
    	}catch(Exception ce){
    		ce.printStackTrace();
    	}
    	return null;
    }
    
    public static final <T> T parseObject(String text, Class<T> clazz){
        try {
			return (T)RivuTools.toObject(new Base64().decode(text));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return null ;
    }
    

    public static final <T> List<T> parseArray(String text, Class<T> clazz) {
        return parseObject(text , List.class);
    }

}
