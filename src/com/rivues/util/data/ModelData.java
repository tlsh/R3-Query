package com.rivues.util.data;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.rivues.module.platform.web.handler.RequestData;
import com.rivues.module.platform.web.model.QueryText;
import com.rivues.util.RivuTools;

public class ModelData implements ReportData{
	private Exception exception ;
	private Level row ;
	private Level col ;
	private int pageSize ;
	private int page ;
	private List<List<ValueData>> data ;
	private List<List> docList ;
	private String viewData ;
	private RequestData reqdata;
	private QueryText queryText ;
	private Map<String , Object> options ;
	private long queryTime ;
	private long total ;
	private Date createTime;
	
	public long getQueryTime() {
		return queryTime;
	}
	public void setQueryTime(long queryTime) {
		this.queryTime = queryTime;
	}
	public Level getRow() {
		return row;
	}
	public void setRow(Level row) {
		this.row = row;
	}
	public Level getCol() {
		return col;
	}
	public void setCol(Level col) {
		this.col = col;
	}
	public List<List<ValueData>> getData() {
		return data;
	}
	public void setData(List<List<ValueData>> data) {
		this.data = data;
	}
	public String getViewData() {
		return viewData;
	}
	public void setViewData(String viewData) {
		this.viewData = viewData;
	}
	public ReportData clone(){
		try {
			return (ReportData) super.clone() ;
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null ;
	}
	public List<List> getDocList() {
		return docList;
	}
	public void setDocList(List<List> docList) {
		this.docList = docList;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	@Override
	public void exchangeColRow() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void merge(ReportData data) {
		
	}
	@Override
	public Date getDate() {
		// TODO Auto-generated method stub
		return createTime;
	}
	public Exception getException() {
		return exception;
	}
	public void setException(Exception exception) {
		this.exception = exception;
	}
	@Override
	public ReportData dataclone() {
		ReportData reportData = null ;
		try {
			reportData = (ReportData) RivuTools.toObject(RivuTools.toBytes(this)) ;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return reportData;
	}
	@Override
	public RequestData getRequestData() {
		return reqdata;
	}
	@Override
	public void setRequestData(RequestData data) {
         reqdata= data;		
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public QueryText getQueryText() {
		return queryText;
	}
	public void setQueryText(QueryText queryText) {
		this.queryText = queryText;
	}
	public Map<String, Object> getOptions() {
		return options;
	}
	public void setOptions(Map<String, Object> options) {
		this.options = options;
	}
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	@Override
	public String getFormatQueryText(){
		String formatSQL = null ;
		if(this.queryText!=null && this.queryText.getQueryText()!=null && this.queryText.getQueryText().length()>0){
			formatSQL = RivuTools.formatQuery(this.queryText.getQueryText()) ;
		}
		return formatSQL ;
	}
	@Override
	public void setDate(Date createtime) {
		// TODO Auto-generated method stub
		this.createTime = createtime;
	}
}
