package com.rivues.util.service.monitor;

import com.rivues.core.RivuDataContext;
import com.rivues.module.platform.web.model.Database;
import com.rivues.module.platform.web.model.JobDetail;
import com.rivues.module.platform.web.model.warning.EventBean;
import com.rivues.util.tools.RuntimeData;

public abstract class BusinessService{
	/**
	 * 
	 * @return
	 */
	public abstract String getName() ;
	
	public abstract void service() throws Exception;
	
	@SuppressWarnings("unchecked")
	public Database getDatabase(String id){
		return (Database) RivuDataContext.getService().getIObjectByPK(Database.class, id);
	}
	/**
	 * 构建 EventBean
	 * @param msg
	 * @param jobDetail
	 * @param msgCode
	 * @return
	 */
	@SuppressWarnings("static-access")
	public static EventBean createEventBean(String msg , JobDetail jobDetail , String msgCode){
		EventBean eventLog = new EventBean();
		RuntimeData runtimeData = new RuntimeData(); 
		eventLog.setHostname(runtimeData.getHostname()) ;
		eventLog.setIpaddr(runtimeData.getIpaddr()) ;
		eventLog.setGroupid("") ;
		eventLog.setDatatype(RivuDataContext.SystemInfoType.R3QUERY_V50_SERVER.toString());
		eventLog.setPort(runtimeData.getPort());
		eventLog.setEventype(RivuDataContext.EventType.REPORTJOB.toString()) ;
		eventLog.setGroupid(msgCode) ;
		eventLog.setEventmsg(msg);
		eventLog.setEventlevel(RivuDataContext.EventLevelType.INFORMATIONAL.toString()) ;
		eventLog.setDataid(jobDetail.getId()) ;
		eventLog.setDataname(jobDetail.getName());
		eventLog.setUserid(jobDetail.getUserid()) ;
		eventLog.setUsername(jobDetail.getUsername()) ;
		return eventLog ;
	}
}
