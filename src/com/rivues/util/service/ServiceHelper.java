package com.rivues.util.service;

import com.rivues.core.RivuDataContext;
import com.rivues.module.platform.web.model.Cube;
import com.rivues.util.datasource.DataSource;
import com.rivues.util.service.business.CubeService;
import com.rivues.util.service.business.DataSourceService;
import com.rivues.util.service.business.DatabaseService;
import com.rivues.util.service.business.ReportService;
import com.rivues.util.service.report.ReportDataFactory;

/**
 * 系统的业务服务，例如，报表服务，元数据服务，图表服务
 * @author admin
 *
 */
public class ServiceHelper {
	/**
	 * 服务类型枚举
	 * @author admin
	 *
	 */
	public enum BusinessServiceEnum{
		REPORT,
		CUBE,
		DATABASE,
		REPORTDATAFACTORY,
		DATASOURCE;
		public String toString(){
			return super.toString().toLowerCase();
		}
	}
	/**
	 * 获取ReportDataFactory，数据处理服务
	 * @return
	 */
	public static ReportDataFactory getReportDataFactoryService(){
		return RivuDataContext.getWebApplicationContext().getBean(BusinessServiceEnum.REPORTDATAFACTORY.toString() , ReportDataFactory.class) ;
	}
	/**
	 * 获取报表服务
	 * @return
	 */
	public static ReportService getReportService(){
		return RivuDataContext.getWebApplicationContext().getBean(BusinessServiceEnum.REPORT.toString() , ReportService.class) ;
	}
	
	/**
	 * 获取报表服务
	 * @return
	 */
	public static CubeService getCubeService(){
		return RivuDataContext.getWebApplicationContext().getBean(BusinessServiceEnum.CUBE.toString() , CubeService.class) ;
	}
	/**
	 * 获取数据库服务
	 * @return
	 */
	public static DatabaseService getDatabaseService(){
		return RivuDataContext.getWebApplicationContext().getBean(BusinessServiceEnum.DATABASE.toString() , DatabaseService.class) ;
	}
	
	/**
	 * 获取报表服务
	 * @return
	 */
	public static DataSourceService getDataSourceService(){
		return RivuDataContext.getWebApplicationContext().getBean(BusinessServiceEnum.DATASOURCE.toString() , DataSourceService.class) ;
	}
	/**
	 * 
	 * @param cube
	 * @return
	 */
	public static DataSource getDataModelService(Cube cube){
		if(cube!=null && RivuDataContext.CubeEnum.CUBE.toString().equals( cube.getDstype())){
			return getCubeService().service(cube) ;
		}else if(cube!=null && RivuDataContext.CubeEnum.TABLE.toString().equals( cube.getDstype())){
			return getDatabaseService().service(cube) ;
		}
		return null ;
	}
	
}
