package com.rivues.util.service.report;

import java.util.HashMap;
import java.util.Map;


import com.rivues.core.RivuDataContext;
import com.rivues.module.platform.web.model.Cube;
import com.rivues.util.datasource.DataSource;
import com.rivues.util.service.ServiceHelper;

public class DataSourceTools {
	private static Map<String , DataSource> dsMap = new HashMap<String , DataSource>() ;;
	/**
	 * 
	 * @param cube
	 * @return
	 */
	public static DataSource getDataSource(String ds , Cube cube){
		DataSource datasource = null;
		StringBuffer strb = new StringBuffer() ;
//		strb.append(cube.getDb()) ;
//		if(cube!=null){
//			strb.append(cube.getDb()).append(".").append(cube.getDstype()) ;
//		}
//		if((datasource = dsMap.get(strb.toString()))==null){
			if(cube!=null && ds.equals("cube")){
				if(RivuDataContext.CubeEnum.CUBE.toString().equals(cube.getDstype())){
					datasource = ServiceHelper.getCubeService().service(cube) ;
				}else if(RivuDataContext.CubeEnum.TABLE.toString().equals(cube.getDstype())){
					datasource = ServiceHelper.getDatabaseService().service(cube) ;
				}else{
					//r3
					//datasource = new R3DataTools();
				}
			}
//			if(datasource!=null){
//				dsMap.put(strb.toString(), datasource) ;
//			}
//		}
		return datasource ;
	}
}
