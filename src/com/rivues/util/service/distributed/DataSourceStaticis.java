package com.rivues.util.service.distributed;

import java.io.Serializable;
import java.util.concurrent.Callable;

import com.alibaba.druid.stat.DruidStatService;
import com.alibaba.druid.util.Utils;
import com.rivues.core.RivuDataContext;
import com.rivues.util.service.monitor.BusinessService;

public class DataSourceStaticis extends BusinessService  implements Callable<String>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8701258343752643874L;
	
	private String orgi ;
	private String service ;
	private int type ;
	/**
	 * 
	 * @param addr
	 * @param port
	 * @param orgi
	 * @param service
	 * @param type
	 */
	public DataSourceStaticis(String service , int type){
		this.service = service ;
		this.type = type ;
	}
	
	@Override
	public String call() throws Exception {
		return type==2? DruidStatService.getInstance().service(new StringBuffer().append("/").append(service).append(".json").toString()) : Utils.readFromResource(new StringBuffer().append("support/http/resources/").append(service).append(".html").toString());
	}
	/**
	 * 
	 * @throws Exception
	 */
	@Override
	public void service() throws Exception{
		call();
	}

	public String getOrgi() {
		return orgi;
	}

	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	@Override
	public String getName() {
		return RivuDataContext.ServiceTypeName.DISTRIBUTE_DATASOURCE.toString();
	}

}
