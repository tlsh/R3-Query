package com.rivues.util.service.distributed;

import java.io.Serializable;
import java.util.concurrent.Callable;

import com.alibaba.druid.stat.DruidStatService;
import com.alibaba.druid.util.Utils;
import com.rivues.core.RivuDataContext;
import com.rivues.util.service.monitor.BusinessService;

public class ServerControl extends BusinessService  implements Callable<String>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8701258343752643874L;
	
	private String orgi ;
	private int type ;
	/**
	 * 
	 * @param addr
	 * @param port
	 * @param orgi
	 * @param service
	 * @param type
	 */
	public ServerControl(int type){
		this.type = type ;
	}
	
	@Override
	public String call() throws Exception {
		if(type==2){//启动服务器
			
		}else if(type == 3){	//服务器关闭
			System.exit(1) ;
		}else if(type == 0){	//服务停止
			
		}else if(type == 1){	//服务启动
			
		}
		return "1";
	}
	/**
	 * 
	 * @throws Exception
	 */
	@Override
	public void service() throws Exception{
		call();
	}

	public String getOrgi() {
		return orgi;
	}

	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	@Override
	public String getName() {
		return RivuDataContext.ServiceTypeName.SERVER_CONTROL.toString();
	}

}
