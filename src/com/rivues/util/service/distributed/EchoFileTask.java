package com.rivues.util.service.distributed;

import java.io.File;
import java.io.Serializable;
import java.util.concurrent.Callable;

import com.rivues.util.RivuTools;
import com.rivues.util.tools.RuntimeData;

public class EchoFileTask implements Callable<String> , Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String fileName ;
	private String orgi ;
	public EchoFileTask(String fileName , String orgi){
		this.fileName = fileName ;
		this.orgi = orgi ;
	}
	@Override
	public String call() throws Exception {
		File exportFile = null ;
		if(this.fileName!=null){
			exportFile = RivuTools.getExportFile(orgi, fileName) ;
		}
		return (exportFile!=null  && RivuTools.getRunningJob(orgi, fileName) == null) ? new RuntimeData().getIpaddr():null;
	}
	

}
