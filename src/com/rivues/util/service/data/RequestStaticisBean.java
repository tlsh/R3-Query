package com.rivues.util.service.data;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import com.rivues.util.local.LocalTools;
import com.rivues.util.tools.RuntimeData;

public class RequestStaticisBean implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9040031133855527534L;
	private String name ;
	private AtomicInteger servicetimes = new AtomicInteger();	//服务次数
	private AtomicLong runningtime = new AtomicLong();	//运行时长
	private AtomicLong waittingtime = new AtomicLong();	//等待时长
	private String servicetype;	//服务类型：队列，线程，请求
	private AtomicInteger successrequest = new AtomicInteger() ;	//成功请求量
	private double successpercent ; //成功请求百分比
	private long successmaxtime ;
	private long successmintime ;
	private AtomicLong successtime = new AtomicLong(); 	//成功请求时长
	private double successavgtime ;	//成功请求的平均处理时间
	private int requestpersec;	//每秒处理量
	private AtomicInteger faildrequest = new AtomicInteger();	//失败处理量
	private double faildpercent;	//失败处理百分比
	private AtomicLong faildtime = new AtomicLong();		//失败处处理的时间
	private long faildmaxtime;
	private long faildmintime ;
	private long maxrequesttime;	//最大处理时间
	private long minrequesttime;	//最小处理时间
	
	private AtomicLong cacheNotFound = new AtomicLong();		//失败处处理的时间
	
	private Date starttime = new Date();
	private long uptime ;
	private RuntimeData runtimeData = new RuntimeData() ;
	private String ipaddr = runtimeData.getIpaddr();
	private String hostname = runtimeData.getHostname();
	public RequestStaticisBean(){}
	public String getRunningTime(){
		return new StringBuffer().append((int)Math.floor(getUptime()/(3600*24))).append(LocalTools.getMessage("I_MONITOR_1000001")).append((int)Math.floor((getUptime()%(3600*24))/3600)).append(LocalTools.getMessage("I_MONITOR_1000002")).append((int)Math.floor((getUptime()%(3600))/60)).append(LocalTools.getMessage("I_MONITOR_1000003")).append((int)Math.floor((getUptime()%(60)))).append(LocalTools.getMessage("I_MONITOR_1000004")).toString() ;
	}
	public RequestStaticisBean(String name){
		this.name = name ;
	}
	
	public void setServicetimes(AtomicInteger servicetimes) {
		this.servicetimes = servicetimes;
	}
	public void setRunningtime(AtomicLong runningtime) {
		this.runningtime = runningtime;
	}
	public void setWaittingtime(AtomicLong waittingtime) {
		this.waittingtime = waittingtime;
	}
	public void setSuccessrequest(AtomicInteger successrequest) {
		this.successrequest = successrequest;
	}
	public void setSuccesstime(AtomicLong successtime) {
		this.successtime = successtime;
	}
	public void setFaildtime(AtomicLong faildtime) {
		this.faildtime = faildtime;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getStarttime() {
		return starttime;
	}
	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}
	public int getServicetimes() {
		return servicetimes.intValue();
	}
	public void setServicetimes(int servicetimes) {
		this.servicetimes.addAndGet(servicetimes);
	}
	public long getRunningtime() {
		return runningtime.longValue();
	}
	public void setRunningtime(long runningtime) {
		this.runningtime.addAndGet(runningtime) ;
	}
	public long getWaittingtime() {
		return waittingtime.longValue();
	}
	public void setWaittingtime(long waittingtime) {
		this.waittingtime.incrementAndGet();
	}
	public String getServicetype() {
		return servicetype;
	}
	public void setServicetype(String servicetype) {
		this.servicetype = servicetype;
	}
	
	public int getSuccessrequest() {
		return successrequest.intValue();
	}
	public void setSuccessrequest(int successrequest) {
		this.successrequest.incrementAndGet();
	}
	public double getSuccesspercent() {
		return servicetimes.intValue()!=0 ? successrequest.intValue()*1.0d/servicetimes.intValue()*1.0d : 0;
	}
	public void setSuccesspercent(double successpercent) {
		this.successpercent = successpercent;
	}
	public long getSuccesstime() {
		return successtime.longValue();
	}
	public void setSuccesstime(long successtime) {
		this.successtime.addAndGet(successtime) ;
	}
	public double getSuccessavgtime() {
		return successrequest.intValue()!=0 ? (successtime.intValue()/1000)*1.0f/successrequest.intValue()*1.0f : 0;	//每次请求的平均处理时间
	}
	public void setSuccessavgtime(int successavgtime) {
		this.successavgtime = successavgtime;
	}
	public double getRequestpersec() {
		return successtime.intValue()!=0 ? successrequest.intValue()*1.0f / (successtime.intValue()/1000)*1.0f : 0; 	//每秒处理 的 请求数量  ， TPS
	}
	public void setRequestpersec(int requestpersec) {
		this.requestpersec = requestpersec;
	}
	public int getFaildrequest() {
		return faildrequest.intValue();
	}
	public void setFaildrequest(int faildrequest) {
		this.faildrequest.incrementAndGet();
	}
	public double getFaildpercent() {
		return servicetimes.intValue()!=0 ? faildrequest.intValue()*1.0f / servicetimes.intValue() * 1.0f : 0;
	}
	public void setFaildpercent(double faildpercent) {
		this.faildpercent = faildpercent;
	}
	public long getFaildtime() {
		return faildtime.longValue();
	}
	public void setFaildtime(long faildtime) {
		this.faildtime.addAndGet(faildtime);
	}
	public long getMaxrequesttime() {
		return maxrequesttime;
	}
	public void setMaxrequesttime(long maxrequesttime) {
		this.maxrequesttime = maxrequesttime;
	}
	public long getMinrequesttime() {
		return minrequesttime;
	}
	public void setMinrequesttime(long minrequesttime) {
		this.minrequesttime = minrequesttime;
	}
	public String getIpaddr() {
		return ipaddr;
	}
	public void setIpaddr(String ipaddr) {
		this.ipaddr = ipaddr;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public long getUptime() {
		return (System.currentTimeMillis() - starttime.getTime())/1000 ;
	}
	public void setUptime(long uptime) {
		this.uptime = uptime;
	}
	public long getSuccessmaxtime() {
		return successmaxtime;
	}
	public void setSuccessmaxtime(long successmaxtime) {
		this.successmaxtime = successmaxtime;
	}
	public long getSuccessmintime() {
		return successmintime;
	}
	public void setSuccessmintime(long successmintime) {
		this.successmintime = successmintime;
	}
	public long getFaildmaxtime() {
		return faildmaxtime;
	}
	public void setFaildmaxtime(long faildmaxtime) {
		this.faildmaxtime = faildmaxtime;
	}
	public long getFaildmintime() {
		return faildmintime;
	}
	public void setFaildmintime(long faildmintime) {
		this.faildmintime = faildmintime;
	}
	public long getCacheNotFound() {
		return cacheNotFound.longValue();
	}
	public void setCacheNotFound(long cacheNotFound) {
		this.cacheNotFound.addAndGet(cacheNotFound);
	}
	
	
}
