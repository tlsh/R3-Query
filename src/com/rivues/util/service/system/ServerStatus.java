package com.rivues.util.service.system;

public class ServerStatus implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5103876166861170546L;
	private String server ;
	private long times ;
	private int tasks ;
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public long getTimes() {
		return times;
	}
	public void setTimes(long times) {
		this.times = times;
	}
	public int getTasks() {
		return tasks;
	}
	public void setTasks(int tasks) {
		this.tasks = tasks;
	}
}
