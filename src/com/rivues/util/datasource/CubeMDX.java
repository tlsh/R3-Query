package com.rivues.util.datasource;

import java.util.ArrayList;
import java.util.List;

public class CubeMDX implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id ;
	private String modelid ;
	private String reportid;
	private List<String> rowcon = new ArrayList<String>();
	private List<String> colcon = new ArrayList<String>();
	private List<String> measure = new ArrayList<String>();
	private List<String> wherecon = new ArrayList<String>();
	private String mdxstr ;
	private List<String> withcon = new ArrayList<String>();
	public CubeMDX(){}
	

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getModelid() {
		return modelid;
	}


	public void setModelid(String modelid) {
		this.modelid = modelid;
	}


	public String getReportid() {
		return reportid;
	}


	public void setReportid(String reportid) {
		this.reportid = reportid;
	}


	public List<String> getRowcon() {
		return rowcon;
	}


	public void setRowcon(List<String> rowcon) {
		this.rowcon = rowcon;
	}


	public List<String> getColcon() {
		return colcon;
	}


	public void setColcon(List<String> colcon) {
		this.colcon = colcon;
	}


	public List<String> getMeasure() {
		return measure;
	}


	public void setMeasure(List<String> measure) {
		this.measure = measure;
	}

	public List<String> getWherecon() {
		return wherecon;
	}


	public void setWherecon(List<String> wherecon) {
		this.wherecon = wherecon;
	}


	public String getMdxstr() {
		return mdxstr;
	}


	public void setMdxstr(String mdxstr) {
		this.mdxstr = mdxstr;
	}


	public List<String> getWithcon() {
		return withcon;
	}


	public void setWithcon(List<String> withcon) {
		this.withcon = withcon;
	}


	public String getMdx(){
		return "" ;
	}
}
