/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50527
Source Host           : 127.0.0.1:3306
Source Database       : r3_bi

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2015-12-23 08:39:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `crm_contacts`
-- ----------------------------
DROP TABLE IF EXISTS `crm_contacts`;
CREATE TABLE `crm_contacts` (
  `id` varchar(45) NOT NULL DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `dept` varchar(255) DEFAULT NULL,
  `birt` varchar(255) DEFAULT NULL,
  `deptpr` varchar(255) DEFAULT NULL,
  `qian` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `fphone` varchar(255) DEFAULT NULL,
  `ophone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `zhuli` varchar(255) DEFAULT NULL,
  `zhuliphone` varchar(255) DEFAULT NULL,
  `zhulimobile` varchar(255) DEFAULT NULL,
  `otheraddress` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `share` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_contacts
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_account`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_account`;
CREATE TABLE `rivu5_account` (
  `ID` varchar(32) NOT NULL,
  `TID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `USERID` varchar(32) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `BALANCE` double DEFAULT NULL,
  `AMOUNT` double DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LASTOP` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SPARE` varchar(255) DEFAULT NULL,
  `SPARE1` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155529560` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_account
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_analyzereport`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_analyzereport`;
CREATE TABLE `rivu5_analyzereport` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `REPORTTYPE` varchar(32) DEFAULT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `OBJECTCOUNT` int(11) DEFAULT NULL,
  `DICID` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DESCRIPTION` longtext,
  `HTML` longtext,
  `REPORTPACKAGE` varchar(255) DEFAULT NULL,
  `USEACL` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `rolename` text,
  `userid` text,
  `blacklist` text,
  `REPORTCONTENT` text,
  `reportmodel` varchar(32) DEFAULT NULL,
  `updatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `creater` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155529680` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_analyzereport
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_analyzereportmodel`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_analyzereportmodel`;
CREATE TABLE `rivu5_analyzereportmodel` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `TB` varchar(32) DEFAULT NULL,
  `DB` varchar(32) DEFAULT NULL,
  `CUBE` varchar(32) DEFAULT NULL,
  `MODELTYPE` varchar(255) DEFAULT NULL,
  `STYLESTR` varchar(255) DEFAULT NULL,
  `CSSCLASSNAME` varchar(255) DEFAULT NULL,
  `MPOSLEFT` varchar(32) DEFAULT NULL,
  `MPOSTOP` varchar(32) DEFAULT NULL,
  `DSTYPE` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `OBJECTID` varchar(32) DEFAULT NULL,
  `REPORTID` varchar(32) DEFAULT NULL,
  `SORTINDEX` int(11) DEFAULT NULL,
  `ROWDIMENSION` longtext,
  `COLDIMENSION` longtext,
  `MEASURE` longtext,
  `EVENTSTR` varchar(255) DEFAULT NULL,
  `DSMODEL` varchar(32) DEFAULT NULL,
  `VIEWTYPE` varchar(32) DEFAULT NULL,
  `CHARTEMPLET` varchar(32) DEFAULT NULL,
  `CHARTDATATYPE` varchar(100) DEFAULT NULL,
  `CHART3D` varchar(32) DEFAULT NULL,
  `XTITLE` varchar(255) DEFAULT NULL,
  `YTITLE` varchar(50) DEFAULT NULL,
  `CHARTTITLE` varchar(32) DEFAULT NULL,
  `DISPLAYBORDER` varchar(32) DEFAULT NULL,
  `BORDERCOLOR` varchar(32) DEFAULT NULL,
  `DISPLAYDESC` varchar(32) DEFAULT NULL,
  `FILTERSTR` longtext,
  `SORTSTR` varchar(255) DEFAULT NULL,
  `LABELTEXT` varchar(32) DEFAULT NULL,
  `FORMDISPLAY` varchar(32) DEFAULT NULL,
  `LABELSTYLE` varchar(32) DEFAULT NULL,
  `FORMNAME` varchar(32) DEFAULT NULL,
  `DEFAULTVALUE` varchar(255) DEFAULT NULL,
  `EXCHANGERW` smallint(6) DEFAULT NULL,
  `QUERYTEXT` longtext,
  `DISPLAYTITLE` smallint(6) DEFAULT NULL,
  `TITLESTR` varchar(255) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `ROWFORMATSTR` varchar(4000) DEFAULT NULL,
  `COLFORMATSTR` varchar(4000) DEFAULT NULL,
  `STARTr` varchar(255) DEFAULT NULL,
  `ENDT` varchar(255) DEFAULT NULL,
  `sqldialect` varchar(255) DEFAULT NULL,
  `pageSize` int(11) DEFAULT NULL,
  `isloadfulldata` varchar(255) DEFAULT NULL,
  `pagesetting` varchar(32) DEFAULT NULL,
  `REPORTCONTENT` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155529750` (`ID`),
  KEY `FK21289552CE4E2285` (`REPORTID`),
  KEY `FK2128955225F5B3A5` (`TB`),
  KEY `FK21289552F9EF0726` (`CUBE`),
  KEY `FK212895522DA4DEB5` (`DB`),
  KEY `FK2128955260C1BE8C` (`pagesetting`),
  CONSTRAINT `rivu5_analyzereportmodel_ibfk_1` FOREIGN KEY (`pagesetting`) REFERENCES `rivu5_analyzereportpagesetting` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_analyzereportmodel
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_analyzereportpagesetting`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_analyzereportpagesetting`;
CREATE TABLE `rivu5_analyzereportpagesetting` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `height` varchar(32) DEFAULT NULL,
  `font` varchar(500) DEFAULT NULL,
  `titlecolor` varchar(32) DEFAULT NULL,
  `backcolor` varchar(32) DEFAULT NULL,
  `width` varchar(500) DEFAULT NULL,
  `backstyle` varchar(10) DEFAULT NULL,
  `widthcolor` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_analyzereportpagesetting
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_auth`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_auth`;
CREATE TABLE `rivu5_auth` (
  `ID` varchar(32) NOT NULL COMMENT '主键',
  `resourceid` varchar(32) DEFAULT NULL COMMENT '资源ID',
  `resourcetype` varchar(10) DEFAULT NULL COMMENT '资源类型 1 报表  2模型',
  `resourcedic` varchar(32) DEFAULT NULL,
  `authread` varchar(45) DEFAULT NULL COMMENT '操作权限 1读',
  `authtype` varchar(255) DEFAULT NULL,
  `ownerid` varchar(32) DEFAULT NULL COMMENT '所有者id',
  `ownertype` varchar(10) DEFAULT NULL COMMENT '所有者类型',
  `orgi` varchar(16) DEFAULT NULL COMMENT 'orgi',
  `createtime` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `dataid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_auth
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_blacklist`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_blacklist`;
CREATE TABLE `rivu5_blacklist` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `ROLES` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `LISTTYPE` varchar(255) DEFAULT NULL,
  `USERNAMES` varchar(255) DEFAULT NULL,
  `IPSTART` varchar(255) DEFAULT NULL,
  `IPEND` varchar(255) DEFAULT NULL,
  `IPROLES` varchar(255) DEFAULT NULL,
  `TIMELIMIT` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155529830` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_blacklist
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_category`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_category`;
CREATE TABLE `rivu5_category` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLET` varchar(255) DEFAULT NULL,
  `BLOCKLISTTEMPLET` varchar(255) DEFAULT NULL,
  `DATASOURCE` varchar(255) DEFAULT NULL,
  `FULLCONTENTVIEW` varchar(255) DEFAULT NULL,
  `DEFAULTCATE` smallint(6) DEFAULT NULL,
  `MLT` smallint(6) DEFAULT NULL,
  `MLTFIELD` varchar(255) DEFAULT NULL,
  `MLTCOUNT` int(11) DEFAULT NULL,
  `MLTNTP` int(11) DEFAULT NULL,
  `RULENAME` varchar(255) DEFAULT NULL,
  `FACETFIELD` longtext,
  `FACETQUERY` longtext,
  `FACETDATE` longtext,
  `SECLEV` varchar(50) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155529880` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_category
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_cloudpackage`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_cloudpackage`;
CREATE TABLE `rivu5_cloudpackage` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `ORGI` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USERID` varchar(255) DEFAULT NULL,
  `SPARE0` varchar(255) DEFAULT NULL,
  `PRICE` double DEFAULT NULL,
  `SPARE1` varchar(255) DEFAULT NULL,
  `SERVERHOST` varchar(255) DEFAULT NULL,
  `HOSTAREA` varchar(255) DEFAULT NULL,
  `CLUSTERTYPE` varchar(255) DEFAULT NULL,
  `SHARECORE` smallint(6) NOT NULL,
  `INDEXSIZE` bigint(20) NOT NULL,
  `TRANSFEROUT` bigint(20) NOT NULL,
  `DOCNUM` bigint(20) NOT NULL,
  `CORES` int(11) NOT NULL,
  `PHONESUPPORT` int(11) NOT NULL,
  `MAILSUPPORT` int(11) NOT NULL,
  `BBSUPPORT` int(11) NOT NULL,
  `PACKAGETYPE` int(11) NOT NULL,
  `CLUSTERS` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155529950` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_cloudpackage
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_clusterserver`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_clusterserver`;
CREATE TABLE `rivu5_clusterserver` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `ORGI` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USERID` varchar(255) DEFAULT NULL,
  `SPARE0` varchar(255) DEFAULT NULL,
  `SPARE1` varchar(255) DEFAULT NULL,
  `SERVERHOST` varchar(255) DEFAULT NULL,
  `ZKHOST` varchar(255) DEFAULT NULL,
  `HOSTAREA` varchar(2000) DEFAULT NULL,
  `CLUSTERTYPE` varchar(255) DEFAULT NULL,
  `SERVERS` int(11) NOT NULL,
  `SEARCHCORES` int(11) NOT NULL,
  `PORT` int(11) DEFAULT NULL,
  `BACK` tinyint(4) DEFAULT NULL,
  `EXCHANGE` tinyint(4) DEFAULT NULL,
  `DISPATCH` tinyint(4) DEFAULT NULL,
  `ANALYSE` tinyint(4) DEFAULT NULL,
  `OTHERS` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530020` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_clusterserver
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_columnproperties`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_columnproperties`;
CREATE TABLE `rivu5_columnproperties` (
  `id` varchar(32) NOT NULL,
  `format` varchar(255) DEFAULT NULL,
  `prefix` varchar(255) DEFAULT NULL,
  `width` varchar(255) DEFAULT NULL,
  `suffix` varchar(255) DEFAULT NULL,
  `font` varchar(255) DEFAULT NULL,
  `colname` varchar(255) DEFAULT NULL,
  `border` varchar(255) DEFAULT NULL,
  `decimalCount` varchar(255) DEFAULT NULL,
  `sepsymbol` varchar(255) DEFAULT NULL,
  `alignment` varchar(255) DEFAULT NULL,
  `fontStyle` varchar(255) DEFAULT NULL,
  `fontColor` varchar(255) DEFAULT NULL,
  `paramName` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `dataid` varchar(255) DEFAULT NULL,
  `modelid` varchar(255) DEFAULT NULL,
  `dataname` varchar(255) DEFAULT NULL,
  `cur` varchar(255) DEFAULT NULL,
  `hyp` varchar(255) DEFAULT NULL,
  `timeFormat` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKB671B6F262D48197` (`modelid`),
  CONSTRAINT `rivu5_columnproperties_ibfk_1` FOREIGN KEY (`modelid`) REFERENCES `rivu5_analyzereportmodel` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_columnproperties
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_configureparam`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_configureparam`;
CREATE TABLE `rivu5_configureparam` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `DEFAULTTHREADS` int(11) NOT NULL,
  `OPERATIONMODE` int(11) NOT NULL,
  `MAXTHREADS` int(11) NOT NULL,
  `COUNTTHREADS` int(11) NOT NULL,
  `CACHEREC` int(11) NOT NULL,
  `CACHEMEMORY` int(11) NOT NULL,
  `MAXCACHEMEMORY` int(11) NOT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `PROTYPE` varchar(32) DEFAULT NULL,
  `VALUE` text,
  `LEVELS` varchar(32) DEFAULT NULL,
  `description` text,
  `TITLE` varchar(255) DEFAULT NULL,
  `PARAMTYPE` varchar(32) DEFAULT NULL,
  `PARAMVALUE` varchar(255) DEFAULT NULL,
  `type` varchar(32) DEFAULT NULL COMMENT '数据类型',
  `earlywarning` varchar(32) DEFAULT NULL COMMENT '是否预警',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530080` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_configureparam
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_crawltask`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_crawltask`;
CREATE TABLE `rivu5_crawltask` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `SECURE` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `TASKSTATUS` varchar(255) DEFAULT NULL,
  `DOCTYPE` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `LASTCRAWL` varchar(255) DEFAULT NULL,
  `SUBDIR` varchar(255) DEFAULT NULL,
  `PARSEPARAM` varchar(255) DEFAULT NULL,
  `NATIVEXML` varchar(255) DEFAULT NULL,
  `LASTUPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CREATEUSER` varchar(255) DEFAULT NULL,
  `TASKTYPE` varchar(255) DEFAULT NULL,
  `TASKNAME` varchar(255) DEFAULT NULL,
  `TASKPLAN` varchar(255) DEFAULT NULL,
  `CONFIGURE` varchar(255) DEFAULT NULL,
  `SECURECONF` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLET` varchar(255) DEFAULT NULL,
  `LISTBLOCKTEMPLET` varchar(255) DEFAULT NULL,
  `ACCOUNT` varchar(255) DEFAULT NULL,
  `RESOURCEURI` varchar(255) DEFAULT NULL,
  `MOREPARAMES` varchar(255) DEFAULT NULL,
  `DRIVERPLUGIN` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530160` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_crawltask
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_cube`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_cube`;
CREATE TABLE `rivu5_cube` (
  `ID` varchar(255) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DB` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `MPOSLEFT` varchar(32) DEFAULT NULL,
  `MPOSTOP` varchar(32) DEFAULT NULL,
  `TYPEID` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `DSTYPE` varchar(255) DEFAULT NULL,
  `MODELTYPE` varchar(32) DEFAULT NULL,
  `createdata` varchar(32) DEFAULT NULL,
  `startindex` int(11) DEFAULT NULL,
  `startdate` datetime DEFAULT NULL,
  `dataid` varchar(32) DEFAULT NULL,
  `dataflag` varchar(255) DEFAULT NULL,
  `CREATER` varchar(32) DEFAULT NULL,
  `UPDATETIME` timestamp NULL DEFAULT NULL,
  `CUBEFILE` longtext,
  UNIQUE KEY `SQL121227155530220` (`ID`),
  KEY `FKD104A39E2DA4DEB5` (`DB`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_cube
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_cubelevel`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_cubelevel`;
CREATE TABLE `rivu5_cubelevel` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COLUMNAME` varchar(255) DEFAULT NULL,
  `UNIQUEMEMBERS` smallint(6) DEFAULT NULL,
  `TYPE` varchar(32) DEFAULT NULL,
  `LEVELTYPE` varchar(32) DEFAULT NULL,
  `TABLENAME` varchar(255) DEFAULT NULL,
  `CUBEID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `SORTINDEX` int(11) DEFAULT NULL,
  `PARAMETERS` longtext,
  `ATTRIBUE` longtext,
  `DIMID` varchar(32) DEFAULT NULL,
  `PERMISSIONS` smallint(6) DEFAULT NULL,
  `TABLEPROPERTY` varchar(32) DEFAULT NULL,
  `FORMATSTR` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530260` (`ID`),
  KEY `FK6B288AA64F175B08` (`TABLEPROPERTY`),
  KEY `FK6B288AA660EA53CD` (`DIMID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_cubelevel
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_cubemeasure`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_cubemeasure`;
CREATE TABLE `rivu5_cubemeasure` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COLUMNAME` varchar(255) DEFAULT NULL,
  `UNIQUEMEMBERS` smallint(6) DEFAULT NULL,
  `TYPE` varchar(32) DEFAULT NULL,
  `LEVELTYPE` varchar(32) DEFAULT NULL,
  `TABLENAME` varchar(255) DEFAULT NULL,
  `CUBEID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `SORTINDEX` int(11) DEFAULT NULL,
  `PARAMETERS` longtext,
  `ATTRIBUE` longtext,
  `MID` varchar(32) DEFAULT NULL,
  `AGGREGATOR` varchar(32) DEFAULT NULL,
  `FORMATSTRING` varchar(255) DEFAULT NULL,
  `CALCULATEDMEMBER` smallint(6) DEFAULT NULL,
  `MODELTYPE` varchar(32) DEFAULT NULL,
  `MEASURE` varchar(32) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530310` (`ID`),
  KEY `FK76F5540039E09E8B` (`MEASURE`),
  KEY `FK76F55400A94EDB01` (`CUBEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_cubemeasure
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_cubemetadata`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_cubemetadata`;
CREATE TABLE `rivu5_cubemetadata` (
  `ID` varchar(32) NOT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TB` varchar(32) DEFAULT NULL,
  `ORGI` varchar(255) DEFAULT NULL,
  `CUBE` varchar(32) DEFAULT NULL,
  `POSTOP` varchar(32) DEFAULT NULL,
  `POSLEFT` varchar(32) DEFAULT NULL,
  `MTYPE` varchar(5) DEFAULT NULL,
  `NAMEALIAS` varchar(255) DEFAULT NULL,
  `PARAMETERS` varchar(255) DEFAULT NULL,
  `ATTRIBUE` longtext,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530340` (`ID`),
  KEY `FK871BA0CD25F5B3A5` (`TB`),
  KEY `FK871BA0CDF9EF0726` (`CUBE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_cubemetadata
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_databasetask`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_databasetask`;
CREATE TABLE `rivu5_databasetask` (
  `ID` varchar(32) NOT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `ENCODING` varchar(255) DEFAULT NULL,
  `PORT` int(11) NOT NULL,
  `SECURE` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `TASKSTATUS` varchar(255) DEFAULT NULL,
  `DOCTYPE` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `LASTCRAWL` varchar(255) DEFAULT NULL,
  `LASTUPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CREATEUSER` varchar(255) DEFAULT NULL,
  `TASKTYPE` varchar(255) DEFAULT NULL,
  `TASKNAME` varchar(255) DEFAULT NULL,
  `TASKPLAN` varchar(255) DEFAULT NULL,
  `CONFIGURE` varchar(255) DEFAULT NULL,
  `SECURECONF` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLET` varchar(255) DEFAULT NULL,
  `LISTBLOCKTEMPLET` varchar(255) DEFAULT NULL,
  `ACCOUNT` varchar(255) DEFAULT NULL,
  `ATTACHMENT` varchar(255) DEFAULT NULL,
  `DATABASETYPE` varchar(255) DEFAULT NULL,
  `DATABASENAME` varchar(255) DEFAULT NULL,
  `CONNECTPARAM` varchar(255) DEFAULT NULL,
  `DATABASEURL` varchar(255) DEFAULT NULL,
  `DRIVERCLAZZ` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `sqldialect` varchar(255) DEFAULT NULL,
  `JNDIPARAM` varchar(255) DEFAULT NULL,
  `JNDINAME` varchar(255) DEFAULT NULL,
  `CONNCTIONTYPE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530370` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_databasetask
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_datadic`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_datadic`;
CREATE TABLE `rivu5_datadic` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(32) DEFAULT NULL,
  `TITLE` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `PARENTID` varchar(32) DEFAULT NULL,
  `TYPE` varchar(32) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `STATUS` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NULL DEFAULT NULL,
  `UPDATETIME` timestamp NULL DEFAULT NULL,
  `CREATER` varchar(255) DEFAULT NULL,
  `PUBLISHEDTYPE` varchar(32) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `TABTYPE` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530400` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_datadic
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_datasource`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_datasource`;
CREATE TABLE `rivu5_datasource` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `DRIVER` varchar(255) DEFAULT NULL,
  `URL` longtext,
  `USERNAME` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `MINCONN` int(11) DEFAULT NULL,
  `MAXCONN` int(11) DEFAULT NULL,
  `INITCONN` int(11) DEFAULT NULL,
  `TIMEOUTVALUE` int(11) DEFAULT NULL,
  `WAITTIME` int(11) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530430` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_datasource
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_datatable`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_datatable`;
CREATE TABLE `rivu5_datatable` (
  `id` varchar(32) NOT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `typeid` varchar(255) DEFAULT NULL,
  `ename` varchar(255) DEFAULT NULL,
  `cname` varchar(255) DEFAULT NULL,
  `typename` varchar(255) DEFAULT NULL,
  `demo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_datatable
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_datatabletype`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_datatabletype`;
CREATE TABLE `rivu5_datatabletype` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `parentid` varchar(255) DEFAULT NULL,
  `demo` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_datatabletype
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_datatableview`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_datatableview`;
CREATE TABLE `rivu5_datatableview` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `tablename` varchar(255) DEFAULT NULL,
  `typeid` varchar(255) DEFAULT NULL,
  `typename` varchar(255) DEFAULT NULL,
  `tableid` varchar(255) DEFAULT NULL,
  `databaseid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_datatableview
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_datatableviewfield`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_datatableviewfield`;
CREATE TABLE `rivu5_datatableviewfield` (
  `id` varchar(32) NOT NULL,
  `viewid` varchar(45) DEFAULT NULL,
  `viewname` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `field` varchar(45) DEFAULT NULL,
  `datatype` varchar(255) DEFAULT NULL,
  `viewtype` varchar(255) DEFAULT NULL,
  `head` smallint(6) DEFAULT NULL,
  `addfield` smallint(6) DEFAULT NULL,
  `edit` smallint(6) DEFAULT NULL,
  `validateadd` varchar(4000) DEFAULT NULL,
  `validateedit` varchar(4000) DEFAULT NULL,
  `validateother` varchar(4000) DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `classname` varchar(255) DEFAULT NULL,
  `cssstyle` varchar(1000) DEFAULT NULL,
  `phonenumber` smallint(6) DEFAULT NULL,
  `readonly` smallint(6) DEFAULT NULL,
  `querylistlshow` smallint(6) DEFAULT NULL,
  `defaultvalue` varchar(255) DEFAULT NULL,
  `orderfield` smallint(6) DEFAULT NULL,
  `ordersequence` int(11) DEFAULT NULL,
  `ordertype` varchar(255) DEFAULT NULL,
  `demo` varchar(255) DEFAULT NULL,
  `display` smallint(6) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `field` (`field`),
  KEY `FK4E6DE33AB25E0A4D` (`viewid`),
  KEY `FK4E6DE33A4E1905F` (`field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_datatableviewfield
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_datatype`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_datatype`;
CREATE TABLE `rivu5_datatype` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CLAZZ` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `COMPRESSD` smallint(6) DEFAULT NULL,
  `ENCRYPTION` varchar(255) DEFAULT NULL,
  `INDEXANALYZER` varchar(255) DEFAULT NULL,
  `QUERYANALYZER` varchar(255) DEFAULT NULL,
  `INDEXFILTER` varchar(255) DEFAULT NULL,
  `QUERYFILTER` varchar(255) DEFAULT NULL,
  `TOKENIZED` smallint(6) NOT NULL,
  `STORED` smallint(6) NOT NULL,
  `INDEXED` smallint(6) NOT NULL,
  `MULTVALUE` smallint(6) NOT NULL,
  `OMITNORMS` smallint(6) NOT NULL,
  `POSITIONINCREMENTGAP` int(11) NOT NULL,
  `SORTMISSINGLAST` smallint(6) NOT NULL,
  `SORTMISSINGFAST` smallint(6) NOT NULL,
  `OMITTERMFREQANDPOSITIONS` smallint(6) NOT NULL,
  `COMPRESSTHRESHOLD` int(11) NOT NULL,
  `DIMENSION` int(11) NOT NULL,
  `SUBFIELDTYPES` varchar(255) DEFAULT NULL,
  `PRECISIONSTEP` int(11) NOT NULL,
  `DATABASEDATATYPE` varchar(255) DEFAULT NULL,
  `MOREPARAM` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530450` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_datatype
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_dimension`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_dimension`;
CREATE TABLE `rivu5_dimension` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CUBEID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(255) DEFAULT NULL,
  `TYPE` varchar(32) DEFAULT NULL,
  `SORTINDEX` int(11) DEFAULT NULL,
  `PARAMETERS` longtext,
  `ATTRIBUE` longtext,
  `POSLEFT` varchar(32) DEFAULT NULL,
  `POSTOP` varchar(32) DEFAULT NULL,
  `FORMATSTR` varchar(32) DEFAULT NULL,
  `MODELTYPE` varchar(32) DEFAULT NULL,
  `DIM` varchar(32) DEFAULT NULL,
  `ALLMEMBERNAME` varchar(32) DEFAULT NULL,
  `FKFIELD` varchar(255) DEFAULT NULL,
  `FKTABLE` varchar(255) DEFAULT NULL,
  `FKTABLEID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530490` (`ID`),
  KEY `FK66E5C79D5B3948B2` (`DIM`),
  KEY `FK66E5C79DA94EDB01` (`CUBEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_dimension
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_drilldown`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_drilldown`;
CREATE TABLE `rivu5_drilldown` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `dataid` varchar(255) DEFAULT NULL,
  `dataname` varchar(255) DEFAULT NULL,
  `tdstyle` varchar(255) DEFAULT NULL,
  `reportid` varchar(255) DEFAULT NULL,
  `modelid` varchar(255) DEFAULT NULL,
  `paramname` varchar(255) DEFAULT NULL,
  `paramtype` varchar(255) DEFAULT NULL,
  `paramurl` varchar(255) DEFAULT NULL,
  `paramtarget` varchar(255) DEFAULT NULL,
  `paramreport` varchar(255) DEFAULT NULL,
  `paramvalue` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK460671B462D48197` (`modelid`),
  CONSTRAINT `rivu5_drilldown_ibfk_1` FOREIGN KEY (`modelid`) REFERENCES `rivu5_analyzereportmodel` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_drilldown
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_expimplog`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_expimplog`;
CREATE TABLE `rivu5_expimplog` (
  `ID` varchar(32) NOT NULL,
  `ERROR` smallint(6) NOT NULL,
  `USERNAME` varchar(255) DEFAULT NULL,
  `ERRORMSG` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `OPTYPE` varchar(255) DEFAULT NULL,
  `OPTIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530530` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_expimplog
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_extensionpoints`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_extensionpoints`;
CREATE TABLE `rivu5_extensionpoints` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `PARAMETERS` longtext,
  `CLAZZ` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `DSCRIPTION` varchar(255) DEFAULT NULL,
  `INTERFACECLAZZ` varchar(255) DEFAULT NULL,
  `EXTENSIONTYPE` varchar(255) DEFAULT NULL,
  `ICONIMAGEPATH` varchar(255) DEFAULT NULL,
  `IMAGETYPE` varchar(255) DEFAULT NULL,
  `ICONPATH` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530560` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_extensionpoints
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_fieldqueryfilter`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_fieldqueryfilter`;
CREATE TABLE `rivu5_fieldqueryfilter` (
  `ID` varchar(32) NOT NULL,
  `FIELDTYPEID` varchar(255) DEFAULT NULL,
  `SORTINDEX` varchar(255) DEFAULT NULL,
  `FILTERID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530600` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_fieldqueryfilter
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_fieldtypefilter`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_fieldtypefilter`;
CREATE TABLE `rivu5_fieldtypefilter` (
  `ID` varchar(32) NOT NULL,
  `FIELDTYPEID` varchar(255) DEFAULT NULL,
  `SORTINDEX` varchar(255) DEFAULT NULL,
  `FILTERID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530650` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_fieldtypefilter
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_ftp`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_ftp`;
CREATE TABLE `rivu5_ftp` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SECURE` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `TASKSTATUS` varchar(255) DEFAULT NULL,
  `DOCTYPE` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `FILEPATH` varchar(255) DEFAULT NULL,
  `LASTCRAWL` varchar(255) DEFAULT NULL,
  `SUBDIR` varchar(255) DEFAULT NULL,
  `PARSEPARAM` varchar(255) DEFAULT NULL,
  `NATIVEXML` varchar(255) DEFAULT NULL,
  `LASTUPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CREATEUSER` varchar(255) DEFAULT NULL,
  `TASKTYPE` varchar(255) DEFAULT NULL,
  `TASKNAME` varchar(255) DEFAULT NULL,
  `TASKPLAN` varchar(255) DEFAULT NULL,
  `CONFIGURE` varchar(255) DEFAULT NULL,
  `SECURECONF` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLET` varchar(255) DEFAULT NULL,
  `LISTBLOCKTEMPLET` varchar(255) DEFAULT NULL,
  `ACCOUNT` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530690` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_ftp
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_historyjobdetail`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_historyjobdetail`;
CREATE TABLE `rivu5_historyjobdetail` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SOURCE` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `CRAWLTASKID` varchar(255) DEFAULT NULL,
  `CLAZZ` varchar(255) DEFAULT NULL,
  `TASKFIRETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GROUPID` varchar(255) DEFAULT NULL,
  `TASKTYPE` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `TASKID` varchar(255) DEFAULT NULL,
  `STARTTIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ENDTIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `REPORTID` varchar(255) DEFAULT NULL,
  `PLANTASK` smallint(6) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `CATALOG` varchar(255) DEFAULT NULL,
  `USERNAME` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `NICKNAME` varchar(255) DEFAULT NULL,
  `RESULTTYPE` smallint(6) DEFAULT NULL,
  `DATAID` varchar(1000) DEFAULT NULL COMMENT '报表id字符串',
  `DICID` varchar(1000) DEFAULT NULL COMMENT '目录id字符串',
  `PRIORITY` int(11) DEFAULT NULL COMMENT '优先级',
  `CRAWLTASK` varchar(32) DEFAULT NULL,
  `TARGETTASK` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530740` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_historyjobdetail
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_historyreport`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_historyreport`;
CREATE TABLE `rivu5_historyreport` (
  `ID` varchar(32) NOT NULL,
  `BYTES` bigint(20) NOT NULL,
  `THREADS` int(11) NOT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `ERRORMSG` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `STARTTIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ENDTIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `AMOUNT` varchar(255) DEFAULT NULL,
  `PAGES` int(11) NOT NULL,
  `ERRORS` int(11) NOT NULL,
  `SPEED` double NOT NULL,
  `BYTESPEED` bigint(20) NOT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530800` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_historyreport
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_indexfield`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_indexfield`;
CREATE TABLE `rivu5_indexfield` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `STORED` smallint(6) NOT NULL,
  `INDEXED` smallint(6) NOT NULL,
  `MOREPARAM` varchar(255) DEFAULT NULL,
  `DATATYPE` varchar(255) DEFAULT NULL,
  `MULTIVALUED` smallint(6) NOT NULL,
  `COPYTO` varchar(255) DEFAULT NULL,
  `FIELDTYPE` varchar(255) DEFAULT NULL,
  `UNIQUEKEY` smallint(6) NOT NULL,
  `DEFAULTSEARCH` smallint(6) NOT NULL,
  `FACET` smallint(6) NOT NULL,
  `SORTABLE` smallint(6) NOT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530860` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_indexfield
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_indexfieldtype`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_indexfieldtype`;
CREATE TABLE `rivu5_indexfieldtype` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530900` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_indexfieldtype
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_instance`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_instance`;
CREATE TABLE `rivu5_instance` (
  `ID` varchar(32) NOT NULL,
  `TID` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `R3MASTE` varchar(255) DEFAULT NULL,
  `R3SLAVE` varchar(255) DEFAULT NULL,
  `ZKHOST` varchar(255) DEFAULT NULL,
  `RECSEARCHHIS` smallint(6) NOT NULL,
  `CLOUDPACKAGE` longtext,
  `DOCNUM` int(11) DEFAULT NULL,
  `INDEXSIZE` int(11) DEFAULT NULL,
  `TRANSFEROUT` int(11) DEFAULT NULL,
  `CORES` int(11) DEFAULT NULL,
  `TYPE` int(11) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `EDITTIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EXPIRETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `INTRO` varchar(255) DEFAULT NULL,
  `USERID` varchar(32) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `SPARE` varchar(255) DEFAULT NULL,
  `SPARE1` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530940` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_instance
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_instanceorder`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_instanceorder`;
CREATE TABLE `rivu5_instanceorder` (
  `ID` varchar(32) NOT NULL,
  `INSTANCEID` varchar(32) DEFAULT NULL,
  `PACKAGEID` varchar(32) DEFAULT NULL,
  `TID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `USERID` varchar(32) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `PRICE` double DEFAULT NULL,
  `NUM` int(11) DEFAULT NULL,
  `OFF` double DEFAULT NULL,
  `TOTAL` double DEFAULT NULL,
  `SIGNCODE` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PAYTIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EXPTIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SPARE` varchar(255) DEFAULT NULL,
  `SPARE1` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530990` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_instanceorder
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_jobdetail`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_jobdetail`;
CREATE TABLE `rivu5_jobdetail` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SOURCE` varchar(255) DEFAULT NULL,
  `CLAZZ` varchar(255) DEFAULT NULL,
  `TASKFIRETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CRAWLTASKID` varchar(255) DEFAULT NULL,
  `ORGI` varchar(255) DEFAULT NULL,
  `USERNAME` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `NICKNAME` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `TASKTYPE` varchar(255) DEFAULT NULL,
  `TASKID` varchar(255) DEFAULT NULL,
  `FETCHER` smallint(6) NOT NULL,
  `PAUSE` smallint(6) NOT NULL,
  `PLANTASK` smallint(6) NOT NULL,
  `SECURE_ID` varchar(32) DEFAULT NULL,
  `CONFIGURE_ID` varchar(32) DEFAULT NULL,
  `TAKSPLAN_ID` varchar(32) DEFAULT NULL,
  `CRAWLTASK` varchar(32) DEFAULT NULL,
  `TARGETTASK` varchar(32) DEFAULT NULL,
  `STARTINDEX` int(11) DEFAULT NULL,
  `LASTDATE` timestamp NULL DEFAULT NULL,
  `CREATETABLE` tinyint(4) DEFAULT NULL,
  `MEMO` text,
  `NEXTFIRETIME` timestamp NULL DEFAULT NULL,
  `CRONEXP` varchar(255) DEFAULT NULL,
  `TASKSTATUS` varchar(32) DEFAULT NULL,
  `usearea` varchar(255) DEFAULT '',
  `areafield` varchar(255) DEFAULT NULL,
  `areafieldtype` varchar(32) DEFAULT NULL,
  `arearule` varchar(255) DEFAULT NULL,
  `minareavalue` varchar(255) DEFAULT NULL,
  `maxareavalue` varchar(255) DEFAULT NULL,
  `formatstr` varchar(255) DEFAULT NULL,
  `DATAID` varchar(1000) DEFAULT NULL COMMENT '报表id字符串',
  `DICID` varchar(1000) DEFAULT NULL COMMENT '目录id字符串',
  `taskinfo` longtext COMMENT 'taskinfo信息',
  `PRIORITY` int(11) DEFAULT NULL COMMENT '优先级',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531080` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_jobdetail
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_localfile`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_localfile`;
CREATE TABLE `rivu5_localfile` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SECURE` varchar(255) DEFAULT NULL,
  `TASKSTATUS` varchar(255) DEFAULT NULL,
  `DOCTYPE` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `FILEPATH` varchar(255) DEFAULT NULL,
  `LASTCRAWL` varchar(255) DEFAULT NULL,
  `SUBDIR` varchar(255) DEFAULT NULL,
  `PARSEPARAM` varchar(255) DEFAULT NULL,
  `NATIVEXML` varchar(255) DEFAULT NULL,
  `LASTUPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CREATEUSER` varchar(255) DEFAULT NULL,
  `TASKTYPE` varchar(255) DEFAULT NULL,
  `TASKNAME` varchar(255) DEFAULT NULL,
  `TASKPLAN` varchar(255) DEFAULT NULL,
  `CONFIGURE` varchar(255) DEFAULT NULL,
  `SECURECONF` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLET` varchar(255) DEFAULT NULL,
  `LISTBLOCKTEMPLET` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531150` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_localfile
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_log_event`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_log_event`;
CREATE TABLE `rivu5_log_event` (
  `id` varchar(32) NOT NULL,
  `port` int(11) NOT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `triggerwarning` varchar(255) DEFAULT NULL,
  `triggertime` varchar(255) DEFAULT NULL,
  `ipaddr` varchar(255) DEFAULT NULL,
  `groupid` varchar(255) DEFAULT NULL,
  `datatype` varchar(255) DEFAULT NULL,
  `dataflag` int(11) NOT NULL,
  `eventmsg` text,
  `eventdate` datetime DEFAULT NULL,
  `eventype` varchar(255) DEFAULT NULL,
  `eventlevel` varchar(255) DEFAULT NULL,
  `userid` varchar(32) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `dataid` varchar(32) DEFAULT NULL,
  `dataname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_log_event
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_log_report`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_log_report`;
CREATE TABLE `rivu5_log_report` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `type` varchar(255) DEFAULT NULL,
  `parameters` text,
  `throwable` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `usermail` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `error` text,
  `dataprocesstime` bigint(20) DEFAULT NULL,
  `dataformatime` bigint(20) DEFAULT NULL,
  `queryparsetime` bigint(20) DEFAULT NULL,
  `executetime` bigint(20) DEFAULT NULL,
  `classname` varchar(255) DEFAULT NULL,
  `starttime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `detailtype` varchar(255) DEFAULT NULL,
  `url` text,
  `reportdic` varchar(255) DEFAULT NULL,
  `dataid` varchar(32) DEFAULT NULL,
  `dataname` varchar(255) DEFAULT NULL,
  `datatype` varchar(32) DEFAULT NULL,
  `reportname` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `statues` varchar(255) DEFAULT NULL,
  `methodname` text,
  `linenumber` varchar(255) DEFAULT NULL,
  `querytime` int(255) DEFAULT NULL,
  `optext` varchar(255) DEFAULT NULL,
  `field6` varchar(255) DEFAULT NULL,
  `field7` varchar(255) DEFAULT NULL,
  `field8` varchar(255) DEFAULT NULL,
  `flowid` varchar(32) DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `funtype` varchar(32) DEFAULT NULL,
  `fundesc` varchar(255) DEFAULT NULL,
  `triggerwarning` varchar(32) DEFAULT NULL,
  `triggertime` varchar(32) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_log_report
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_log_request`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_log_request`;
CREATE TABLE `rivu5_log_request` (
  `id` varchar(32) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `parameters` longtext,
  `throwable` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `usermail` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `error` text,
  `classname` varchar(255) DEFAULT NULL,
  `starttime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `detailtype` varchar(255) DEFAULT NULL,
  `url` text,
  `reportdic` varchar(255) DEFAULT NULL,
  `reportname` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `statues` varchar(255) DEFAULT NULL,
  `methodname` text,
  `linenumber` varchar(255) DEFAULT NULL,
  `querytime` int(255) DEFAULT NULL,
  `optext` varchar(255) DEFAULT NULL,
  `field6` varchar(255) DEFAULT NULL,
  `field7` varchar(255) DEFAULT NULL,
  `field8` varchar(255) DEFAULT NULL,
  `flowid` varchar(32) DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `funtype` varchar(32) DEFAULT NULL,
  `fundesc` varchar(255) DEFAULT NULL,
  `triggerwarning` varchar(32) DEFAULT NULL,
  `triggertime` varchar(32) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_log_request
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_log_systemaction`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_log_systemaction`;
CREATE TABLE `rivu5_log_systemaction` (
  `id` varchar(32) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `throwable` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `starttime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `detailtype` varchar(255) DEFAULT NULL,
  `field6` varchar(255) DEFAULT NULL,
  `field7` varchar(255) DEFAULT NULL,
  `field8` varchar(255) DEFAULT NULL,
  `startid` varchar(32) DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `times` int(11) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `triggerwarning` varchar(32) DEFAULT NULL,
  `triggertime` varchar(32) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `ipaddr` varchar(255) DEFAULT NULL,
  `port` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_log_systemaction
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_log_systeminfo`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_log_systeminfo`;
CREATE TABLE `rivu5_log_systeminfo` (
  `id` varchar(32) NOT NULL,
  `port` int(11) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `unavailabletype` varchar(32) DEFAULT NULL,
  `unavailabletime` bigint(11) DEFAULT NULL,
  `serverdowntime` bigint(20) DEFAULT NULL,
  `netread` bigint(20) NOT NULL,
  `netwrite` bigint(20) NOT NULL,
  `createdate` datetime DEFAULT NULL,
  `ipaddr` varchar(255) DEFAULT NULL,
  `groupid` varchar(255) DEFAULT NULL,
  `datatype` varchar(255) DEFAULT NULL,
  `cpuus` double NOT NULL,
  `cpusys` double NOT NULL,
  `cpuidle` double NOT NULL,
  `cpuwait` double NOT NULL,
  `cpunice` double NOT NULL,
  `cpupercent` double DEFAULT NULL,
  `memav` bigint(20) NOT NULL,
  `memused` bigint(20) NOT NULL,
  `memfree` bigint(20) NOT NULL,
  `diskfree` bigint(20) NOT NULL,
  `diskuse` bigint(20) NOT NULL,
  `diskusepercent` double NOT NULL,
  `disktotal` bigint(20) NOT NULL,
  `diskread` bigint(20) NOT NULL,
  `diskwrite` bigint(20) NOT NULL,
  `diskqueue` double NOT NULL,
  `netreadspeed` bigint(20) NOT NULL,
  `netwritespeed` bigint(20) NOT NULL,
  `netname` varchar(255) DEFAULT NULL,
  `mempercent` double NOT NULL,
  `memtotal` bigint(20) NOT NULL,
  `rxpackage` bigint(20) NOT NULL,
  `txpackage` bigint(20) NOT NULL,
  `txpackagespeed` double DEFAULT NULL,
  `rxpackagespeed` double DEFAULT NULL,
  `rxerrors` bigint(20) NOT NULL,
  `txerrors` bigint(20) NOT NULL,
  `dataflag` int(11) DEFAULT NULL,
  `jvmmemtotal` bigint(20) DEFAULT NULL,
  `jvmmemfree` bigint(20) DEFAULT NULL,
  `jvmmemusepercent` double DEFAULT NULL,
  `jvmmemuse` bigint(20) DEFAULT NULL,
  `triggertime` varchar(32) DEFAULT NULL,
  `triggerwarning` varchar(32) DEFAULT NULL,
  `netspeed` bigint(20) DEFAULT NULL,
  `netuseprecent` double DEFAULT NULL,
  `uptime` bigint(20) DEFAULT NULL,
  `updatetime` datetime DEFAULT NULL,
  `ostype` varchar(255) DEFAULT NULL,
  `hardware` varchar(255) DEFAULT NULL,
  `tempdiskfree` bigint(20) DEFAULT NULL,
  `tempdisk` bigint(20) DEFAULT NULL,
  `tempdiskpercent` double DEFAULT NULL,
  `diskwritespeed` double DEFAULT NULL,
  `diskreadspeed` double DEFAULT NULL,
  `swaptotal` bigint(20) DEFAULT NULL,
  `swapuse` bigint(20) DEFAULT NULL,
  `swapfree` bigint(20) DEFAULT NULL,
  `swappercent` double DEFAULT NULL,
  `swappagein` bigint(20) DEFAULT NULL,
  `swappageout` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_log_systeminfo
-- ----------------------------
INSERT INTO `rivu5_log_systeminfo` VALUES ('297e1e8751cc3bef0151cc464189008d', '0', '', null, '0', '0', '0', '0', '2015-12-23 08:38:55', '', '', 'r3query_v50_server', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '290166227', '375914496', '93901160', '0.7502061128616333', '282013336', null, null, '0', '0', '1450830460127', '2015-12-23 08:27:40', 'Windows 8 6.2', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `rivu5_log_systeminfo` VALUES ('297e1e8751cc3bef0151cc465513008e', '0', '', null, '0', '0', '0', '0', '2015-12-23 08:39:00', '', '', 'r3query_v50_server', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '290166228', '375914496', '93901160', '0.7502061128616333', '282013336', null, null, '0', '0', '1450830460127', '2015-12-23 08:27:40', 'Windows 8 6.2', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `rivu5_log_systeminfo` VALUES ('297e1e8751cc3bef0151cc46689b008f', '0', '', null, '0', '0', '0', '0', '2015-12-23 08:39:05', '', '', 'r3query_v50_server', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '290166229', '375914496', '92927720', '0.7527956366539001', '282986776', null, null, '0', '0', '1450830460127', '2015-12-23 08:27:40', 'Windows 8 6.2', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `rivu5_log_systeminfo` VALUES ('297e1e8751cc3bef0151cc467c220090', '0', '', null, '0', '0', '0', '0', '2015-12-23 08:39:10', '', '', 'r3query_v50_server', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '290166230', '375914496', '92927720', '0.7527956366539001', '282986776', null, null, '0', '0', '1450830460127', '2015-12-23 08:27:40', 'Windows 8 6.2', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `rivu5_log_systeminfo` VALUES ('297e1e8751cc3bef0151cc468fab0091', '0', '', null, '0', '0', '0', '0', '2015-12-23 08:39:15', '', '', 'r3query_v50_server', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0', '0', '290166231', '375914496', '91871016', '0.7556066513061523', '284043480', null, null, '0', '0', '1450830460127', '2015-12-23 08:27:40', 'Windows 8 6.2', null, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for `rivu5_log_systemout`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_log_systemout`;
CREATE TABLE `rivu5_log_systemout` (
  `id` varchar(32) NOT NULL,
  `orgi` varchar(32) DEFAULT NULL,
  `flowid` varchar(32) DEFAULT NULL,
  `logtype` varchar(32) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `msg` longtext,
  `LEVELS` varchar(32) DEFAULT NULL,
  `thread` varchar(255) DEFAULT NULL,
  `clazz` varchar(255) DEFAULT NULL,
  `FILES` varchar(255) DEFAULT NULL,
  `linenumber` varchar(32) DEFAULT NULL,
  `method` varchar(32) DEFAULT NULL,
  `startid` varchar(32) DEFAULT NULL,
  `errorinfo` text,
  `triggerwarning` varchar(32) DEFAULT NULL,
  `triggertime` varchar(32) DEFAULT NULL,
  `triggertimes` int(11) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `code` varchar(32) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `userid` varchar(32) DEFAULT NULL,
  `username` varchar(32) DEFAULT NULL,
  `logtime` varchar(32) DEFAULT NULL,
  `ipaddr` varchar(255) DEFAULT NULL,
  `port` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_log_systemout
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_logicdatabase`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_logicdatabase`;
CREATE TABLE `rivu5_logicdatabase` (
  `ID` varchar(50) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `EXPRESS` longtext,
  `MEMO` varchar(255) DEFAULT NULL,
  `MAPPINGTYPE` varchar(255) DEFAULT NULL,
  `PROPERTYNAME` longtext,
  `VALUEOPERATOR` varchar(255) DEFAULT NULL,
  `CATEGORYID` varchar(255) DEFAULT NULL,
  `RULENAME` longtext,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531230` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_logicdatabase
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_measureconditiondata`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_measureconditiondata`;
CREATE TABLE `rivu5_measureconditiondata` (
  `id` varchar(32) NOT NULL,
  `value` double NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `dataid` varchar(255) DEFAULT NULL,
  `dataname` varchar(255) DEFAULT NULL,
  `ruleid` varchar(255) DEFAULT NULL,
  `compute` varchar(255) DEFAULT NULL,
  `style` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `excontent` varchar(255) DEFAULT NULL,
  `datacode` varchar(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `resultType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK9A50C810A9C2F11F` (`ruleid`),
  CONSTRAINT `rivu5_measureconditiondata_ibfk_1` FOREIGN KEY (`ruleid`) REFERENCES `rivu5_measureruledata` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_measureconditiondata
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_measureruledata`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_measureruledata`;
CREATE TABLE `rivu5_measureruledata` (
  `id` varchar(32) NOT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `dataid` varchar(255) DEFAULT NULL,
  `dataname` varchar(255) DEFAULT NULL,
  `warningId` varchar(255) DEFAULT NULL,
  `ifStr` varchar(255) DEFAULT NULL,
  `thenStr` varchar(255) DEFAULT NULL,
  `ifvalue` varchar(255) DEFAULT NULL,
  `thenvalue` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6417A4FB1715F2F6` (`warningId`),
  CONSTRAINT `rivu5_measureruledata_ibfk_1` FOREIGN KEY (`warningId`) REFERENCES `rivu5_measurewarningmeta` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_measureruledata
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_measurewarningmeta`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_measurewarningmeta`;
CREATE TABLE `rivu5_measurewarningmeta` (
  `id` varchar(32) NOT NULL,
  `modelid` varchar(255) DEFAULT NULL,
  `dataid` varchar(255) DEFAULT NULL,
  `dataname` varchar(255) DEFAULT NULL,
  `paramtype` varchar(255) DEFAULT NULL,
  `regionmin` double DEFAULT NULL,
  `regionmax` double DEFAULT NULL,
  `imgclass` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `reportid` varchar(255) DEFAULT NULL,
  `tdstylevar` varchar(255) DEFAULT NULL,
  `html` varchar(255) DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_measurewarningmeta
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_metadata`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_metadata`;
CREATE TABLE `rivu5_metadata` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `MIMEID` varchar(255) DEFAULT NULL,
  `INDEXFIELD` varchar(32) DEFAULT NULL,
  `PLUGIN` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531290` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_metadata
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_mime`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_mime`;
CREATE TABLE `rivu5_mime` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `DOCTYPE` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLET` varchar(255) DEFAULT NULL,
  `LISTBLOCKTEMPLET` varchar(255) DEFAULT NULL,
  `MIME` varchar(255) DEFAULT NULL,
  `EXTENTIONPOINT` varchar(255) DEFAULT NULL,
  `CONTENTICON` varchar(255) DEFAULT NULL,
  `CONTENTNAME` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531330` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_mime
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_mindmap_nodes`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_mindmap_nodes`;
CREATE TABLE `rivu5_mindmap_nodes` (
  `id` varchar(100) NOT NULL,
  `nodeid` varchar(100) DEFAULT NULL,
  `nodename` varchar(100) NOT NULL,
  `nodecode` varchar(100) DEFAULT NULL,
  `nodetype` varchar(100) DEFAULT NULL,
  `typevalue` varchar(100) DEFAULT NULL,
  `displaytype` varchar(100) DEFAULT NULL,
  `remarks` varchar(100) DEFAULT NULL,
  `hasroot` varchar(5) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `creater` varchar(100) DEFAULT NULL,
  `orgi` varchar(100) NOT NULL,
  `parentid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_mindmap_nodes
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_mindmaps`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_mindmaps`;
CREATE TABLE `rivu5_mindmaps` (
  `id` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(100) DEFAULT NULL,
  `content` text,
  `orgi` varchar(100) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `projecttypeid` varchar(100) DEFAULT NULL,
  `creater` varchar(100) DEFAULT NULL,
  `creatertime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_mindmaps
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_modelcontainer`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_modelcontainer`;
CREATE TABLE `rivu5_modelcontainer` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `TITLE` varchar(100) DEFAULT NULL,
  `ICONSTR` varchar(100) DEFAULT NULL,
  `STYLESTR` varchar(255) DEFAULT NULL,
  `FORMSTYLE` varchar(32) DEFAULT NULL,
  `USEFORM` smallint(6) DEFAULT NULL,
  `FORMACTION` varchar(255) DEFAULT NULL,
  `FORMASUBMITYPE` varchar(32) DEFAULT NULL,
  `FORMTOGGLE` varchar(32) DEFAULT NULL,
  `FROMDATAACTION` varchar(255) DEFAULT NULL,
  `REPORTID` varchar(32) DEFAULT NULL,
  `MODELCOLS` varchar(32) DEFAULT NULL,
  `DIVID` varchar(32) DEFAULT NULL,
  `FORMTARGET` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531360` (`ID`),
  KEY `FK20733FA1CE4E2285` (`REPORTID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_modelcontainer
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_monitoraccessstatistics`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_monitoraccessstatistics`;
CREATE TABLE `rivu5_monitoraccessstatistics` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `PUBLISHTYPE` varchar(255) DEFAULT NULL,
  `PUBLISHSTATUS` varchar(255) DEFAULT NULL,
  `REPORTTYPE` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `TOTALPRODUCTS` bigint(255) DEFAULT NULL,
  `MINPRODUCTS` int(11) DEFAULT NULL,
  `MAXPRODUCTS` int(11) DEFAULT NULL,
  `AVGACCESSTIME` bigint(255) DEFAULT NULL,
  `ACCESSERRORNUM` int(11) DEFAULT NULL,
  `CALCULATENUM` int(11) DEFAULT NULL,
  `MODELDBNUM` int(11) DEFAULT NULL,
  `CREATETIME` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `REPORTMS1` int(11) DEFAULT NULL,
  `REPORTMS2` int(11) DEFAULT NULL,
  `REPORTMS3` int(11) DEFAULT NULL,
  `REPORTS1` int(11) DEFAULT NULL,
  `REPORTS2` int(11) DEFAULT NULL,
  `REPORTS3` int(11) DEFAULT NULL,
  `REPORTS4` int(11) DEFAULT NULL,
  `REPORTS5` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_monitoraccessstatistics
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_netfile`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_netfile`;
CREATE TABLE `rivu5_netfile` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SECURE` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `TASKSTATUS` varchar(255) DEFAULT NULL,
  `DOCTYPE` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `FILEPATH` varchar(255) DEFAULT NULL,
  `LASTCRAWL` varchar(255) DEFAULT NULL,
  `SUBDIR` varchar(255) DEFAULT NULL,
  `PARSEPARAM` varchar(255) DEFAULT NULL,
  `NATIVEXML` varchar(255) DEFAULT NULL,
  `LASTUPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CREATEUSER` varchar(255) DEFAULT NULL,
  `TASKTYPE` varchar(255) DEFAULT NULL,
  `TASKNAME` varchar(255) DEFAULT NULL,
  `TASKPLAN` varchar(255) DEFAULT NULL,
  `CONFIGURE` varchar(255) DEFAULT NULL,
  `SECURECONF` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLET` varchar(255) DEFAULT NULL,
  `LISTBLOCKTEMPLET` varchar(255) DEFAULT NULL,
  `ACCOUNT` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531390` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_netfile
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_organ`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_organ`;
CREATE TABLE `rivu5_organ` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `CREATER` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NULL DEFAULT NULL,
  `UPDATETIME` timestamp NULL DEFAULT NULL,
  `PARENTID` varchar(32) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `FIELD4` varchar(255) DEFAULT NULL,
  `FIELD5` varchar(255) DEFAULT NULL,
  `FIELD6` varchar(255) DEFAULT NULL,
  `FIELD7` varchar(255) DEFAULT NULL,
  `FIELD8` varchar(255) DEFAULT NULL,
  `FIELD9` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_organ
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_pagechild`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_pagechild`;
CREATE TABLE `rivu5_pagechild` (
  `id` varchar(32) NOT NULL,
  `incviewid` varchar(32) DEFAULT NULL,
  `incviewname` varchar(255) DEFAULT NULL,
  `pageviewid` varchar(32) DEFAULT NULL,
  `pageviewname` varchar(255) DEFAULT NULL,
  `pageviewtext` longtext,
  `defaulttemplate` varchar(32) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_pagechild
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_pagedataview`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_pagedataview`;
CREATE TABLE `rivu5_pagedataview` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `pageview` varchar(32) DEFAULT NULL,
  `dataview` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK94370F474DBD1D1C` (`dataview`),
  CONSTRAINT `rivu5_pagedataview_ibfk_1` FOREIGN KEY (`dataview`) REFERENCES `rivu5_datatableview` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_pagedataview
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_pagetemplate`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_pagetemplate`;
CREATE TABLE `rivu5_pagetemplate` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `parentid` varchar(32) DEFAULT NULL,
  `templatetext` longtext,
  `datamodel` varchar(32) DEFAULT NULL,
  `demo` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_pagetemplate
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_pagetype`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_pagetype`;
CREATE TABLE `rivu5_pagetype` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `datamodel` varchar(32) DEFAULT NULL,
  `incview` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `parentid` varchar(255) DEFAULT NULL,
  `demo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_pagetype
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_pageview`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_pageview`;
CREATE TABLE `rivu5_pageview` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `datamodel` varchar(32) DEFAULT NULL,
  `incview` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `typeid` varchar(255) DEFAULT NULL,
  `pagecode` varchar(255) DEFAULT NULL,
  `demo` varchar(255) DEFAULT NULL,
  `viewid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_pageview
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_payment`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_payment`;
CREATE TABLE `rivu5_payment` (
  `ID` varchar(32) NOT NULL,
  `TID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `USERID` varchar(32) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `RESULT` int(11) DEFAULT NULL,
  `SOURCEID` varchar(32) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `DEMSUM` double DEFAULT NULL,
  `ACCOUNTOTAL` double DEFAULT NULL,
  `PAYMENTYPE` varchar(32) DEFAULT NULL,
  `MESSAGE` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `SPARE` varchar(255) DEFAULT NULL,
  `SPARE1` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531420` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_payment
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_pluginparam`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_pluginparam`;
CREATE TABLE `rivu5_pluginparam` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `TPID` varchar(255) DEFAULT NULL,
  `PLUGINID` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531510` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_pluginparam
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_pop3`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_pop3`;
CREATE TABLE `rivu5_pop3` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SECURE` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `TASKSTATUS` varchar(255) DEFAULT NULL,
  `DOCTYPE` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `LASTCRAWL` varchar(255) DEFAULT NULL,
  `PARSEPARAM` varchar(255) DEFAULT NULL,
  `NATIVEXML` varchar(255) DEFAULT NULL,
  `LASTUPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CREATEUSER` varchar(255) DEFAULT NULL,
  `TASKTYPE` varchar(255) DEFAULT NULL,
  `TASKNAME` varchar(255) DEFAULT NULL,
  `TASKPLAN` varchar(255) DEFAULT NULL,
  `CONFIGURE` varchar(255) DEFAULT NULL,
  `SECURECONF` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLET` varchar(255) DEFAULT NULL,
  `LISTBLOCKTEMPLET` varchar(255) DEFAULT NULL,
  `ACCOUNT` varchar(255) DEFAULT NULL,
  `POPSERVER` varchar(255) DEFAULT NULL,
  `ATTACHMENT` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531580` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_pop3
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_project`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_project`;
CREATE TABLE `rivu5_project` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `demo` varchar(255) DEFAULT NULL,
  `contexturl` longtext,
  `multilang` smallint(6) NOT NULL,
  `langtype` varchar(255) DEFAULT NULL,
  `packagename` varchar(255) DEFAULT NULL,
  `dbid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_project
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_projecttype`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_projecttype`;
CREATE TABLE `rivu5_projecttype` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `viewtype` varchar(255) DEFAULT NULL,
  `demo` varchar(255) DEFAULT NULL,
  `viewtypeid` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `parentid` varchar(255) DEFAULT NULL,
  `jsontext` varchar(255) DEFAULT NULL,
  `showtype` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_projecttype
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_projecttypechild`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_projecttypechild`;
CREATE TABLE `rivu5_projecttypechild` (
  `id` varchar(32) NOT NULL,
  `typeid` varchar(255) DEFAULT NULL,
  `viewtypeid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_projecttypechild
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_publishedcube`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_publishedcube`;
CREATE TABLE `rivu5_publishedcube` (
  `ID` varchar(255) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DB` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `MPOSLEFT` varchar(32) DEFAULT NULL,
  `MPOSTOP` varchar(32) DEFAULT NULL,
  `TYPEID` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `DSTYPE` varchar(255) DEFAULT NULL,
  `MODELTYPE` varchar(32) DEFAULT NULL,
  `createdata` varchar(32) DEFAULT NULL,
  `startindex` int(11) DEFAULT NULL,
  `startdate` datetime DEFAULT NULL,
  `dataid` varchar(32) DEFAULT NULL,
  `dataflag` varchar(255) DEFAULT NULL,
  `DATAVERSION` int(11) DEFAULT NULL,
  `CREATER` varchar(255) DEFAULT NULL,
  `USERID` varchar(32) DEFAULT NULL,
  `USERNAME` varchar(255) DEFAULT NULL,
  `CUBECONTENT` longtext,
  `DBID` varchar(32) DEFAULT NULL,
  `DICLOCATION` varchar(255) DEFAULT NULL,
  `USEREMAIL` varchar(255) DEFAULT NULL,
  UNIQUE KEY `SQL121227155530220` (`ID`),
  KEY `FKD104A39E2DA4DEB5` (`DB`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_publishedcube
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_publishedreport`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_publishedreport`;
CREATE TABLE `rivu5_publishedreport` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `REPORTTYPE` varchar(32) DEFAULT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `OBJECTCOUNT` int(11) DEFAULT NULL,
  `DICID` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DESCRIPTION` longtext,
  `HTML` longtext,
  `REPORTPACKAGE` varchar(255) DEFAULT NULL,
  `USEACL` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `rolename` text,
  `userid` text,
  `blacklist` text,
  `REPORTCONTENT` longtext,
  `reportmodel` varchar(32) DEFAULT NULL,
  `updatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `creater` varchar(255) DEFAULT NULL,
  `reportversion` int(11) DEFAULT NULL,
  `publishedtype` varchar(32) DEFAULT NULL,
  `tabtype` varchar(32) DEFAULT NULL,
  `USERNAME` varchar(32) DEFAULT NULL,
  `USEREMAIL` varchar(255) DEFAULT NULL,
  `CACHE` smallint(6) DEFAULT NULL,
  `EXTPARAM` varchar(255) DEFAULT NULL,
  `TARGETREPORT` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155529680` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_publishedreport
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_reportfilter`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_reportfilter`;
CREATE TABLE `rivu5_reportfilter` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `dataid` varchar(32) DEFAULT NULL,
  `dataname` varchar(255) DEFAULT NULL,
  `modelid` varchar(32) DEFAULT NULL,
  `reportid` varchar(32) DEFAULT NULL,
  `contype` varchar(32) DEFAULT NULL,
  `filtertype` varchar(32) DEFAULT NULL,
  `formatstr` varchar(255) DEFAULT NULL,
  `convalue` varchar(255) DEFAULT NULL,
  `userdefvalue` text,
  `valuefiltertype` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(32) DEFAULT NULL,
  `orgi` varchar(32) DEFAULT NULL,
  `content` text,
  `valuestr` varchar(255) DEFAULT NULL,
  `filterprefix` varchar(255) DEFAULT NULL,
  `filtersuffix` varchar(255) DEFAULT NULL,
  `modeltype` varchar(32) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `funtype` varchar(32) DEFAULT NULL,
  `measureid` varchar(32) DEFAULT NULL,
  `valuecompare` varchar(32) DEFAULT NULL,
  `defaultvalue` text,
  `comparetype` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_reportfilter
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_reportoperations`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_reportoperations`;
CREATE TABLE `rivu5_reportoperations` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `reporttype` varchar(32) DEFAULT NULL,
  `reportid` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `orgi` varchar(32) DEFAULT NULL,
  `objectcount` int(11) DEFAULT NULL,
  `dicid` varchar(32) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `description` text,
  `html` text,
  `reportpackage` varchar(255) DEFAULT NULL,
  `useacl` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `rolename` text,
  `userid` varchar(32) DEFAULT NULL,
  `blacklist` text,
  `reportcontent` text,
  `reportmodel` varchar(32) DEFAULT NULL,
  `updatetime` datetime DEFAULT NULL,
  `creater` varchar(255) DEFAULT NULL,
  `reportversion` int(11) DEFAULT NULL,
  `publishedtype` varchar(32) DEFAULT NULL,
  `tabtype` varchar(32) DEFAULT NULL,
  `username` varchar(32) DEFAULT NULL,
  `useremail` varchar(255) DEFAULT NULL,
  `cache` int(11) DEFAULT NULL,
  `extparam` varchar(255) DEFAULT NULL,
  `targetreport` varchar(32) DEFAULT NULL,
  `flowid` double(32,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_reportoperations
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_role`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_role`;
CREATE TABLE `rivu5_role` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `ROLEMAP` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `UPDATETIME` timestamp NULL DEFAULT NULL,
  `CREATETIME` timestamp NULL DEFAULT NULL,
  `CREATER` varchar(32) DEFAULT NULL,
  `DESCRIPTION` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531620` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_role
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_role_group`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_role_group`;
CREATE TABLE `rivu5_role_group` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) DEFAULT NULL,
  `parentid` varchar(32) DEFAULT NULL,
  `creater` varchar(32) DEFAULT NULL,
  `orgi` varchar(32) DEFAULT NULL,
  `createtime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_role_group
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_rsasetting`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_rsasetting`;
CREATE TABLE `rivu5_rsasetting` (
  `ID` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `TID` varchar(255) DEFAULT NULL,
  `PUBLICKEY` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_rsasetting
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_rule`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_rule`;
CREATE TABLE `rivu5_rule` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `SEARCHPROJECTID` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531660` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_rule
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_searchproject`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_searchproject`;
CREATE TABLE `rivu5_searchproject` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SECURE` varchar(255) DEFAULT NULL,
  `VERSION` varchar(255) DEFAULT NULL,
  `AUTHOR` varchar(255) DEFAULT NULL,
  `THEMEURL` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `RULES` varchar(255) DEFAULT NULL,
  `AUTH` smallint(6) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USERID` varchar(255) DEFAULT NULL,
  `FILTERQ` longtext,
  `QUERYRESULTSET` varchar(255) DEFAULT NULL,
  `ADMINUSER` varchar(255) DEFAULT NULL,
  `DEFAULTPROJECT` smallint(6) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531710` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_searchproject
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_searchresulttemplet`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_searchresulttemplet`;
CREATE TABLE `rivu5_searchresulttemplet` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `DESCRIPTION` longtext,
  `CODE` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USERID` varchar(255) DEFAULT NULL,
  `TEMPLETTEXT` longtext,
  `TEMPLETTYPE` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `ICONSTR` varchar(255) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `ORDERINDEX` int(11) DEFAULT NULL,
  `TYPEID` varchar(32) DEFAULT NULL,
  `SELDATA` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531770` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_searchresulttemplet
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_searchsetting`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_searchsetting`;
CREATE TABLE `rivu5_searchsetting` (
  `ID` varchar(32) NOT NULL,
  `STARTDELAY` int(11) NOT NULL,
  `MOREPARAM` varchar(255) DEFAULT NULL,
  `HIGHTLIGHT` varchar(255) DEFAULT NULL,
  `FIELDMAXLENGTH` varchar(255) DEFAULT NULL,
  `HLSNIPPETS` varchar(255) DEFAULT NULL,
  `HLHTMLSTART` varchar(255) DEFAULT NULL,
  `HLHTMLEND` varchar(255) DEFAULT NULL,
  `FACET` varchar(255) DEFAULT NULL,
  `DEFAULTPAGESIZE` varchar(255) DEFAULT NULL,
  `DEFAULTOPERATOR` varchar(255) DEFAULT NULL,
  `DATEFORMAT` varchar(255) DEFAULT NULL,
  `UPLOADIMAGESIZE` int(11) NOT NULL,
  `UPLOADJARSIZE` int(11) NOT NULL,
  `DEFAULTUPLOADIMAGEPATH` varchar(255) DEFAULT NULL,
  `DEFAULTUPLOADJARPATH` varchar(255) DEFAULT NULL,
  `DEFAULTTEMPLETPATH` varchar(255) DEFAULT NULL,
  `FACTTEMPLET` varchar(255) DEFAULT NULL,
  `RELATEDTEMPLET` varchar(255) DEFAULT NULL,
  `FACTSYSTEMFIELD` varchar(255) DEFAULT NULL,
  `SORTSYSTEMFIELD` varchar(255) DEFAULT NULL,
  `SORTTEMPLET` varchar(255) DEFAULT NULL,
  `SPELLCHECKTEMPLET` varchar(255) DEFAULT NULL,
  `SPELLCHECK` varchar(255) DEFAULT NULL,
  `SUGGESTAREATEMPLET` varchar(255) DEFAULT NULL,
  `SITENAME` varchar(255) DEFAULT NULL,
  `LOGONIMAGEPATH` varchar(255) DEFAULT NULL,
  `SITEDESC` varchar(255) DEFAULT NULL,
  `COPYRIGHT` varchar(255) DEFAULT NULL,
  `USERWELCOMETEMPLET` varchar(255) DEFAULT NULL,
  `SEARCHBOXTEMPLET` varchar(255) DEFAULT NULL,
  `FACTSEARCHQUERY` longtext,
  `FACTDATE` longtext,
  `FACETDATEGAP` varchar(255) DEFAULT NULL,
  `SITEEMAIL` varchar(255) DEFAULT NULL,
  `SEARCHTYPE` varchar(255) DEFAULT NULL,
  `DEFAULTTERMSFIELD` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `LOCALSERVER` varchar(255) DEFAULT NULL,
  `R3CLOUDSERVER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531810` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_searchsetting
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_secureconfigure`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_secureconfigure`;
CREATE TABLE `rivu5_secureconfigure` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `SECURELEVEL` varchar(255) DEFAULT NULL,
  `ENCRYPTION` smallint(6) NOT NULL,
  `SECSTATUS` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531860` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_secureconfigure
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_selffiltercondtion`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_selffiltercondtion`;
CREATE TABLE `rivu5_selffiltercondtion` (
  `id` varchar(32) NOT NULL,
  `modelid` varchar(32) DEFAULT NULL,
  `levelid` varchar(32) DEFAULT NULL,
  `levelname` varchar(255) DEFAULT NULL,
  `relation` varchar(255) DEFAULT NULL,
  `comparision` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_selffiltercondtion
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_selfreportchoicelevel`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_selfreportchoicelevel`;
CREATE TABLE `rivu5_selfreportchoicelevel` (
  `ID` varchar(32) NOT NULL DEFAULT '',
  `modelid` varchar(255) DEFAULT NULL,
  `levelid` varchar(255) DEFAULT NULL,
  `choicemembers` blob,
  `ORGI` varchar(255) DEFAULT NULL,
  `memberprefix` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_selfreportchoicelevel
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_server`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_server`;
CREATE TABLE `rivu5_server` (
  `ID` varchar(32) NOT NULL,
  `TID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `USERID` varchar(32) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PLATFORMSERVER` varchar(255) DEFAULT NULL,
  `TENANTSERVER` varchar(255) DEFAULT NULL,
  `INSTANCESERVER` varchar(255) DEFAULT NULL,
  `CRAWLSERVER` varchar(255) DEFAULT NULL,
  `INDEXSERVER` varchar(255) DEFAULT NULL,
  `SEARCHSERVER` varchar(255) DEFAULT NULL,
  `ZKHOST` varchar(255) DEFAULT NULL,
  `MQSERVER` varchar(255) DEFAULT NULL,
  `UPDATESERVER` varchar(255) DEFAULT NULL,
  `OTHERSERVER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531900` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_server
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_site`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_site`;
CREATE TABLE `rivu5_site` (
  `ID` varchar(32) NOT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `DEFAULTERRORMSGNUM` int(11) NOT NULL,
  `DEFAULTTHREADS` int(11) NOT NULL,
  `OPERATIONMODE` int(11) NOT NULL,
  `MAXTHREADS` int(11) NOT NULL,
  `COUNTTHREADS` int(11) NOT NULL,
  `CACHEREC` int(11) NOT NULL,
  `CACHEMEMORY` int(11) NOT NULL,
  `MAXCACHEMEMORY` int(11) NOT NULL,
  `SEARCHINDEX` varchar(255) DEFAULT NULL,
  `UNIQUEKEY` varchar(255) DEFAULT NULL,
  `HLFRAGMENTERLENGTH` int(11) NOT NULL,
  `MAXRUNNINGTASK` int(11) NOT NULL,
  `DATABASEMAXTHREADS` int(11) NOT NULL,
  `SMTPSERVER` varchar(255) DEFAULT NULL,
  `SMTPUSER` varchar(255) DEFAULT NULL,
  `SMTPPASSWORD` varchar(255) DEFAULT NULL,
  `MAILFROM` varchar(255) DEFAULT NULL,
  `SECLEV` varchar(50) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155531970` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_site
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_tabledir`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_tabledir`;
CREATE TABLE `rivu5_tabledir` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(32) DEFAULT NULL,
  `TITLE` varchar(32) DEFAULT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `PARENTID` varchar(32) DEFAULT NULL,
  `TYPE` varchar(32) DEFAULT NULL,
  `MEMO` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `STATUS` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NULL DEFAULT NULL,
  `UPDATETIME` timestamp NULL DEFAULT NULL,
  `CREATER` varchar(255) DEFAULT NULL,
  `DATABASEID` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155530400` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_tabledir
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_tableproperties`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_tableproperties`;
CREATE TABLE `rivu5_tableproperties` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `FIELDNAME` varchar(255) DEFAULT NULL,
  `DATATYPECODE` int(11) NOT NULL,
  `DATATYPENAME` varchar(255) DEFAULT NULL,
  `DBTABLEID` varchar(255) DEFAULT NULL,
  `INDEXDATATYPE` varchar(255) DEFAULT NULL,
  `PK` smallint(6) DEFAULT NULL,
  `MODITS` smallint(6) DEFAULT NULL,
  `INDEXFIELD` varchar(32) DEFAULT NULL,
  `PLUGIN` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `FKTABLE` varchar(32) DEFAULT NULL,
  `FKPROPERTY` varchar(32) DEFAULT NULL,
  `TABLENAME` varchar(255) DEFAULT NULL,
  `viewtype` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL130112140848940` (`ID`),
  KEY `FKF8D74787854BC62` (`DBTABLEID`),
  KEY `FKF8D747811BE44FF` (`FKPROPERTY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_tableproperties
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_tabletask`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_tabletask`;
CREATE TABLE `rivu5_tabletask` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SECURE` varchar(255) DEFAULT NULL,
  `TASKSTATUS` varchar(255) DEFAULT NULL,
  `TABLEDIRID` varchar(255) DEFAULT NULL,
  `DBID` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `LASTUPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `TASKTYPE` varchar(255) DEFAULT NULL,
  `TASKNAME` varchar(255) DEFAULT NULL,
  `TASKPLAN` varchar(255) DEFAULT NULL,
  `CONFIGURE` varchar(255) DEFAULT NULL,
  `SECURECONF` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLET` text,
  `LISTBLOCKTEMPLET` text,
  `TABLENAME` varchar(255) DEFAULT NULL,
  `TABLETYPE` varchar(255) DEFAULT NULL,
  `STARTINDEX` int(11) NOT NULL,
  `UPDATETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `UPDATETIMENUMBER` bigint(20) NOT NULL,
  `DATASQL` longtext,
  `DATABASETASK` varchar(32) DEFAULT NULL,
  `DRIVERPLUGIN` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL130112140849020` (`ID`),
  KEY `FK31B2348A1258E3B7` (`DATABASETASK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_tabletask
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_taskplanconfigure`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_taskplanconfigure`;
CREATE TABLE `rivu5_taskplanconfigure` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `USERID` varchar(255) DEFAULT NULL,
  `PLANTYPE` varchar(255) DEFAULT NULL,
  `RIT` varchar(255) DEFAULT NULL,
  `RUNONDATE` varchar(255) DEFAULT NULL,
  `RHOURS` varchar(255) DEFAULT NULL,
  `RMIN` varchar(255) DEFAULT NULL,
  `RSEC` varchar(255) DEFAULT NULL,
  `RWEEK` varchar(255) DEFAULT NULL,
  `RMONTH` varchar(255) DEFAULT NULL,
  `BEGINTIME` varchar(255) DEFAULT NULL,
  `RPT` varchar(255) DEFAULT NULL,
  `REPEATTERVAL` varchar(255) DEFAULT NULL,
  `CRT` varchar(255) DEFAULT NULL,
  `CRONDSTR` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155532100` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_taskplanconfigure
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_templetmapping`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_templetmapping`;
CREATE TABLE `rivu5_templetmapping` (
  `ID` varchar(32) NOT NULL,
  `TASKID` varchar(255) DEFAULT NULL,
  `LISTBLOCKID` varchar(255) DEFAULT NULL,
  `PREVIEWTEMPLETID` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155532140` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_templetmapping
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_tenant`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_tenant`;
CREATE TABLE `rivu5_tenant` (
  `ID` varchar(32) NOT NULL,
  `CODE` varchar(32) DEFAULT NULL,
  `PASSWORD` varchar(32) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `TYPE` int(11) DEFAULT NULL,
  `SOURCE` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `EDITTIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `CTYPE` int(11) DEFAULT NULL,
  `CCODE` varchar(32) DEFAULT NULL,
  `PHONE` varchar(32) DEFAULT NULL,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `POSTCODE` varchar(8) DEFAULT NULL,
  `EMAIL` varchar(64) DEFAULT NULL,
  `INTRO` longtext,
  `USERID` varchar(32) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `SPARE0` varchar(255) DEFAULT NULL,
  `SPARE1` varchar(255) DEFAULT NULL,
  `STATUS` varchar(32) DEFAULT NULL,
  `SECKEY` varchar(32) DEFAULT NULL,
  `SITE` varchar(255) DEFAULT NULL,
  `LICENCE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155532160` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_tenant
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_typecategory`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_typecategory`;
CREATE TABLE `rivu5_typecategory` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `TITLE` varchar(100) DEFAULT NULL,
  `CODE` varchar(100) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `CTYPE` varchar(32) DEFAULT NULL,
  `PARENTID` varchar(32) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `MEMO` varchar(32) DEFAULT NULL,
  `ICONSTR` varchar(255) DEFAULT NULL,
  `ICONSKIN` varchar(32) DEFAULT NULL,
  `CATETYPE` varchar(32) DEFAULT NULL,
  `CREATER` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NULL DEFAULT NULL,
  `UPDATETIME` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155532210` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_typecategory
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_user`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_user`;
CREATE TABLE `rivu5_user` (
  `ID` varchar(32) NOT NULL,
  `LANGUAGE` varchar(255) DEFAULT NULL,
  `USERNAME` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `SECURECONF` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `FIRSTNAME` varchar(255) DEFAULT NULL,
  `MIDNAME` varchar(255) DEFAULT NULL,
  `LASTNAME` varchar(255) DEFAULT NULL,
  `JOBTITLE` varchar(255) DEFAULT NULL,
  `DEPARTMENT` varchar(255) DEFAULT NULL,
  `GENDER` varchar(255) DEFAULT NULL,
  `BIRTHDAY` varchar(255) DEFAULT NULL,
  `NICKNAME` varchar(255) DEFAULT NULL,
  `USERTYPE` varchar(255) DEFAULT NULL,
  `RULENAME` varchar(255) DEFAULT NULL,
  `SEARCHPROJECTID` varchar(255) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL,
  `CREATER` varchar(32) DEFAULT NULL,
  `CREATETIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `MEMO` varchar(255) DEFAULT NULL,
  `UPDATETIME` timestamp NULL DEFAULT NULL,
  `ORGAN` varchar(32) DEFAULT NULL,
  `MOBILE` varchar(32) DEFAULT NULL,
  `passupdatetime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155532250` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_user
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_userole`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_userole`;
CREATE TABLE `rivu5_userole` (
  `ID` varchar(32) DEFAULT NULL,
  `ROLEID` varchar(32) DEFAULT NULL,
  `USERID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_userole
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_userorgan`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_userorgan`;
CREATE TABLE `rivu5_userorgan` (
  `ID` varchar(32) DEFAULT NULL,
  `ORGANID` varchar(32) DEFAULT NULL,
  `USERID` varchar(32) DEFAULT NULL,
  `ORGI` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_userorgan
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_vouchers`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_vouchers`;
CREATE TABLE `rivu5_vouchers` (
  `ID` varchar(32) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `ORGI` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UPDATETIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `USERID` varchar(255) DEFAULT NULL,
  `SIGNCODE` varchar(255) DEFAULT NULL,
  `DENSUM` double DEFAULT NULL,
  `VSTATUS` int(11) DEFAULT NULL,
  `TENANT` varchar(32) DEFAULT NULL,
  `STARTTIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ENDTIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SQL121227155532290` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_vouchers
-- ----------------------------

-- ----------------------------
-- Table structure for `rivu5_wsbusiness`
-- ----------------------------
DROP TABLE IF EXISTS `rivu5_wsbusiness`;
CREATE TABLE `rivu5_wsbusiness` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `demo` varchar(255) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `parentid` varchar(32) DEFAULT NULL,
  `wstype` varchar(255) DEFAULT NULL,
  `orgi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rivu5_wsbusiness
-- ----------------------------


INSERT INTO `rivu5_user` VALUES ('4028d481428add1801428ae6be120003', null, 'admin', 'a5092280546414436147', '5', 'panjunfeng@rivues.com', null, null, null, '123', '运维用户', null, '', '管理员', '0', '4028d481428abd8001428acb2b830002', null, 'rivues', null, '2015-11-13 12:31:44', '', '2015-12-23 08:58:46', null, '18025950907', '2015-11-13 12:32:54');

-- ----------------------------
-- Records of rivu5_searchresulttemplet
-- ----------------------------
INSERT INTO `rivu5_searchresulttemplet` VALUES ('297e8c7b478a5d7501478a61cfa2002e', '标签', 'HTML标签<label></label>', 'model', null, '2014-08-07 14:58:04', null, '<label>${v_text!\'HTML标签\'}</label>', 'reportmodule', 'rivues', '/assets/images/templeticon/html-add.png', null, null, '8a8a8ae64627cd19014628281dce0603', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('297e8c7b478a864101478a8c26a70028', '布局', '表格<table></table>', 'layout', null, '2014-07-31 12:07:42', null, '<script language=\"javascript\">\r\n$(document).ready(function(){\r\n	<#if dt.layoutcontent??>\r\n	<#list dt.layoutcontent as rows>\r\n	<#if rows_index ==0>\r\n	<#assign usedefwidth = \"\">\r\n	<#assign colsnum = 0>\r\n	<#list rows as layoutpkg>\r\n	<#if layoutpkg.style.widthdef == \'customer\'>\r\n		<#if usedefwidth!= \"\">\r\n		<#assign usedefwidth = usedefwidth + \"+\">\r\n		</#if>\r\n		<#assign usedefwidth = usedefwidth + layoutpkg.style.width>\r\n	<#else>\r\n	<#assign colsnum = colsnum + 1>\r\n	</#if>\r\n	</#list>\r\n	<#list rows as layoutpkg>\r\n	<#if layoutpkg.style.widthdef == \'auto\'>\r\n	$(\'#col_${layoutpkg.id!\'\'}\').width(($(\'#layout_${dt.id!\'\'}\').width() - <#if usedefwidth?? && usedefwidth!=\'\'>${usedefwidth}<#else>0</#if>)/${colsnum});\r\n	<#else>\r\n		<#if layoutpkg.style.width?? && layoutpkg.style.width!= \"\">$(\'#col_${layoutpkg.id!\'\'}\').width(${layoutpkg.style.width});</#if>\r\n	</#if>\r\n	</#list>\r\n	</#if>\r\n	</#list>\r\n	</#if>\r\n\r\n});\r\n</script>\r\n<table class=\"comptable\" style=\"width:100%;height:100%;\" class=\"layout\" id=\'layout_${dt.id!\'\'}\'>\r\n	<#if dt.layoutcontent??>\r\n	\r\n	<#list dt.layoutcontent as rows>\r\n	<#if rows_index == 0 >\r\n	<tr>\r\n		<#list rows as layoutpkg>\r\n		<td id=\'col_${layoutpkg.id!\"\"}\' style=\"height:0px;border:0px;padding:0px;<#if layoutpkg.style.widthdef == \'customer\' && layoutpkg.style.width != \'\'>width:${layoutpkg.style.width}px;</#if>\">\r\n			\r\n		</td>\r\n		</#list>\r\n	</tr>\r\n	</#if>\r\n	<tr>\r\n		<#list rows as layoutpkg>\r\n		<#if layoutpkg.display == true>\r\n		<td style=\"<#if layoutpkg.style.cellwidth??>width:${layoutpkg.style.cellwidth};</#if><#if layoutpkg.style.cellheight??>height:${layoutpkg.style.cellheight};</#if>${layoutpkg.style.padding!\'\'}\" data-id=\"${dt.id!\'\'}\" rowspan=\"${layoutpkg.style.rowspan}\" colspan=\"${layoutpkg.style.colspan}\" data-type=\"layout-cell\" data-rowinx=\"${rows_index}\" data-cell=\"${layoutpkg.id!\'\'}\" data-colinx=\"${layoutpkg_index}\">\r\n			<div class=\"innercom\">\r\n    			<#if layoutpkg?? && layoutpkg.modelpkg??>\r\n    			<div class=\"comp\" data-id=\"${layoutpkg.modelpkg.id!\'\'}\" data-cell=\"${layoutpkg.id!\'\'}\"  data-layout=\"${dt.id!\'\'}\" id=\"comp_${layoutpkg.modelpkg.id!\'\'}\" style=\"height:100%;\">\r\n    				<div class=\"comp-drag-handle\"></div>\r\n    				<div class=\"comp-remove\" style=\"margin-right:13px;\" onclick=\"BtnAction.delPackageLayout(\'${dt.id!\'\'}\',\'${layoutpkg.modelpkg.id!\'\'}\',\'#design_table\')\"></div>\r\n    				<#if layoutpkg.modelpkg.pkgtype?? && layoutpkg.modelpkg.pkgtype = \'chart\'>\r\n    				<div style=\"width:<#if reportModel.defwidth?? && reportModel.defwidth??>${reportModel.defwidth}px<#else>100%</#if>;background-color: #FEFEFE;height:<#if reportModel.defheight??>${reportModel.defheight}<#else>200px</#if>\"  id=\"chart_${layoutpkg.modelpkg.id!\'\'}\" data-id=\"${layoutpkg.modelpkg.id!\'\'}\">\r\n    				<#else>\r\n    				<div id=\"pkg_${reportModel.id!\'\'}\" data-id=\"${layoutpkg.modelpkg.id!\'\'}\" style=\"height:100%;\">\r\n    				</#if>\r\n    				<#if layoutpkg.modelpkg.templet?? && layoutpkg.modelpkg.templet.templettext??>\r\n    					<#assign temp = dt>\r\n    					<#assign dt = layoutpkg.modelpkg>\r\n    					<#assign inlineTemplate = layoutpkg.modelpkg.templet.templettext?interpret>\r\n    					<@inlineTemplate />\r\n    					<#assign dt = temp>\r\n    				</#if>					\r\n    				</div>	\r\n    			</div>			\r\n    			</#if>\r\n			</div>\r\n		</td>\r\n		</#if>\r\n		</#list>\r\n	</tr>\r\n	</#list>\r\n	</#if>\r\n</table>\r\n', 'reportmodule', 'rivues', '/assets/images/design/layout_tpl.png', null, null, '8a8a8ae64627cd19014628281dce0603', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('297e8c7b479007e60147901ac0150064', '下拉列表', '下拉列表<select></select>', 'model', null, '2014-08-01 13:46:17', null, '<span class=\"cx-input-label\" style=\"\"><#if v_name??>${v_name}:</#if></span>\r\n<select>\r\n	<#if dt?? && dt.data??>\r\n	<#list dt.data.custom as pkgdata>\r\n	<option value=\"${pkgdata.value!\'\'}\">${pkgdata.label!\'\'}</option>\r\n	</#list>\r\n	</#if>\r\n</select>', 'reportmodule', 'rivues', '/assets/images/templeticon/html-add.png', null, null, '8a8a8ae64627cd19014628281dce0603', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('297e8c7b47ae81300147ae9a630700e9', '表格', '表格组件<table></table>', 'table', null, '2014-08-07 11:54:19', null, '<script language=\"javascript\">\r\n$(document).ready(function(){\r\n	<#if dt.layoutcontent??>\r\n	<#list dt.layoutcontent as rows>\r\n	<#if rows_index ==0>\r\n	<#assign usedefwidth = \"\">\r\n	<#assign colsnum = 0>\r\n	<#list rows as layoutpkg>\r\n	<#if layoutpkg.style.widthdef == \'customer\'>\r\n		<#if usedefwidth!= \"\">\r\n		<#assign usedefwidth = usedefwidth + \"+\">\r\n		</#if>\r\n		<#assign usedefwidth = usedefwidth + layoutpkg.style.width>\r\n	<#else>\r\n	<#assign colsnum = colsnum + 1>\r\n	</#if>\r\n	</#list>\r\n	<#list rows as layoutpkg>\r\n	<#if layoutpkg.style.widthdef == \'auto\'>\r\n	$(\'#col_${layoutpkg.id!\'\'}\').width(($(\'#layout_${dt.id!\'\'}\').width() - <#if usedefwidth?? && usedefwidth!=\'\'>${usedefwidth}<#else>0</#if>)/${colsnum});\r\n	<#else>\r\n		<#if layoutpkg.style.width?? && layoutpkg.style.width!= \"\">$(\'#col_${layoutpkg.id!\'\'}\').width(${layoutpkg.style.width});</#if>\r\n	</#if>\r\n	</#list>\r\n	</#if>\r\n	</#list>\r\n	</#if>\r\n\r\n});\r\n</script>\r\n<table class=\"comptable\" style=\"width:100%;height:100%;\" class=\"layout\" id=\'table_${dt.id!\'\'}\'>\r\n	<#if dt.displaytitle>\r\n	<thead>\r\n	   <#if dt.layoutcontent??>\r\n		<#list dt.layoutcontent as rows>\r\n		<#if rows_index == 0>\r\n		<tr>\r\n			<#list rows as tablepkg>  			\r\n			<td id=\'col_${tablepkg.id!\'\'}\' style=\"<#if tablepkg.style.cellwidth??>width:${tablepkg.style.cellwidth};</#if>height:${tablepkg.style.cellheight!\'25px\'};${tablepkg.style.padding!\'\'}\" data-id=\"${dt.id!\'\'}\" rowspan=\"${tablepkg.style.rowspan}\" colspan=\"${tablepkg.style.colspan}\" data-type=\"table\" data-row=\"${rows_index}\" data-cell=\"${tablepkg.id!\'\'}\" data-col=\"${tablepkg_index}\">\r\n			${tablepkg.title!\"标题\"}\r\n			</td>\r\n			</#list>\r\n		</tr>\r\n		</#if>\r\n		</#list>\r\n		</#if>\r\n	</thead>\r\n	</#if>\r\n	<tbody>\r\n		<#if dt.layoutcontent??>\r\n		<#list dt.layoutcontent as rows>\r\n		<#if rows_index gt 0>\r\n		<tr>\r\n			<#list rows as tablepkg>\r\n			\r\n			<td id=\'col_${tablepkg.id!\'\'}\' style=\"<#if tablepkg.style.cellwidth??>width:${tablepkg.style.cellwidth};</#if>height:${tablepkg.style.cellheight!\'25px\'};${tablepkg.style.padding!\'\'}\" data-id=\"${dt.id!\'\'}\" rowspan=\"${tablepkg.style.rowspan}\" colspan=\"${tablepkg.style.colspan}\" data-type=\"layout\" data-row=\"${rows_index}\" data-cell=\"${tablepkg.id!\'\'}\" data-col=\"${tablepkg_index}\">\r\n				\r\n			</td>\r\n			</#list>\r\n		</tr>\r\n		</#if>\r\n		</#list>\r\n		</#if>\r\n	</tbody>\r\n</table>', 'reportmodule', 'rivues', '/assets/images/design/table_48.png', null, null, '8a8a8ae64627cd19014628281dce0603', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('297e8c7b47d4640e0147d4bc923f0693', '分割线', '分割线<div></div>', 'model', null, '2014-08-14 21:37:13', null, '<div style=\"height:100%;width:1px;background-color:#CCCCCC;margin:0px auto;\">&nbsp;</div>', 'reportmodule', 'rivues', '/assets/images/design/line_tpl.png', null, null, '8a8a8ae64627cd19014628281dce0603', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('297e8c7b47d4640e0147d4d2d7920864', 'HTML代码', 'HTML代码内容<html></html>', 'model', null, '2014-08-14 22:01:33', null, '<div id=\"level_div\" style=\"padding: 3px; font-size:12px;line-height:20px;\">\r\n	<p style=\"line-height:20px;\">添加一段HTML代码，您可以从编辑器里编辑这段HTML代码，HTML代码里可以包括：</p>\r\n	<ol style=\"margin-left:25px;\">\r\n		<li style=\"font-weight:bold;line-height:20px;\">1、使用标准的HTML标签代码</li>\r\n		<li style=\"font-weight:bold;line-height:20px;\">2、本地或远程图片访问</li>\r\n		<li style=\"font-weight:bold;line-height:20px;\">3、执行本地JS或VBScript</li>\r\n	</ol>\r\n</div>', 'reportmodule', 'rivues', '/assets/images/templeticon/html-add.png', null, null, '8a8a8ae64627cd19014628281dce0603', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('297e8c7b47f8b4030147f8ecf4190455', 'layout', 'layout', 'layout', null, '2014-08-21 22:16:24', null, '<div class=\"artCon\">\r\n	<div style=\"width:100%;\">\r\n		<div style=\"height:185px;overflow-y:auto;overflow-x:hidden;\">\r\n			<div class=\"mainTop row-fluid\" style=\"padding-bottom:0px;\" id=\"drillurl\">\r\n				<div class=\"span6\" style=\"padding-top:0px !important;	padding-bottom:0px !important;\">\r\n					 <div class=\"row-fluid\">\r\n						<div class=\"span3\"><h3>合并单元格（列宽）：</h3></div>\r\n						<div class=\"span2\">\r\n							<div style=\"position: relative; display: inline;\" class=\"ctrl_btn\">\r\n								<input type=\"text\"  name=\"colspan\" value=\"<#if layoutData??>${layoutData.style.colspan!\'1\'}</#if>\" style=\"padding-right: 28px;padding: 6px 4px;\"/>\r\n								<button class=\"increase\" type=\"button\" name=\"setup\"></button>\r\n								<button class=\"decrease\" type=\"button\" name=\"setdown\"></button>\r\n							</div>\r\n						</div>\r\n						<div class=\"span3\" style=\"text-align:right;\"><h3>合并单元格（行宽）：</h3></div>\r\n						<div class=\"span2\">\r\n							<div style=\"position: relative; display: inline;\" class=\"ctrl_btn\">\r\n								<input type=\"text\"  name=\"rowspan\" value=\"<#if layoutData??>${layoutData.style.rowspan!\'1\'}</#if>\" style=\"padding-right: 28px;padding: 6px 4px;\"/>\r\n								<button class=\"increase\" type=\"button\" name=\"setup\"></button>\r\n								<button class=\"decrease\" type=\"button\" name=\"setdown\"></button>\r\n							</div>\r\n						</div>\r\n					 </div>\r\n				</div>\r\n				<div class=\"span6\" style=\"padding-top:0px !important;	padding-bottom:0px !important;\">\r\n					 <div class=\"row-fluid\">\r\n						<div class=\"span2\"><h3>单元格宽度：</h3></div>\r\n						<div class=\"span3\">\r\n							<select name=\"widthdef\" class=\"span12\" onchange=\"if($(this).val() == \'customer\'){$(\'#customer_width\').show();}else{$(\'#customer_width\').hide();}\">\r\n								<option value=\"auto\" <#if layoutData?? && layoutData.style.widthdef == \'auto\'>selected=\"selected\"</#if>>默认</option>\r\n								<option value=\"customer\" <#if layoutData?? && layoutData.style.widthdef == \'customer\'>selected=\"selected\"</#if>>自定义</option>\r\n							</select>\r\n						</div>\r\n						<div class=\"span3\" id=\"customer_width\" style=\"<#if layoutData?? && layoutData.style.widthdef == \'auto\'>display:none;</#if>\">\r\n							<div style=\"position: relative; display: inline;\" class=\"ctrl_btn\">\r\n								<input type=\"text\"  name=\"width\" value=\"<#if layoutData??>${layoutData.style.width!\'300\'}</#if>\" style=\"padding-right: 28px;padding: 6px 4px;\"/>\r\n								<button class=\"increase\" type=\"button\" name=\"setup\"></button>\r\n								<button class=\"decrease\" type=\"button\" name=\"setdown\"></button>\r\n							</div>\r\n							px\r\n						</div>\r\n					 </div>\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"mainTop row-fluid\" style=\"padding-bottom:0px;\">\r\n				<div class=\"span6\" style=\"padding-top:0px !important;	padding-bottom:0px !important;\">\r\n					<div class=\"row-fluid\">\r\n						<div class=\"span2\"><h3>单元格内空白：</h3></div> 						\r\n					</div>\r\n				</div>\r\n				<div class=\"span6\" style=\"padding-top:0px !important;	padding-bottom:0px !important;\">\r\n					<div class=\"row-fluid\">\r\n						<div class=\"span2\"><h3>单元格高度：</h3></div>\r\n						<div class=\"span3\">\r\n							<select name=\"heightdef\" class=\"span12\" onchange=\"if($(this).val() == \'customer\'){$(\'#customer_height\').show();}else{$(\'#customer_height\').hide();}\">\r\n								<option value=\"auto\" <#if layoutData?? && layoutData.style.heightdef == \'auto\'>selected=\"selected\"</#if>>默认</option>\r\n								<option value=\"customer\" <#if layoutData?? && layoutData.style.heightdef == \'customer\'>selected=\"selected\"</#if>>自定义</option>\r\n							</select>\r\n						</div>	\r\n						<div class=\"span3\" id=\"customer_height\" style=\"<#if layoutData?? && layoutData.style.heightdef == \'auto\'>display:none;</#if>\"\">\r\n							<div style=\"position: relative; display: inline;\" class=\"ctrl_btn\">\r\n								<input type=\"text\"  name=\"height\" value=\"<#if layoutData??>${layoutData.style.height!\'300\'}</#if>\" style=\"padding-right: 28px;padding: 6px 4px;\"/>\r\n								<button class=\"increase\" type=\"button\" name=\"setup\"></button>\r\n								<button class=\"decrease\" type=\"button\" name=\"setdown\"></button>\r\n							</div>\r\n							px\r\n						</div>		\r\n					</div>\r\n				</div>\r\n\r\n			</div>\r\n\r\n			<div class=\"mainTop row-fluid\" style=\"padding-bottom:0px;\">\r\n				<div class=\"span6\" style=\"padding-top:0px !important;	padding-bottom:0px !important;\">\r\n					<div class=\"row-fluid\">\r\n						<div class=\"span2\"></div>\r\n						<div class=\"span1\"><h3>上：</h3></div>\r\n						<div class=\"span2\">							\r\n							<div style=\"position: relative; display: inline;\" class=\"ctrl_btn\">\r\n								<input type=\"text\"  name=\"padding_top\" value=\"<#if layoutData??>${layoutData.style.padding_top!\'2\'}</#if>\" style=\"padding-right: 28px;padding: 6px 4px;\"/>\r\n								<button class=\"increase\" type=\"button\" name=\"setup\"></button>\r\n								<button class=\"decrease\" type=\"button\" name=\"setdown\"></button>\r\n							</div>\r\n						</div>\r\n						<div class=\"span3\" style=\"text-align:right;\"><h3>右：</h3></div>\r\n						<div class=\"span2\">							\r\n							<div style=\"position: relative; display: inline;\" class=\"ctrl_btn\">\r\n								<input type=\"text\"  name=\"padding_right\" value=\"<#if layoutData??>${layoutData.style.padding_right!\'5\'}</#if>\" style=\"padding-right: 28px;padding: 6px 4px;\"/>\r\n								<button class=\"increase\" type=\"button\" name=\"setup\"></button>\r\n								<button class=\"decrease\" type=\"button\" name=\"setdown\"></button>\r\n							</div>\r\n						</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n\r\n			<div class=\"mainTop row-fluid\" style=\"padding-bottom:0px;\">\r\n				<div class=\"span6\" style=\"padding-top:0px !important;	padding-bottom:0px !important;\">\r\n					<div class=\"row-fluid\">\r\n						<div class=\"span2\"></div>\r\n						\r\n						<div class=\"span1\"><h3>下：</h3></div>\r\n						<div class=\"span2\">							\r\n							<div style=\"position: relative; display: inline;\" class=\"ctrl_btn\">\r\n								<input type=\"text\"  name=\"padding_bottom\" value=\"<#if layoutData??>${layoutData.style.padding_bottom!\'2\'}</#if>\" style=\"padding-right: 28px;padding: 6px 4px;\"/>\r\n								<button class=\"increase\" type=\"button\" name=\"setup\"></button>\r\n								<button class=\"decrease\" type=\"button\" name=\"setdown\"></button>\r\n							</div>\r\n						</div>\r\n						<div class=\"span3\" style=\"text-align:right;\"><h3>左：</h3></div>\r\n						<div class=\"span2\">							\r\n							<div style=\"position: relative; display: inline;\" class=\"ctrl_btn\">\r\n								<input type=\"text\"  name=\"padding_left\" value=\"<#if layoutData??>${layoutData.style.padding_left!\'5\'}</#if>\" style=\"padding-right: 28px;padding: 6px 4px;\"/>\r\n								<button class=\"increase\" type=\"button\" name=\"setup\"></button>\r\n								<button class=\"decrease\" type=\"button\" name=\"setdown\"></button>\r\n							</div>\r\n						</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class=\"bottomBg\">\r\n			<div class=\"btnWrap\">\r\n				<input type=\"submit\" class=\"subButtonStyle\" value=\"确认\"/>\r\n				<a href=\"javascript:closeR3ArtDialog()\" class=\"btnStyle2\">取消</a>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>\r\n<script language=\"javascript\">\r\n	//20140310 Bina\r\n	$(document).ready(function(){\r\n		$(\'.increase\').on(\'click\',function(event){\r\n			var value = $(this).parent().find(\"input\").val();\r\n			if($.isNumeric( value )){\r\n				 $(this).parent().find(\"input\").val(parseInt(value)+1);\r\n			}\r\n			//event.stopPropagation();//点击Button阻止事件冒泡到document\r\n		});\r\n		$(\'.decrease\').on(\'click\',function(event){\r\n			var value = $(this).parent().find(\"input\").val();\r\n			if($.isNumeric( value ) && (parseInt(value)-1) > 0 ){\r\n				 $(this).parent().find(\"input\").val(parseInt(value)-1);\r\n			}\r\n			//event.stopPropagation();//点击Button阻止事件冒泡到document\r\n		});		\r\n	});\r\n</script>', 'properties', 'rivues', '/assets/images/design/table.png', null, null, '297e8c7b47f8b4030147f8eb7b6a0427', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('402881e43ab07e65013ab080e4c60002', 'R3_QUERY', 'R3 Query模板', 'active', 'R3 Query模板', '2014-03-10 12:34:18', null, '<?xml version=\'1.0\' ?>\r\n<Schema name=\'${name}\'>\r\n        <#if cubeList??>\r\n    <#list cubeList as cube>\r\n    <#if !cube.modeltype?? || cube.modeltype!=\"1\">\r\n    <Cube name=\'${cube.name}\'>\r\n        <#if cube.metadata??>\r\n                <#list cube.metadata as metadata>\r\n                <#if metadata.tb.tabletype =\'1\' && ((metadata.mtype?? && metadata.mtype==\"0\") || (cube.metadata?size==1))>\r\n                <Table name=\'${metadata.tb.tablename}\' />\r\n                <#elseif metadata.tb.tabletype =\'2\'  && ((metadata.mtype?? && metadata.mtype==\"0\") || (cube.metadata?size==1))>\r\n                <View alias=\'${metadata.tb.name}\'>\r\n                        <SQL>\r\n                                ${(metadata.tb.formatDatasql!\"\")?html}\r\n                        </SQL>\r\n                </View>\r\n                </#if>\r\n                </#list>\r\n        </#if>\r\n        <#if cube.dimension??>\r\n                <#list cube.dimension as dimension>\r\n			<#if dimension.fkfield?? && dimension.fkfield?length gt 0>\r\n			<Dimension name=\'${dimension.name}\' foreignKey=\"${dimension.fkfield}\">\r\n				<Hierarchy hasAll=\'true\' <#if dimension.allmembername?? && dimension.allmembername?length gt 0>allMemberName=\'${dimension.allmembername}\'</#if> primaryKey=\"${dimension.fktableid!\'\'}\">\r\n					<Table name=\'${dimension.fktable!\"\"}\' />\r\n					<#if dimension.cubeLevel??>\r\n					<#list dimension.cubeLevel as level>\r\n\r\n					<Level name=\'${level.name}\' <#if level.leveltype?? && level.leveltype?length gt 0>levelType=\'${level.leveltype!\"\"}\'</#if> column=\'<#if level.tableproperty?? && level.tableproperty.fkproperty??>${level.tableproperty.fkproperty.name}<#else>${level.columname}</#if>\' uniqueMembers=\'${level.uniquemembers?string}\' <#if level.type??>type=\'${level.type}\'</#if> <#if level.parameters??>${level.parameters!\"\"}</#if><#if level.attribue?? && level.attribue?length gt 0><#else>/</#if>>\r\n						<#if level.attribue?? && level.attribue?length gt 0>\r\n						${level.attribue!\"\"}\r\n					</Level><#else></#if>\r\n					</#list>\r\n					</#if>\r\n				</Hierarchy>\r\n			</Dimension>  \r\n			<#else>\r\n\r\n                        <Dimension name=\'${dimension.name}\'\r\n                                <#if dimension.cubeLevel??>\r\n                                        <#list dimension.cubeLevel as level>\r\n                                                <#if level.tableproperty?? && level.tableproperty.fkproperty??>\r\n                                                        <#if cube.metadata??>\r\n                                                        <#list cube.metadata as metadata>\r\n                                                        <#if level.tableproperty.fkproperty.tablename = metadata.tb.tablename>\r\n                                                        foreignKey=\"${level.tableproperty.name}\"\r\n                                                        <#break/>\r\n                                                        </#if>\r\n                                                        </#list>\r\n                                                        </#if>\r\n\r\n                                                </#if>\r\n                                        </#list>\r\n                                </#if>>\r\n                                <Hierarchy hasAll=\'true\' <#if dimension.allmembername?? && dimension.allmembername?length gt 0>allMemberName=\'${dimension.allmembername}\'</#if>\r\n                                <#if dimension.cubeLevel??>\r\n                                        <#list dimension.cubeLevel as level>\r\n                                                <#if level.tableproperty?? && level.tableproperty.fkproperty??>\r\n                                                        <#if cube.metadata??>\r\n                                                        <#list cube.metadata as metadata>\r\n                                                        <#if level.tableproperty.fkproperty.tablename = metadata.tb.tablename>\r\n                                                        primaryKey=\"${level.tableproperty.fkproperty.name}\"\r\n                                                        <#break/>\r\n                                                        </#if>\r\n                                                        </#list>\r\n                                                        </#if>\r\n\r\n                                                </#if>\r\n                                        </#list>\r\n                                </#if>>\r\n                                        <#if dimension.cubeLevel??>\r\n                                        <#list dimension.cubeLevel as level>\r\n                                        <#if level.tableproperty?? && level.tableproperty.fkproperty??>\r\n                                        <#if cube.metadata??>\r\n                                        <#list cube.metadata as metadata>\r\n                                        <#if level.tableproperty.fkproperty.tablename = metadata.tb.tablename>\r\n\r\n                                        <#if metadata.tb.tabletype =\'1\'>\r\n                                        <Table name=\'${metadata.tb.tablename}\' />\r\n                                        <#elseif metadata.tb.tabletype =\'2\'>\r\n                                        <View alias=\'${metadata.tb.name}\'>\r\n                                                <SQL>\r\n                                                        ${(metadata.tb.datasql!\"\")?html}\r\n                                                </SQL>\r\n                                        </View>\r\n                                        </#if>\r\n\r\n\r\n\r\n                                        <#break/>\r\n                                        </#if>\r\n                                        </#list>\r\n                                        </#if>\r\n                                        <#break/>\r\n                                        </#if>\r\n                                        </#list>\r\n                                        </#if>\r\n                                        <#if dimension.cubeLevel??>\r\n                                        <#list dimension.cubeLevel as level>\r\n\r\n                                        <Level name=\'${level.name}\' <#if level.leveltype?? && level.leveltype?length gt 0>levelType=\'${level.leveltype!\"\"}\'</#if> column=\'<#if level.tableproperty?? && level.tableproperty.fkproperty??>${level.tableproperty.fkproperty.name}<#else>${level.columname}</#if>\' uniqueMembers=\'${level.uniquemembers?string}\' <#if level.type??>type=\'${level.type}\'</#if> <#if level.parameters??>${level.parameters!\"\"}</#if><#if level.attribue?? && level.attribue?length gt 0><#else>/</#if>>\r\n                                                <#if level.attribue?? && level.attribue?length gt 0>\r\n                                                ${level.attribue!\"\"}\r\n                                        </Level><#else></#if>\r\n                                        </#list>\r\n                                        </#if>\r\n                                </Hierarchy>\r\n                        </Dimension>\r\n					</#if>\r\n                </#list>\r\n        </#if>\r\n	    <#if express??>\r\n    		<#list express as dimension>\r\n    		    <#if dimension.child = true>\r\n        			<#if dimension.fkfield?? && dimension.fkfield?length gt 0>\r\n        			<Dimension name=\'${dimension.name}\' foreignKey=\"${dimension.fkfield}\">\r\n        				<Hierarchy hasAll=\'true\' <#if dimension.allmembername?? && dimension.allmembername?length gt 0>allMemberName=\'${dimension.allmembername}\'</#if> primaryKey=\"${dimension.fktableid!\'\'}\">\r\n        					<Table name=\'${dimension.fktable!\"\"}\' />\r\n        					<#if dimension.cubeLevel??>\r\n        					<#list dimension.cubeLevel as level>\r\n        \r\n        					<Level name=\'${level.name}\' <#if level.leveltype?? && level.leveltype?length gt 0>levelType=\'${level.leveltype!\"\"}\'</#if> column=\'<#if level.tableproperty?? && level.tableproperty.fkproperty??>${level.tableproperty.fkproperty.name}<#else>${level.columname}</#if>\' uniqueMembers=\'${level.uniquemembers?string}\' <#if level.type??>type=\'${level.type}\'</#if> <#if level.parameters??>${level.parameters!\"\"}</#if><#if level.attribue?? && level.attribue?length gt 0><#else>/</#if>>\r\n        						<#if level.attribue?? && level.attribue?length gt 0>\r\n        						${level.attribue!\"\"}\r\n        					</Level><#else></#if>\r\n        					</#list>\r\n        					</#if>\r\n        				</Hierarchy>\r\n        			</Dimension>  \r\n        			<#else>\r\n        \r\n        			<Dimension name=\'${dimension.name}\'\r\n        				<#if dimension.cubeLevel??>\r\n        					<#list dimension.cubeLevel as level>\r\n        						<#if level.tableproperty?? && level.tableproperty.fkproperty??>\r\n        							<#if cube.metadata??>\r\n        							<#list cube.metadata as metadata>\r\n        							<#if level.tableproperty.fkproperty.tablename = metadata.tb.tablename>\r\n        							foreignKey=\"${level.tableproperty.name}\"\r\n        							<#break/>\r\n        							</#if>\r\n        							</#list>\r\n        							</#if>\r\n        \r\n        						</#if>\r\n        					</#list>\r\n        				</#if>>\r\n        				<Hierarchy hasAll=\'true\' <#if dimension.allmembername?? && dimension.allmembername?length gt 0>allMemberName=\'${dimension.allmembername}\'</#if>\r\n        				<#if dimension.cubeLevel??>\r\n        					<#list dimension.cubeLevel as level>\r\n        						<#if level.tableproperty?? && level.tableproperty.fkproperty??>\r\n        							<#if cube.metadata??>\r\n        							<#list cube.metadata as metadata>\r\n        							<#if level.tableproperty.fkproperty.tablename = metadata.tb.tablename>\r\n        							primaryKey=\"${level.tableproperty.fkproperty.name}\"\r\n        							<#break/>\r\n        							</#if>\r\n        							</#list>\r\n        							</#if>\r\n        \r\n        						</#if>\r\n        					</#list>\r\n        				</#if>>\r\n        					<#if dimension.cubeLevel??>\r\n        					<#list dimension.cubeLevel as level>\r\n        					<#if level.tableproperty?? && level.tableproperty.fkproperty??>\r\n        					<#if cube.metadata??>\r\n        					<#list cube.metadata as metadata>\r\n        					<#if level.tableproperty.fkproperty.tablename = metadata.tb.tablename>\r\n        \r\n        					<#if metadata.tb.tabletype =\'1\'>\r\n        					<Table name=\'${metadata.tb.tablename}\' />\r\n        					<#elseif metadata.tb.tabletype =\'2\'>\r\n        					<View alias=\'${metadata.tb.name}\'>\r\n        						<SQL>\r\n        							${(metadata.tb.datasql!\"\")?html}\r\n        						</SQL>\r\n        					</View>\r\n        					</#if>\r\n        \r\n        \r\n        \r\n        					<#break/>\r\n        					</#if>\r\n        					</#list>\r\n        					</#if>\r\n        					<#break/>\r\n        					</#if>\r\n        					</#list>\r\n        					</#if>\r\n        					<#if dimension.cubeLevel??>\r\n        					<#list dimension.cubeLevel as level>\r\n        \r\n        					<Level name=\'${level.name}\' <#if level.leveltype?? && level.leveltype?length gt 0>levelType=\'${level.leveltype!\"\"}\'</#if> column=\'<#if level.tableproperty?? && level.tableproperty.fkproperty??>${level.tableproperty.fkproperty.name}<#else>${level.columname}</#if>\' uniqueMembers=\'${level.uniquemembers?string}\' <#if level.type??>type=\'${level.type}\'</#if> <#if level.parameters??>${level.parameters!\"\"}</#if><#if level.attribue?? && level.attribue?length gt 0><#else>/</#if>>\r\n        						<#if level.attribue?? && level.attribue?length gt 0>\r\n        						${level.attribue!\"\"}\r\n        					</Level><#else></#if>\r\n        					</#list>\r\n        					</#if>\r\n        				</Hierarchy>\r\n        			</Dimension>\r\n        			</#if>\r\n        		</#if>\r\n    		</#list>\r\n    	</#if>\r\n\r\n        <#if cube.measure??>\r\n                <#list cube.measure as measure>\r\n                        <#if measure.calculatedmember != true>\r\n                        <Measure name=\'${measure.name}\' <#if measure.attribue?? && measure.attribue?length gt 0><#else>column=\'${measure.columname}\'</#if> aggregator=\'${measure.aggregator}\' formatString=\'${measure.formatstring!\"####\"}\'>\r\n                                <#if measure.attribue?? && measure.attribue?length gt 0>\r\n                                ${measure.attribue!\"\"}\r\n                                </#if>\r\n                        </Measure>\r\n                        </#if>\r\n                </#list>\r\n        </#if>\r\n\r\n        <#if cube.measure??>\r\n                <#list cube.measure as measure>\r\n                        <#if measure.calculatedmember == true>\r\n                        <CalculatedMember name=\'${measure.name}\' dimension=\'Measures\' <#if measure.parameters??>${measure.parameters!\"\"}</#if>>\r\n                             <#if measure.attribue?? && measure.attribue?length gt 0>${measure.attribue!\"\"}</#if>\r\n                        </CalculatedMember>\r\n                        </#if>\r\n                </#list>\r\n        </#if>\r\n    </Cube>\r\n    </#if>\r\n    </#list>\r\n\r\n    <#list cubeList as cube>\r\n    <#if cube.modeltype?? && cube.modeltype==\"1\">\r\n    <VirtualCube name=\'${cube.name}\'>\r\n                <#if cube.dimension??>\r\n                <#list cube.dimension as dimension>\r\n                <#list cubeList as cube>\r\n                <#if cube.id == dimension.dim.cubeid>\r\n                <VirtualCubeDimension cubeName=\'${cube.name}\' name=\'${dimension.dim.name!\"\"}\'/>\r\n                <#break/>\r\n                </#if>\r\n                </#list>\r\n                </#list>\r\n                </#if>\r\n\r\n                <#if cube.measure??>\r\n                <#list cube.measure as measure>\r\n                <#if measure.calculatedmember != true>\r\n                <#list cubeList as cube>\r\n                <#if measure.measure?? && cube.id == measure.measure.cubeid>\r\n                <VirtualCubeMeasure cubeName=\'${cube.name!\"\"}\' name=\'[Measures].[${measure.measure.name!\"\"}]\'/>\r\n                <#break/>\r\n                </#if>\r\n                </#list>\r\n                </#if>\r\n                </#list>\r\n\r\n                <#list cube.measure as measure>\r\n                <#if measure.calculatedmember == true>\r\n                <CalculatedMember name=\'${measure.name}\' dimension=\"Measures\" <#if measure.parameters??>${measure.parameters!\"\"}</#if>>\r\n                        <#if measure.attribue?? && measure.attribue?length gt 0>${measure.attribue!\"\"}</#if>\r\n                  </CalculatedMember>\r\n                </#if>\r\n                </#list>\r\n                </#if>\r\n    </VirtualCube>\r\n    </#if>\r\n    </#list>\r\n\r\n    </#if>\r\n    <UserDefinedFunction name=\"compare\" className=\"com.rivues.util.bi.StrCompareFunction\"/>\r\n</Schema>', 'system', 'rivues', '', null, null, null, null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('402881e43ab07e65013ab080e4c60695', 'R3_REPORT_PUBLISH', 'R3发布报表模板', null, 'R3 Query模板', '2014-03-10 12:34:18', null, '<img src=\"${url!\'\'}\"/>', 'system', 'rivues', '', null, null, null, null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('402881e43ab07e65013ab080e4c65002', 'R3_REPORT', 'R3 Query模板', 'active', 'R3 Query模板', '2014-06-14 23:47:43', null, '<!DOCTYPE html>\r\n<!--[if lt IE 7 ]><html class=\"ie ie6\"> <![endif]-->\r\n<!--[if IE 7 ]><html class=\"ie ie7\"> <![endif]-->\r\n<!--[if IE 8 ]><html class=\"ie ie8\"> <![endif]-->\r\n<!--[if (gte IE 9)|!(IE)]><!--><html> <!--<![endif]-->\r\n<head>\r\n        <!-- Basic Page Needs\r\n  ================================================== -->\r\n        <meta charset=\"utf-8\">\r\n        <title>R3 Big Data Analytics</title>\r\n        <meta name=\"description\" content=\"\">\r\n        <meta name=\"author\" content=\"J.Chen\">\r\n        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=Edge,chrome=1\" />\r\n        <!-- 让360双核浏览器用webkit内核渲染页面\r\n  ================================================== -->\r\n	<meta name=\"renderer\" content=\"webkit\">\r\n        <!-- Mobile Specific Metas\r\n  ================================================== -->\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">\r\n\r\n        <!-- CSS\r\n  ================================================== -->\r\n	<style>\r\n		html { *overflow:auto; font-family:\"Microsoft Yahei\",\"Helvetica\",\"Simsun\",\"Arial\";}\r\n		.designer_k table th.ui-sortable-placeholder {\r\n			border: solid 1px #000;\r\n			background: #199eff!important;\r\n			visibility: visible!important;\r\n		}\r\n		.designer_k table {border: 1px solid #7dc9ff!important;border-collapse:collapse;background:#7dc9ff; }\r\n		.designer_k table tbody{overflow: auto;}\r\n		.designer_k table th,.designer_k table thead,.designer_k table td,.designer_k table tbody{border:1px solid #7dc9ff!important;}\r\n		.task_title2 {\r\n			height: 26px;\r\n			line-height: 26px;\r\n			font-size: 16px;\r\n			background: #e6e6e6;\r\n			padding: 0 0 0 12px;\r\n		}\r\n		.ds_title{font:20px/44px Microsoft yahei;color:#000;text-align:center;text-decoration:underline;border:1px dotted #c6c6c6;margin:9px 7px;}\r\n		.designer_k{overflow: auto; position: relative;}\r\n\r\n		.designer_k table th{font:12px/30px Microsoft yahei;color:#000;background:#bfe4ff;}\r\n		.designer_k table thead td{font:12px/30px Microsoft yahei;color:#000;background:#bfe4ff;}\r\n		.designer_k table .s_blue{color:#fff;background:#0383de;text-align:center;padding:0;}\r\n		.designer_k td{font:12px/22px Microsoft yahei;color:#666;background:#ffffff;}\r\n		.designer_k td.blue_k{color:#000;background:#bfe4ff;}\r\n		.designer_k table .b_none{border-bottom:none}\r\n		.designer_k table .r_x{border-right:1px solid #6f6f6f}\r\n		.designer_k table .b_x{border-bottom:1px solid #6f6f6f}\r\n		.date_k{height:30px;padding:0 10px;font:12px/30px Microsoft yahei;text-align:center;border:1px dotted #c6c6c6;margin:6px 7px 5px;}\r\n		.fl{float:left}\r\n		.fr{float:right}\r\n		.ithemTable {width: 250px;}\r\n		.ithemTable th,\r\n		.ithemTable td {vertical-align: top; height: 28px; line-height: normal;}\r\n		.ithemTable th {padding: 0 2px; background: none; white-space: nowrap; word-break: keep-all; width: 1%; font-weight: normal; font-family: \"Simsun\"; font-size: 12px; line-height: 28px; vertical-align: top;}\r\n		.tswtd {padding: 4px;}\r\n		.ithemTable td {font-size: 12px; line-height: normal; vertical-align: top;}\r\n\r\n		.ithemTable .siil {\r\n			line-height: 28px;\r\n			float: left;\r\n			vertical-align: middle;\r\n		}\r\n		.ithemTable .siir {\r\n			line-height: 28px;\r\n			float: right;\r\n			vertical-align: middle;\r\n		}\r\n\r\n		.table_style_2 {\r\n			width: 100%;\r\n			border-collapse: collapse;\r\n		}\r\n		.table_style_2 th,\r\n		.table_style_2 td {padding: 0 12px; white-space: nowrap; word-break: keep-all;}\r\n\r\n		.table_style_2 thead {}\r\n		.table_style_2 thead th {\r\n			text-align: left;\r\n			height: 28px;\r\n			background: #fff;\r\n			font-size: 12px;\r\n			color: #000;\r\n			font-weight: bold;\r\n		}\r\n		.table_style_2 td {\r\n			font-size: 12px;\r\n			height: 26px;\r\n			line-height: normal;\r\n			vertical-align: middle;\r\n		}\r\n		.table_style_2 tr.odd td {\r\n			background:#f8f8f8;\r\n		}\r\n		.table_style_2 tr.even td {\r\n			background:#ffffff;\r\n		}\r\n		.icon-ctr-1,\r\n		.icon-ctr-2 {\r\n			display: inline-block;;\r\n			width: 18px;\r\n			height: 18px;\r\n		}\r\n\r\n		.icon-ctr-1 { background:url(../images/ctr-icons-10.png) center center no-repeat;}\r\n		.icon-ctr-2 { background:url(../images/ctr-icons-15.png) center center no-repeat;cursor:pointer; }\r\n		.tableTitle2 { background:#eeeeee; height:26px; line-height:26px; font-size:12px; padding-left:12px;}\r\n		.pumbTabs {\r\n			position: absolute;\r\n			right: 8px;\r\n			top: 0px;\r\n		}\r\n		.putab {\r\n			min-width:108px;\r\n			float: left;\r\n			margin-right:4px;\r\n			display: inline-block;\r\n			line-height: 23px;\r\n			text-align:center;\r\n			font-size:12px;\r\n			text-decoration:none;\r\n			color:#222222;\r\n			padding-bottom:6px;\r\n		}\r\n\r\n		.pumbTabs .cur {\r\n			background:url(../images/pumtabdown.gif) center bottom no-repeat;\r\n		}\r\n\r\n		.papers_1 {\r\n			text-align:center;\r\n			background: #EFEFEF;\r\n			vertical-align: middle;\r\n			padding:3px 0;\r\n		}\r\n		.papers_1 span{\r\n			padding:0px;\r\n			margin: 0px;\r\n		}\r\n		.papers_1 .pageElems {\r\n			cursor:pointer;\r\n		}\r\n		.table_style_3 table { border-collapse: separate; width:100%; border:solid 1px #d7d7d7; border-style: none solid solid none; }\r\n		.table_style_3 table th,\r\n		.table_style_3 table td {color:#666666; border:solid 1px #d7d7d7; border-style:solid none none solid; font-size:12px;  white-space:nowrap; word-break:keep-all; padding:0 10px; height:22px; line-height:normal; vertical-align:middle; text-align:left;}\r\n		.table_style_3 table thead th { height:30px; background:#bfe4ff; color:#000; border-left:solid 1px #acd3ef}\r\n		.table_style_3 table thead th:first-child { background:#fff; color:#000; border-left:solid 1px #d7d7d7; font-size:14px;}\r\n		.table_style_3 table tbody th { background:#d8efff; border-top: solid 1px #acd3ef; color:#000;}\r\n		.table_style_3 table tbody td { border:solid 1px #f0f0f0; border-style:solid none none solid;}\r\n\r\n\r\n		.ts4PadWrap { padding: 4px 25px 14px 18px;}\r\n		.table_style_4 table { width:100%; border-collapse:separate; border-spacing:0;\r\n			border:solid 1px #d7d7d7;\r\n			border-style:none solid solid none;\r\n		}\r\n\r\n		.table_style_4 table th,\r\n		.table_style_4 table td {\r\n			font-size:12px;\r\n			vertical-align:middle;\r\n			line-height:normal;\r\n			text-align:left;\r\n			padding:0 12px;\r\n			height:22px;\r\n			border:solid 1px #d7d7d7;\r\n			border-style:solid none none solid;	\r\n			background:#f3f6f8;\r\n			color:#868789;\r\n		}\r\n\r\n\r\n		.table_style_4 table thead {}\r\n		.table_style_4 table tbody {}\r\n		.table_style_4 table tfoot {}\r\n\r\n		.table_style_4 table thead th { height:29px; color:#000;}\r\n		.table_style_4 table thead th:first-child { background:#fff; font-size:14px;}\r\n		.table_style_4 table thead td { height:29px; background:#0383de; color:#fff;}\r\n\r\n		.table_style_4 table tbody td {}\r\n		.table_style_4 table tbody th {background:#efefef; color:#000;}\r\n		.table_style_4 table tbody td.total {background:#bfe4ff; color:#000; font-weight:bold;}\r\n\r\n		.table_style_4 table tfoot th {background:#0383de; color:#fff;}\r\n		.table_style_4 table tfoot td {background:#bfe4ff; color:#000; font-weight:bold;}\r\n\r\n\r\n		/*2014年3月17日 tony*/\r\n		.designer_k{\r\n			min-height: 100px;\r\n		}\r\n		.newDom .ui-resizable-handle {display:none!important;}\r\n		.newDom:hover .ui-resizable-handle {display:block!important;}\r\n		.designer_k table{\r\n		}\r\n\r\n		.designer_k .newDom {\r\n			background:#fff;\r\n			min-width:250px;\r\n			min-height:100px;\r\n			border:1px solid #c6c6c6;\r\n			position:relative;\r\n		}\r\n\r\n		.noborder{\r\n			border:none !important;\r\n		}\r\n\r\n		.designer_k .newDom:hover {\r\n			z-index:95;\r\n		}\r\n		.designer_k .newDom .table_menu{\r\n			width:100%;\r\n			font-size: 14px;\r\n			border: solid 1px #d7d7d7;\r\n			display: none;\r\n			position:absolute;\r\n			top:-1px;\r\n			height: 35px;\r\n			left:-1px;\r\n			background:#fafafa;	\r\n		}\r\n\r\n		.designer_k .newDom .table_menu span{\r\n			width:34px;\r\n			height:34px;\r\n			float:left;\r\n			border-right:1px solid #ededed;\r\n			border-left:1px solid #fff;\r\n			cursor:pointer;\r\n		}\r\n		.designer_k .newDom .table_menu em {\r\n			width:34px;\r\n			height:34px;\r\n			float:right;\r\n			cursor:pointer;\r\n			background:left top no-repeat;\r\n		}\r\n		.designer_k .newDom .table_menu .tool-ctr-close {\r\n			background-image:url(../images/newDom-Title_close-btn.gif);\r\n		}\r\n		.designer_k .newDom .table_menu .tool-ctr-close:hover { background-color:#ff6935;}\r\n		.designer_k .newDom .table_menu em:hover {background-position:left bottom;}\r\n		.designer_k .newDom .table_menu .tool-icons-1 {\r\n			background:url(../images/icon-copy.png) center center no-repeat;\r\n		}\r\n		.designer_k .newDom .table_menu span:first-child { border-left:none;}\r\n		.designer_k .newDom .table_menu span:hover {\r\n			background-color:#bfe4ff;\r\n		}\r\n		.designer_k .newDom:hover .table_menu{\r\n		/* .designer_k .newDom .table_menu{ */\r\n			display: block;\r\n		}\r\n		.designer_k .newDom .table_menu_inner {\r\n			height:34px;\r\n			border:solid 1px #fff;\r\n			border-style:solid solid none;\r\n		}\r\n		ul.ztree.zTreeDragUL {z-index:5;}\r\n\r\n		.newDom .ppdiv{position:absolute;left:0;top:0;background-color:#0073c7;width:0;height:0;z-index:6;}\r\n\r\n		.topHighLight.active{\r\n			color:red;\r\n		}\r\n		/*end*/\r\n\r\n		.table th{font-size:13px;}\r\n		.table thead th{vertical-align:bottom;background-color:#f9f9f9;background-image:-moz-linear-gradient(top, #ffffff, #f0f0f0);background-image:-webkit-gradient(linear, 0 0, 0 100%, from(#ffffff), to(#f0f0f0));background-image:-webkit-linear-gradient(top, #ffffff, #f0f0f0);background-image:-o-linear-gradient(top, #ffffff, #f0f0f0);background-image:linear-gradient(to bottom, #ffffff, #f0f0f0);background-image:-webkit-gradient(linear, left 0%, left 100%, from(#ffffff), to(#f0f0f0));background-image:-webkit-linear-gradient(top, #ffffff, 0%, #f0f0f0, 100%);background-image:-moz-linear-gradient(top, #ffffff 0%, #f0f0f0 100%);background-image:linear-gradient(to bottom, #ffffff 0%, #f0f0f0 100%);background-repeat:repeat-x;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#ffffffff\', endColorstr=\'#fff0f0f0\', GradientType=0);}\r\n		.table caption+thead tr:first-child th,.table caption+thead tr:first-child td,.table colgroup+thead tr:first-child th,.table colgroup+thead tr:first-child td,.table thead:first-child tr:first-child th,.table thead:first-child tr:first-child td{border-top:0;}\r\n		.table tbody+tbody{border-top:2px solid #dddddd;}\r\n		.table .table{background-color:#ffffff;}\r\n		.well .table{margin-bottom:0px;}\r\n		.table-condensed th,.table-condensed td{padding:4px 5px;}\r\n		.table-condensed-more th,.table-condensed-more td{padding:2px 2px 3px 3px;}\r\n		.table-transparent{background:transparent;}\r\n		.table-bordered{border:1px solid #dddddd;border-collapse:separate;*border-collapse:collapse;border-left:0;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;}.table-bordered th,.table-bordered td{border-left:none;}\r\n		.table-bordered caption+thead tr:first-child th,.table-bordered caption+tbody tr:first-child th,.table-bordered caption+tbody tr:first-child td,.table-bordered colgroup+thead tr:first-child th,.table-bordered colgroup+tbody tr:first-child th,.table-bordered colgroup+tbody tr:first-child td,.table-bordered thead:first-child tr:first-child th,.table-bordered tbody:first-child tr:first-child th,.table-bordered tbody:first-child tr:first-child td{border-top:0;}\r\n		.table-bordered thead:first-child tr:first-child>th:first-child,.table-bordered tbody:first-child tr:first-child>td:first-child,.table-bordered tbody:first-child tr:first-child>th:first-child{-webkit-border-top-left-radius:4px;-moz-border-radius-topleft:4px;border-top-left-radius:4px;}\r\n		.table-bordered thead:first-child tr:first-child>th:last-child,.table-bordered tbody:first-child tr:first-child>td:last-child,.table-bordered tbody:first-child tr:first-child>th:last-child{-webkit-border-top-right-radius:4px;-moz-border-radius-topright:4px;border-top-right-radius:4px;}\r\n		.table-bordered thead:last-child tr:last-child>th:first-child,.table-bordered tbody:last-child tr:last-child>td:first-child,.table-bordered tbody:last-child tr:last-child>th:first-child,.table-bordered tfoot:last-child tr:last-child>td:first-child,.table-bordered tfoot:last-child tr:last-child>th:first-child{-webkit-border-bottom-left-radius:4px;-moz-border-radius-bottomleft:4px;border-bottom-left-radius:4px;}\r\n		.table-bordered thead:last-child tr:last-child>th:last-child,.table-bordered tbody:last-child tr:last-child>td:last-child,.table-bordered tbody:last-child tr:last-child>th:last-child,.table-bordered tfoot:last-child tr:last-child>td:last-child,.table-bordered tfoot:last-child tr:last-child>th:last-child{-webkit-border-bottom-right-radius:4px;-moz-border-radius-bottomright:4px;border-bottom-right-radius:4px;}\r\n		.table-bordered tfoot+tbody:last-child tr:last-child td:first-child{-webkit-border-bottom-left-radius:0;-moz-border-radius-bottomleft:0;border-bottom-left-radius:0;}\r\n		.table-bordered tfoot+tbody:last-child tr:last-child td:last-child{-webkit-border-bottom-right-radius:0;-moz-border-radius-bottomright:0;border-bottom-right-radius:0;}\r\n		.table-bordered caption+thead tr:first-child th:first-child,.table-bordered caption+tbody tr:first-child td:first-child,.table-bordered colgroup+thead tr:first-child th:first-child,.table-bordered colgroup+tbody tr:first-child td:first-child{-webkit-border-top-left-radius:4px;-moz-border-radius-topleft:4px;border-top-left-radius:4px;}\r\n		.table-bordered caption+thead tr:first-child th:last-child,.table-bordered caption+tbody tr:first-child td:last-child,.table-bordered colgroup+thead tr:first-child th:last-child,.table-bordered colgroup+tbody tr:first-child td:last-child{-webkit-border-top-right-radius:4px;-moz-border-radius-topright:4px;border-top-right-radius:4px;}\r\n		.table-bordered thead th:first-child,.table-bordered tbody th:first-child,.table-bordered tbody td:first-child{border-left:1px solid #dddddd;}\r\n		.table-striped tbody>tr:nth-child(even)>td,.table-striped tbody>tr:nth-child(even)>th{background-color:#f9f9f9;}\r\n		.table-striped-two tbody>tr:nth-child(4n+3)>td,.table-striped-two tbody>tr:nth-child(4n+3)>th,.table-striped-two tbody>tr:nth-child(4n+4)>td,.table-striped-two tbody>tr:nth-child(4n+4)>th{background-color:#f9f9f9;}\r\n		.table-striped-two tbody>tr:nth-child(2n) td,.table-striped-two tbody>tr:nth-child(2n) th{border-top:none;}\r\n		table-striped-three tbody>tr:nth-child(6n+3)>td,table-striped-three tbody>tr:nth-child(6n+3)>th,table-striped-three tbody>tr:nth-child(6n+4)>td,table-striped-three tbody>tr:nth-child(6n+4)>th,table-striped-three tbody>tr:nth-child(6n+5)>td,table-striped-three tbody>tr:nth-child(6n+5)>th{background-color:#f9f9f9;}\r\n		table-striped-three tbody>tr:nth-child(3n-1) td,table-striped-three tbody>tr:nth-child(3n-1) th>tr:nth-child(3n) td,table-striped-three tbody>tr:nth-child(3n) th{border-top:none;}\r\n		.table-hover tbody tr:hover>td,.table-hover tbody tr:hover>th{background-color:#f5f5f5;}\r\n		.table td.span1,.table th.span1{float:none;width:54px;margin-left:0;}\r\n		.table td.span2,.table th.span2{float:none;width:134px;margin-left:0;}\r\n		.table td.span3,.table th.span3{float:none;width:214px;margin-left:0;}\r\n		.table td.span4,.table th.span4{float:none;width:294px;margin-left:0;}\r\n		.table td.span5,.table th.span5{float:none;width:374px;margin-left:0;}\r\n		.table td.span6,.table th.span6{float:none;width:454px;margin-left:0;}\r\n		.table td.span7,.table th.span7{float:none;width:534px;margin-left:0;}\r\n		.table td.span8,.table th.span8{float:none;width:614px;margin-left:0;}\r\n		.table td.span9,.table th.span9{float:none;width:694px;margin-left:0;}\r\n		.table td.span10,.table th.span10{float:none;width:774px;margin-left:0;}\r\n		.table td.span11,.table th.span11{float:none;width:854px;margin-left:0;}\r\n		.table td.span12,.table th.span12{float:none;width:934px;margin-left:0;}\r\n		.table tbody tr.success>td{background-color:#dff0d8;}\r\n		.table tbody tr.error>td{background-color:#f2dede;}\r\n		.table tbody tr.warning>td{background-color:#fcf8e3;}\r\n		.table tbody tr.suppressed>td{background-color:#f3eaff;}\r\n		.table tbody tr.info>td{background-color:#d9edf7;}\r\n		.table-hover tbody tr.success:hover>td{background-color:#d0e9c6;}\r\n		.table-hover tbody tr.error:hover>td{background-color:#ebcccc;}\r\n		.table-hover tbody tr.warning:hover>td{background-color:#faf2cc;}\r\n		.table-hover tbody tr.suppressed:hover>td{background-color:#e4d0ff;}\r\n		.table-hover tbody tr.info:hover>td{background-color:#c4e3f3;}\r\n		.table-monitor{width:100%;}\r\n		.well{	background-color: #edf5fa;background-image: -webkit-linear-gradient(top, #fdfefe, #edf5fa);\r\n		background-image: -moz-linear-gradient(top, #fdfefe, #edf5fa);\r\n		background-image: -ms-linear-gradient(top, #fdfefe, #edf5fa);\r\n		background-image: -o-linear-gradient(top, #fdfefe, #edf5fa);}\r\n		.progress {\r\n			height: 20px;\r\n			overflow: hidden;\r\n			background-color: #f5f5f5;\r\n			border-radius: 4px;\r\n			-webkit-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);\r\n			box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);\r\n		}\r\n		.progress-small {\r\n			height: 10px;\r\n			margin-bottom: 7px;\r\n		}\r\n		.chart-height {\r\n			position: relative;\r\n			height: 180px;\r\n		}\r\n		.progress-bar {\r\n			float: left;\r\n			width: 0;\r\n			height: 100%;\r\n			font-size: 12px;\r\n			line-height: 20px;\r\n			color: #ffffff;\r\n			text-align: center;\r\n			background-color: #428bca;\r\n			-webkit-box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);\r\n			box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);\r\n			-webkit-transition: width 0.6s ease;\r\n			transition: width 0.6s ease;\r\n		}\r\n		.progress-bar-success {\r\n		background-color: #5cb85c;\r\n		}\r\n		.panel-body {\r\n			padding: 0 0 30px;\r\n		}\r\n		.daily-stats h1.number {\r\n		  text-align: right;\r\n		  color: #a4db79;\r\n		  font-weight: 500;\r\n		  letter-spacing: 1px;\r\n		  margin-bottom: 0; }\r\n		  .daily-stats h1.number span {\r\n			color: #e6e6e6; }\r\n		  .daily-stats h1.number.primary {\r\n			color: #3784b1; }\r\n		.daily-stats p.avg, .daily-stats p.total {\r\n		  text-align: right;\r\n		  color: #3784b1;\r\n		  font-weight: 500; }\r\n		.daily-stats ul.details {\r\n		  text-align: right;\r\n		  list-style-type: none;\r\n		  margin: 0;\r\n		  padding: 0; }\r\n		  .daily-stats ul.details li {\r\n			width: 33%;\r\n			float: left;\r\n			color: #666666; }\r\n			.daily-stats ul.details li h4.num {\r\n			  font-size: 16px;\r\n			  font-weight: 500;\r\n			  display: block;\r\n			  padding: 0;\r\n			  margin: 0;\r\n			  line-height: 12px; }\r\n			  \r\n			.daily-stats ul.details li small {\r\n			  color: #b3b3b3; }\r\n		.daily-stats ul.demography {\r\n		  list-style-type: none;\r\n		  margin: 0;\r\n		  padding: 0; }\r\n		  .daily-stats ul.demography li {\r\n			width: 50%;\r\n			float: left;\r\n			color: #666666; }\r\n			.daily-stats ul.demography li i {\r\n			  font-size: 28px;\r\n			  color: #a4db79;\r\n			  float: left;\r\n			  margin-right: 10px; }\r\n			.daily-stats ul.demography li h4.num {\r\n			  margin-left: 40px;\r\n			  font-size: 16px;\r\n			  font-weight: 500;\r\n			  padding: 0;\r\n			  margin: 0;\r\n			  line-height: 21px; }\r\n			  .daily-stats ul.demography li h4.num small {\r\n				color: #b3b3b3;\r\n				display: block;\r\n				font-size: 11px; }\r\n		.daily-stats ul.min-max {\r\n		  list-style-type: none;\r\n		  margin: 0;\r\n		  padding: 0; }\r\n		  .daily-stats ul.min-max li {\r\n			width: 50%;\r\n			float: left;\r\n			color: #666666; }\r\n			.daily-stats ul.min-max li h4.num {\r\n			  font-size: 16px;\r\n			  font-weight: 500;\r\n			  padding: 0;\r\n			  margin: 0;\r\n			  line-height: 14px; }\r\n			  .daily-stats ul.min-max li h4.num small {\r\n				color: #b3b3b3;\r\n				margin-right: 5px;\r\n				font-size: 11px; }\r\n			.daily-stats ul.min-max li:last-child h4.num {\r\n			  text-align: right; }\r\n		.daily-stats .bar-graph {\r\n		  overflow: hidden; }\r\n		.table-monitor{width:100%;}\r\n		.servercur {\r\n			background-color: #0073C7 !important;\r\n			color: #FFFFFF;\r\n		}\r\n		.monitor_icon {\r\n			display: inline-block;\r\n			width: 16px;\r\n			height: 16px;\r\n			vertical-align: middle;\r\n		}\r\n		.health_icon {\r\n			display: inline-block;\r\n			width: 45px;\r\n			height: 16px;\r\n			vertical-align: middle;\r\n		}\r\n		.bottomborder{\r\n			border:0px !important;\r\n			border-bottom: solid 1px #f2f2f2 !important;\r\n		}\r\n		.notification{\r\n			z-index:100000;\r\n			position:absolute;\r\n			padding:0;\r\n			font-size:15px;\r\n			font-family:\'OpenSansRegular\';\r\n			color:#FFF;\r\n			box-shadow:0px 1px 4px 0px rgba(0,0,0,0.06);\r\n			background-image: url(\"../images/load.gif\");\r\n			text-align:center;\r\n			width:auto;\r\n			height:7px;\r\n			width:95px;\r\n		}\r\n		.designer_k{\r\n			margin:1px;\r\n		}\r\n		.designer_k td {\r\n			min-width: 50px !important;\r\n			padding:0 5px 0 5px;\r\n		}\r\n		.data_model h3{\r\n			background-color:#ffffff;\r\n		}\r\n		.s_white{\r\n			background-color:#FFFFFF !important;\r\n		}\r\n		.designer_k table thead td{\r\n			font-weight:bold;\r\n		}\r\n		thead .coltotal{\r\n			background-color: #0383de !important;\r\n		}\r\n		.rowcell .coltotal{\r\n			background-color: #bfe4ff;\r\n			font-weight:bold;\r\n			color:#5C5C5C;\r\n		}\r\n		.table_style_1 tbody tr td .setting {\r\n			display:none;\r\n		}\r\n		.table_style_1 tbody tr:hover td .setting {\r\n			display:block;\r\n		}\r\n		#design_table table thead td {text-align:center;white-space: nowrap;}\r\n		.a_open{\r\n			background-image: url(../images/open.png);\r\n			margin-top: 5px;cursor: pointer;\r\n		}\r\n		.a_close{\r\n			background-image: url(../images/close.png);\r\n			margin-top: 5px;cursor: pointer;\r\n		}\r\n		.morefloat{\r\n			background-color: #FFFFFF;\r\n			border: 1px solid #C8CBD0;\r\n			box-shadow: 0 2px 5px rgba(55, 55, 55, 0.3);\r\n			display: none;\r\n			left: -2px;\r\n			margin-left:0px;\r\n			max-height: 145px;\r\n			overflow-y: auto;\r\n			position: absolute;\r\n			top: 22px;\r\n			z-index: 100;\r\n			\r\n		}\r\n		.moremain{\r\n			cursor: pointer;\r\n			float: left;\r\n			height: 22px;\r\n			margin-right: 9px;\r\n		}\r\n\r\n		.moremain em {\r\n			color: #000000;\r\n			display: inline-block;\r\n			font: 12px/22px \'Microsoft Yahei\';\r\n			height: 22px;\r\n			padding: 0 15px 0 0;\r\n		}\r\n		.moremain ul li {\r\n			background: none repeat scroll 0 0 #FFFFFF;\r\n			color: #666666;\r\n			cursor: pointer;\r\n			display: block;\r\n			font: 12px/24px \'Microsoft Yahei\';\r\n			height: 24px;\r\n			overflow: hidden;\r\n			padding-left: 6px;\r\n			padding-right: 30px;\r\n			text-overflow: ellipsis;\r\n			white-space: nowrap;\r\n		}\r\n		.morepannel{\r\n			background: url(\"../images/arrowIco4.png\") no-repeat scroll right center rgba(0, 0, 0, 0);\r\n			float: right;\r\n			height: 22px;\r\n			position: relative;\r\n		}\r\n		.templet_nocur{\r\n			border: solid 2px #EEEEEE;\r\n		}\r\n		.templet_cur{\r\n			border: solid 2px #0099FF;\r\n		}\r\n		.cr-target-toggle {\r\n			width: 49px;\r\n			height: 23px;\r\n			padding: 0;\r\n			cursor: pointer;\r\n			background-image: url(\'/assets/images/condition-toggle.png\');\r\n			background-repeat: no-repeat;\r\n		}\r\n		.cr-target-toggle-text {\r\n			background-position: 0 -25px;\r\n		}\r\n	</style>\r\n</head>\r\n	<body>\r\n		<style>\r\n			/*** 模型的样式表 ***/\r\n			<#if reportModel?? && reportModel.viewtype != \"view\">\r\n			.cz_<#if reportModel??>${reportModel.id}</#if>{\r\n				<#if reportModel?? && reportModel.height??>height:${reportModel.height}px;<#else>height:220px;</#if>width:<#if reportModel??>${reportModel.width!\'400\'}px</#if>; margin: 0 auto;\r\n			}  \r\n			.cz_<#if reportModel??>${reportModel.id}</#if>_table{\r\n				padding:5px;margin-left: 0px; margin-right: 0px; height: 225px;overflow:auto;\r\n			}\r\n			</#if>\r\n			<#if reportModel?? && reportModel.styles??>\r\n			<#list reportModel.styles as style>\r\n			<#if style.pline??  && style.pline == \"0\">\r\n			.cz_${style.dataid}_even{\r\n				${style.style!\'\'}\r\n			}\r\n			<#elseif style.pline??  && style.pline == \"1\">\r\n			.cz_${style.dataid}_odd{\r\n				${style.style!\'\'}\r\n			}\r\n			<#else>\r\n			.cz_${style.dataid}{\r\n				${style.style!\'\'}\r\n			}\r\n			</#if>\r\n			</#list>\r\n			</#if>\r\n			td{white-space: nowrap;}\r\n		</style>\r\n		<div class=\"container rs_layout\" wrapper=\"true\"><!-- Everything started here -->\r\n			<div class=\"rs_layout_center reTabcon\">\r\n				<div class=\"rs_layout show\">\r\n					<div class=\"rs_layout_top breadCrumbWrap\" style=\"border:0px;height:50px;text-align:center;font-weight:bold;font-size:18px\">\r\n						   ${report.name!\'\'}\r\n					</div>\r\n\r\n					<div class=\"rs_layout_center\">\r\n						<#list filters as filter> 										\r\n						<#if filter.funtype?? && filter.funtype==\"filter\" && filter.request == false>\r\n						<#assign hasFilter = true />\r\n						<div class=\"f_slc\" style=\"border:1px solid #d9d9d9;\">\r\n							<div class=\"filter_input filter_icon\">\r\n								<span class=\"filter_label nrspan\" style=\"padding-top:2px;padding-right:3px;\">\r\n									<#if filter.name??>${filter.name!\'\'}<#else>${filter.dataname!\'\'}</#if>				\r\n									<#if filter.comparetype?? && filter.comparetype=\"not\">\r\n										（不等于）\r\n									</#if>\r\n									<#if filter.valuefiltertype?? && filter.valuefiltertype == \'range\'>\r\n									从：\r\n									<#else>\r\n									：\r\n									</#if>\r\n								</span>\r\n\r\n								<#assign inlineTemplate = filter.defaultvalue?interpret>\r\n								<#if filter.modeltype?? && filter.modeltype == \"text\" >\r\n									\r\n\r\n									<#if filter.valuefiltertype?? && filter.valuefiltertype == \'range\'>\r\n									\r\n										<#if filter.requestendvalue??>${filter.requeststartvalue}<#else><@inlineTemplate /></#if>\r\n\r\n										<span class=\"filter_label nrspan\" style=\"padding-top:2px;padding-right:3px;\">\r\n										到：	\r\n										</span>\r\n										<#if filter.requestendvalue??>${filter.requestendvalue}<#else><@inlineTemplate /></#if>\r\n\r\n									<#else>\r\n										<#if filter.requestvalue??>${filter.requestvalue}<#else><@inlineTemplate /></#if>\r\n									</#if>\r\n								\r\n								<#elseif filter.modeltype?? && filter.modeltype == \"date\" >\r\n\r\n									<#if filter.valuefiltertype?? && filter.valuefiltertype == \'range\'>\r\n										<#if filter.requeststartvalue??>${filter.requeststartvalue}<#else><@inlineTemplate /></#if>\r\n										<span class=\"filter_label nrspan\" style=\"padding-top:2px;padding-right:3px;\">\r\n										到：	\r\n										</span>\r\n											<#if filter.requestendvalue??>${filter.requestendvalue}<#else><@inlineTemplate /></#if>\r\n										<#else>\r\n											<#assign defaultValueTemplate = filter.curvalue?interpret>\r\n											<@defaultValueTemplate/>\r\n									</#if>\r\n								<#elseif filter.modeltype?? && filter.modeltype == \"sigsel\" >\r\n									<#if filter.valuefiltertype?? && filter.valuefiltertype == \'range\'>	   <!-- 日期 范围查询  -->\r\n										<#if filter.reportData?? && filter.reportData.row?? && filter.reportData.row.title??>\r\n											<#list filter.reportData.row.title as titles>\r\n												<#list titles as title>\r\n													<#if filter.requeststartvalue?? && (filter.requeststartvalue == title.name || filter.requeststartvalue == \'[\'+filter.dataname+\'].\'+title.cubeLevel)><#if title.cubeLevel!=\'\'>[${filter.dataname!\'\'}].${title.cubeLevel!\'\'}<#else>${title.name!\'\'}</#if></#if>\r\n												</#list>	\r\n											</#list>\r\n										</#if>\r\n\r\n										<span class=\"filter_label nrspan\" style=\"padding-top:2px;padding-right:3px;\">\r\n										到：	\r\n										</span>\r\n										<#if filter.requestendvalue?? && (filter.requestendvalue == title.name || filter.requestendvalue == \'[\'+filter.dataname+\'].\'+title.cubeLevel)>\r\n											<#if title.cubeLevel!=\'\'>[${filter.dataname!\'\'}].${title.cubeLevel!\'\'}<#else>${title.name!\'\'}</#if>\r\n										</#if>\r\n									<#else>	 <!-- 日期 比较查询  -->\r\n										<#if filter.curvalue?? && (filter.curvalue == title.name || filter.curvalue == \'[\'+filter.dataname+\'].\'+title.cubeLevel)>\r\n											<#if title.cubeLevel!=\'\'>[${filter.dataname!\'\'}].${title.cubeLevel!\'\'}<#else>${title.name!\'\'}</#if>\r\n										</#if>\r\n										\r\n									</#if>\r\n								<#elseif filter.modeltype?? && filter.modeltype == \"select\" >\r\n								<span class=\"input_wrap nrspan\" id=\"cubedic\" style=\"padding:0px;\">\r\n									<#assign inlineTemplate = filter.defaultvalue?interpret>\r\n									<@inlineTemplate/>\r\n								</span>\r\n								</#if>\r\n							</div> 		\r\n						</div>\r\n						</#if>\r\n						</#list> 	\r\n						<div class=\"shrinkage\" style=\"border-top:solid 2px #d8af84;\">\r\n							\r\n						</div>\r\n						<div class=\"designer_k noborder cz_<#if reportModel??>${reportModel.id}</#if>\" style=\"overflow:auto;padding:5px 0px 5px 0px;\" id=\"design_table\" >\r\n							<#if reportModel??&&reportModel.viewtype?? && reportModel.viewtype == \"view\">\r\n								<#if reportModel??>\r\n								<table class=\"selectableTable <#if reportModel?? && reportModel.viewtype == \'view\'>cz_${reportModel.id}</#if>\" data-id=\"${reportModel.id!\'\'}\">\r\n								<thead>\r\n								<#if reportModel?? && reportModel.reportData?? && reportModel.reportData.col?? && reportModel.reportData.col.title??>\r\n								<#list reportModel.reportData.col.title as tlist>\r\n								<tr class=\"sortableTr\">\r\n								<#if reportModel.reportData.col.title?size gt 1 && tlist_index == 0 && reportModel.reportData.row.title?size gt 0>\r\n								<td align=\"center\" colspan=\"${reportModel.reportData.row.title?size}\" rowspan=\"${reportModel.reportData.col.title?size-1}\" class=\"s_blue\"></td>	\r\n								</#if>\r\n								<#if (tlist_index+1) == reportModel.reportData.col.title?size && reportModel.reportData.row?? && reportModel.reportData.row.firstTitle??>\r\n								<#list reportModel.reportData.row.firstTitle as first>\r\n								<td align=\"center\" data-title=\"${first.name}\" data-flag=\"dim\">\r\n								${first.rename!first.name!\'\'}\r\n								</td>\r\n								</#list>\r\n								</#if>\r\n								<#if tlist??>\r\n								<#list tlist as tl>\r\n								<td  align=\"center\"  <#if  tl.leveltype?? && tl.leveltype == \"newcol\">rowspan=\"${reportModel.reportData.col.title?size}\"</#if> colspan=\"${tl.colspan}\" >\r\n								${tl.rename!tl.name!\"\"}		\r\n								</td>\r\n								</#list>\r\n								</#if>\r\n								</tr>\r\n								</#list>\r\n								</#if>\r\n								</thead>\r\n								<tbody>\r\n								<#if reportModel.reportData?? && reportModel.reportData.data??>\r\n								<#list reportModel.reportData.data as values>\r\n								<tr class=\"rowcell\">	  \r\n								<#if reportModel.reportData.row?? && reportModel.reportData.row.title?? && reportModel.reportData.row.title?size gt 0>\r\n								<#list reportModel.reportData.row.title as tl>\r\n								<#assign rows = 0 >\r\n								<#list tl as title>\r\n								<#if title??>\r\n								<#if rows == values_index && title.name != \"TOTAL_TEMP\">\r\n								<td nowrap=\"nowrap\" align=\"center\" class=\"blue_k <#if title.total == true>total</#if>\" rowspan=\"${title.rowspan!\'0\'}\" <#if title.colspan gt 1>colspan=\"${title.colspan}\"</#if>>${title.formatName!\'\'}</td>\r\n								<#if title.valueData??>\r\n								<#list title.valueData as value>\r\n								<#if value.merge==false>\r\n								<td rowspan=\"${value.rowspan}\" colspan=\"${value.colspan}\" align=\"center\"  class=\"measure ${value.vtclass!\'\'} <#if (values_index+1) % 2 == 1>cz_${reportModel.id}_odd cz_${value.dataid}_odd<#else>cz_${reportModel.id}_even cz_${value.dataid}_even</#if>  cz_${reportModel.id} cz_${value.dataid!\'\'}\" nowrap=\"nowrap\" <#if value.cellmergeid??>data-cellmerge=\"${value.cellmergeid}\"</#if>>${value.foramatValue!\'\'}</td>\r\n								</#if>\r\n								</#list>\r\n								</#if>\r\n								</#if>\r\n								<#assign rows = rows + title.rowspan>\r\n								</#if>\r\n								</#list>\r\n								</#list>\r\n								<#else>\r\n								<#list values as value>\r\n								<td class=\"row ${value.vtclass!\'\'} <#if (values_index+1) % 2 == 1>cz_${reportModel.id}_odd cz_${value.dataid}_odd<#else>cz_${reportModel.id}_even cz_${value.dataid}_even</#if>  cz_${reportModel.id} cz_${value.dataid!\'\'}\" style =\"text-align: right;<#if value.valueType?? && value.valueType == \'total\'>background-color:#c5daed;</#if>\">${value.foramatValue!\'\'}</td>\r\n								</#list>\r\n								</#if>\r\n								</tr>\r\n								</#list>\r\n								</#if>\r\n								</tbody>\r\n								</table>\r\n								</#if>\r\n							</#if>\r\n						</div>\r\n					</div>\r\n				</div>					\r\n			</div>\r\n		</div>\r\n	</body>\r\n</html>', 'system', 'rivues', '', null, null, null, null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('402881e43da5225d013da532ea880001', 'R3_CUBE_DATA', '', 'cube_data', 'R3_CUBE_DATA', '2014-03-10 12:34:31', null, '<?xml version=\'1.0\' ?>\r\n<Schema name=\'${name}\'>\r\n	<#if cubeList??>\r\n    <#list cubeList as cube>\r\n    <#if !cube.modeltype?? || cube.modeltype!=\"1\">\r\n    <Cube name=\'${cube.name}\'>\r\n		<#if cube.sql?? && cube.sql?length gt 0>\r\n		<View alias=\'${cube.table?upper_case}\'>\r\n                        <SQL>\r\n                                ${(cube.sql!\'\')?html}\r\n                        </SQL>\r\n                </View>\r\n		<#else>\r\n		<Table name=\'${cube.table?upper_case}\' />\r\n                </#if>\r\n		\r\n		<#if cube.dimension??>\r\n			<#list cube.dimension as dimension>\r\n			<Dimension name=\'${dimension.name}\'>\r\n				<Hierarchy hasAll=\'true\' <#if dimension.allmembername?? && dimension.allmembername?length gt 0>allMemberName=\'${dimension.allmembername}\'</#if>>\r\n					<#if dimension.cubeLevel??>\r\n					<#list dimension.cubeLevel as level>\r\n					<Level name=\'${level.name}\' <#if level.leveltype?? && level.leveltype?length gt 0>levelType=\'${level.leveltype!\"\"}\'</#if> column=\'DIM_${level.nameAlias?upper_case}\' uniqueMembers=\'${level.uniquemembers?string}\' <#if level.type??>type=\'${level.type}\'</#if> <#if level.parameters??>${level.parameters!\"\"}</#if>/>\r\n					</#list>\r\n					</#if>\r\n				</Hierarchy>\r\n			</Dimension>        \r\n			</#list>\r\n		</#if>\r\n\r\n		<#if express??>\r\n			<#list express as dimension>\r\n			<#if dimension.child == true>\r\n			<Dimension name=\'${dimension.name}\'>\r\n				<Hierarchy hasAll=\'true\' <#if dimension.allmembername?? && dimension.allmembername?length gt 0>allMemberName=\'${dimension.allmembername}\'</#if>>\r\n					<#if dimension.cubeLevel??>\r\n					<#list dimension.cubeLevel as level>\r\n					<Level name=\'${level.name}\' <#if level.leveltype?? && level.leveltype?length gt 0>levelType=\'${level.leveltype!\"\"}\'</#if> column=\'DIM_${level.nameAlias?upper_case}\' uniqueMembers=\'${level.uniquemembers?string}\' <#if level.type??>type=\'${level.type}\'</#if> <#if level.parameters??>${level.parameters!\"\"}</#if>/>\r\n					</#list>\r\n					</#if>\r\n				</Hierarchy>\r\n			</Dimension>\r\n			</#if>\r\n			</#list>\r\n		</#if>\r\n\r\n		<#if cube.measure??>\r\n			<#list cube.measure as measure>\r\n			<#if measure.calculatedmember != true>\r\n			<Measure name=\'${measure.name}\' column=\'MEA_${measure.nameAlias?upper_case}\' aggregator=\'<#if measure.aggregator=\"count\" || measure.aggregator=\"distinct-count\">sum<#else>${measure.aggregator}</#if>\' formatString=\'${measure.formatstring!\"####\"}\'>\r\n                </Measure>\r\n			</#if>\r\n			</#list>\r\n		</#if>\r\n\r\n		<#if cube.measure??>\r\n			<#list cube.measure as measure>\r\n			<#if measure.calculatedmember == true>\r\n			<CalculatedMember name=\'${measure.name}\' dimension=\'Measures\' <#if measure.parameters??>${measure.parameters!\"\"}</#if>>\r\n				 <#if measure.attribue?? && measure.attribue?length gt 0>${measure.attribue!\"\"}</#if>\r\n			</CalculatedMember>\r\n			</#if>\r\n			</#list>\r\n		</#if>\r\n		</Cube>\r\n		</#if>\r\n		</#list>\r\n\r\n		<#list cubeList as cube>\r\n		<#if cube.modeltype?? && cube.modeltype==\"1\">\r\n		<VirtualCube name=\'${cube.name}\'>\r\n			<#if cube.dimension??>\r\n			<#list cube.dimension as dimension>\r\n			<#list cubeList as cube>\r\n			<#if cube.id == dimension.dim.cubeid>\r\n			<VirtualCubeDimension cubeName=\'${cube.name}\' name=\'${dimension.dim.name!\"\"}\'/>\r\n			<#break/>\r\n			</#if>\r\n			</#list>\r\n			</#list>\r\n			</#if>\r\n\r\n			<#if cube.measure??>\r\n			<#list cube.measure as measure>\r\n			<#if measure.calculatedmember != true>\r\n			<#list cubeList as cube>\r\n			<#if measure.measure?? && cube.id == measure.measure.cubeid>\r\n			<VirtualCubeMeasure cubeName=\'${cube.name!\"\"}\' name=\'[Measures].[${measure.measure.name!\"\"}]\'/>\r\n			<#break/>\r\n			</#if>\r\n			</#list>			\r\n			</#if>\r\n			</#list>\r\n\r\n			<#list cube.measure as measure>\r\n			<#if measure.calculatedmember == true>\r\n			<CalculatedMember name=\'${measure.name}\' dimension=\"Measures\" <#if measure.parameters??>${measure.parameters!\"\"}</#if>>\r\n				<#if measure.attribue?? && measure.attribue?length gt 0>${measure.attribue!\"\"}</#if>\r\n			  </CalculatedMember>	\r\n			</#if>\r\n			</#list>\r\n			</#if>\r\n		</VirtualCube>\r\n		</#if>\r\n		</#list>\r\n\r\n    </#if>\r\n    <UserDefinedFunction name=\"compare\" className=\"com.rivues.util.bi.StrCompareFunction\"/>\r\n</Schema>\r\n', 'system', 'rivues', null, null, null, null, null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('402881e74874c22b014874e04561020d', '中国地图', '中国地图，包含34个省市级地图数据', 'chart', null, '2014-09-14 23:55:27', null, '<script language=\"javascript\">\r\n	$(function () {\r\n		var series_${dt.id!\'\'} = [\r\n			<#if pkg?? && dt.chart?? && dt.chart.series??>\r\n			<#list dt.chart.series as series>\r\n			<#if series_index gt 0>,</#if>{\r\n				name: \'${series.name!\"\"}\',\r\n				type: \'map\',\r\n				mapType: \'china\',\r\n				roam: false,\r\n				itemStyle:{\r\n					normal:{label:{show:true}},\r\n					emphasis:{label:{show:true}}\r\n				},\r\n				data: [\r\n					<#if reportModel.reportData?? && reportModel.reportData.data??>\r\n					<#list series.values(reportModel.reportData) as value>\r\n						<#if value_index gt 0>,</#if>{name:\'<#if value.row?? && value.row.parent??>${value.row.parent.name!\'\'}</#if>\', value:${value.value!\'0\'}}\r\n					</#list>\r\n					</#if>\r\n				]\r\n			}\r\n			</#list>\r\n			</#if>\r\n		];\r\n		ChartAction.mapChart(\'chart_${dt.id!\'\'}\', \'<#if reportModel.displaytitle == true>${reportModel.title!report.name!\"\"}</#if>\' , series_${dt.id!\'\'});\r\n	});\r\n</script>', 'chart', 'rivues', '/assets/images/templeticon/map.png', null, null, '402881e74874c22b014874df8e6d01fa', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('4028ce9a4893cc42014893fe9df6042d', '漏斗图', '漏斗图', 'chart', null, '2014-09-21 00:56:50', null, '<script language=\"javascript\">\r\n	$(function () {\r\n		var series_${dt.id!\'\'} = [\r\n			<#if pkg?? && dt.chart?? && dt.chart.series??>\r\n			<#list dt.chart.series as series>\r\n			<#if series_index gt 0>,</#if>{\r\n				name: \'${series.name!\"\"}\',\r\n				data: [\r\n				    <#if reportModel.reportData?? && reportModel.reportData.data??>\r\n					<#list series.values(reportModel.reportData) as value>\r\n						<#if value_index gt 0>,</#if>[\'<#if value.row?? && value.row.parent??>${value.row.parent.name!\'\'}</#if>\', ${value.value!\'0\'}]\r\n					</#list>\r\n					</#if>\r\n				]\r\n			}\r\n			</#list>\r\n			</#if>\r\n		];\r\n		ChartAction.funnelChart(\'#chart_${dt.id!\'\'}\', \'\', \'<#if reportModel.displaytitle == true>${reportModel.title!report.name!\"\"}</#if>\' , series_${dt.id!\'\'});\r\n	});\r\n</script>', 'chart', 'rivues', '/assets/images/templeticon/funnel.png', null, null, '4028ce9a4893cc42014893fbebd30400', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae64627cd190146283307a006d2', '柱形图', '数据序列显示成不同的彩色柱形图组。每个柱形图的顶部表示数据列值。此图表比较实际值。', 'chart', null, '2014-08-07 14:58:08', null, '<script language=\"javascript\">\r\n	$(function() {\r\n	    $(\'#chart_${dt.id!\'\'}\').highcharts({\r\n		chart: {\r\n		    type: \'column\',\r\n		    plotBackgroundColor: null,\r\n			plotBorderWidth: 1,//null,\r\n			plotShadow: false\r\n		},\r\n		title: {\r\n		    text: \'<#if reportModel.displaytitle == true>${reportModel.title!report.name!\"\"}</#if>\'\r\n		},\r\n		xAxis: {\r\n			type: \'category\',\r\n			labels: {\r\n				rotation: -45,\r\n				style: {\r\n					fontSize: \'13px\',\r\n					fontFamily: \'Verdana, sans-serif\'\r\n				}\r\n			}\r\n		},\r\n		yAxis: {\r\n			min: 0,\r\n			title: {\r\n				text: \'<#if reportModel?? && reportModel.chart?? && reportModel.chart.yaxis_title??>${reportModel.chart.yaxis_title}</#if>\'\r\n			}\r\n		},\r\n		tooltip: {\r\n			pointFormat: \'{series.name}: <b>{point.y:.1f}</b>\'\r\n		},\r\n		legend: {\r\n			enabled: false\r\n		    },\r\n		exporting: {\r\n			enabled: false\r\n		},\r\n		credits:{\r\n			enabled:false\r\n		},\r\n		series: [\r\n			<#if pkg?? && dt.chart?? && dt.chart.series??>\r\n			<#list dt.chart.series as series>			\r\n			<#if series_index gt 0>,</#if>\r\n			{\r\n				name: \'${series.name!\"\"}\',\r\n				<#if series.chartype??>type: \'${series.chartype!\"\"}\',</#if>\r\n				<#if series.left?? && series.top??>center: [${series.left}, ${series.top}],</#if>\r\n				<#if series.size??>size: ${series.size},</#if>\r\n				data: [\r\n				    <#if reportModel.reportData?? && reportModel.reportData.data??>\r\n					<#list series.values(reportModel.reportData) as value>\r\n						<#if value_index gt 0>,</#if>[\'<#if value.row?? && value.row.parent??>${value.row.parent.name!\'\'}</#if>\', ${value.value!\'0\'}]\r\n					</#list>\r\n					</#if>\r\n				],\r\n				dataLabels: {\r\n				    enabled: <#if series.datalabel?? && series.datalabel == true>true<#else>false</#if>,\r\n				    rotation: 90,\r\n				    color: \'#000000\',\r\n				    align: \'right\',\r\n				    x: -5,\r\n				    y: -5,\r\n				    style: {\r\n						fontSize: \'13px\'\r\n				    }\r\n				}\r\n			}\r\n			</#list>\r\n			</#if>\r\n		    \r\n		]\r\n	    });\r\n	});\r\n</script>', 'chart', 'rivues', '/assets/images/templeticon/type_column_clustered_flat.gif', null, null, '8a8a8ae64627cd190146282577df05e4', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae64627cd1901462835224b06f2', '三维柱形图', '数据序列显示成已加深的不同彩色柱形图组。每个柱形图的顶部表示数据序列的值。此图表比较实际值。', 'chart', null, '2014-08-07 14:59:05', null, '<script language=\"javascript\">\r\n	$(function() {\r\n	    $(\'#chart_${reportModel.id!\'\'}\').highcharts({\r\n		chart: {\r\n		    type: \'column\',\r\n		    options3d: {\r\n    			enabled: true,\r\n    			alpha: 20,\r\n    			beta: 10,\r\n    			depth: 50,\r\n    			viewDistance: 125\r\n		    }\r\n		},\r\n		title: {\r\n		    text: \'<#if reportModel.displaytitle == true>${reportModel.title!report.name!\"\"}</#if>\'\r\n		},\r\n		xAxis: {\r\n			type: \'category\',\r\n			labels: {\r\n				rotation: -45,\r\n				style: {\r\n					fontSize: \'13px\',\r\n					fontFamily: \'Verdana, sans-serif\'\r\n				}\r\n			}\r\n		},\r\n		yAxis: {\r\n			min: 0,\r\n			title: {\r\n				text: \'<#if reportModel?? && reportModel.chart?? && reportModel.chart.yaxis_title??>${reportModel.chart.yaxis_title}</#if>\'\r\n			}\r\n		},\r\n		legend: {\r\n			enabled: false\r\n		},\r\n		tooltip: {\r\n			pointFormat: \'{series.name}: <b>{point.y:.1f}</b>\',\r\n		},\r\n		exporting: {\r\n			enabled: false\r\n		},\r\n		credits:{\r\n			enabled:false\r\n		},\r\n		series: [\r\n			<#if reportModel?? && reportModel.chart?? && reportModel.chart.series??>\r\n			<#list reportModel.chart.series as series>			\r\n			<#if series_index gt 0>,</#if>{\r\n			name: \'${series.name!\"\"}\',\r\n			data: [\r\n			    <#if reportData?? && reportData.data??>\r\n			    <#list reportData.data as data>\r\n				<#list data as value>\r\n				     <#if series.name == value.name>\r\n				     <#if data_index gt 0>,</#if>[\'<#if value.row?? && value.row.parent??>${value.row.parent.name!\'\'}</#if>\', ${value.value!\'0\'}]\r\n				     <#break/>\r\n				     </#if>\r\n				</#list>\r\n			    </#list>\r\n			    </#if>\r\n			],			\r\n			dataLabels: {\r\n			    enabled: true,\r\n			    rotation: 90,\r\n			    color: \'#000000\',\r\n			    align: \'right\',\r\n			    x: -5,\r\n			    y: -5,\r\n			    style: {\r\n				fontSize: \'13px\'\r\n			    }\r\n			}\r\n			</#list>\r\n			</#if>\r\n		    }\r\n		]\r\n	    });\r\n	});\r\n</script>', 'chart', 'rivues', '/assets/images/templeticon/type_column_clustered.gif', null, null, '8a8a8ae64627cd190146282577df05e4', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae64627cd190146284622d407d9', '堆积柱形图', '数据序列以彩色分段形式堆积在柱形图中。每个柱形图的顶部表示序列总值，此图表比较各个值占序列总值的比例。', 'chart', null, '2014-08-07 14:59:04', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_column_stacked_flat.gif', null, null, '8a8a8ae64627cd190146282577df05e4', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae64627cd1901462847a3b307f1', '三维堆积柱形图', '数据序列以彩色分段形式堆积在已加深的柱形图中，每个柱形图的顶部表示序列总值，此图表比较各个值占序列总值的比例。', 'chart', null, '2014-08-07 14:59:03', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_column_stacked.gif', null, null, '8a8a8ae64627cd190146282577df05e4', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae64627cd1901462849891f080e', '百分堆积柱形图', '数据序列以彩色分段形式堆积在大小相同的柱形图中，每个柱形图表示序列总值的百分之百，此图表比较各个值占总值的百分比。', 'chart', null, '2014-08-07 14:59:02', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_column_percent_flat.gif', null, null, '8a8a8ae64627cd190146282577df05e4', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae64627cd190146284b8d89082d', '三维百分堆积柱形图', '数据序列以彩色分段形式堆积在已加深且大小相同的柱形图中，每个柱形图表示序列总值的百分之百，此图表比较各个值占总值的百分比。', 'chart', null, '2014-08-07 14:59:02', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_column_percent.gif', null, null, '8a8a8ae64627cd190146282577df05e4', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae64627cd190146284d80c6084b', 'Marimekko图', '数据序列显示成彩色堆积面积，其中，列的宽度与列值的合计成正比，各个段的高度是其各自列合计值的百分比。', 'chart', null, '2014-08-07 14:59:01', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_column_marimekko.gif', null, null, '8a8a8ae64627cd190146282577df05e4', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae64627cd190146284f9f26086a', '三维轴柱形图', '具有三个轴的三位柱形图，它沿着第三个轴均匀地分布数据序列。此图表根据一个或多个度量以三维方式绘出多个变量。', 'chart', null, '2014-08-07 14:59:00', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_column_3daxis.gif', null, null, '8a8a8ae64627cd190146282577df05e4', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae946285fa50146287ab4df01f0', '条形图', '数据序列显示成不同的彩色条形图组，每个条形图的末端表示序列值，此图表比较实际值。', 'chart', null, '2014-08-07 14:59:00', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_bar_clustered_flat.gif', null, null, '8a8a8ae64627cd190146282604c805f0', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae946285fa50146287c7507020b', '三维条形图', '数据序列显示成已加深的不同彩色条形图组，每个条形图的末端表示序列值，此图表比较实际值。', 'chart', null, '2014-08-07 14:59:00', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_bar_clustered.gif', null, null, '8a8a8ae64627cd190146282604c805f0', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae946285fa50146287ea6cf022c', '堆积条形图', '数据序列以彩色分段形式堆积在水平条形图中，每个条形图的末端表示数据序列的总值，此图表比较各个值占序列总值的比例。', 'chart', null, '2014-08-07 14:58:58', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_bar_stacked_flat.gif', null, null, '8a8a8ae64627cd190146282604c805f0', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae946285fa501462880df9d024d', '三维堆积条形图', '数据序列以彩色分段形式堆积在已加深的水平条形图中，每个条形图的末端表示数据序列的总值，此图表比较各个值占序列总值的比例。', 'chart', null, '2014-08-07 14:58:59', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_bar_stacked.gif', null, null, '8a8a8ae64627cd190146282604c805f0', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae946285fa501462883263f026f', '百分堆积条形图', '数据序列以彩色分段形式堆积在大小相同的水平条形图中，每个条形图表示序列总值的百分之百，此图表比较各个值占总值的百分比。', 'chart', null, '2014-08-07 14:58:57', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_bar_percent_flat.gif', null, null, '8a8a8ae64627cd190146282604c805f0', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae946285fa501462884d43d0289', '三维百分堆积条形图', '数据序列以彩色分段形式堆积在已加深且大小相同的水平条形图中，每个条形图表示序列总值的百分之百，此图表比较各个值占总值的百分比。', 'chart', null, '2014-08-07 14:58:57', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_bar_percent.gif', null, null, '8a8a8ae64627cd190146282604c805f0', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462be73101462c19a8d502a7', '渐进柱形图', '柱形图用于绘出一个度量在单个数据序列中的增长及减少，第一个柱形图的顶部表示第二个柱形图的起始值。', 'chart', null, '2014-08-07 14:58:57', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_column_progressive_flat.gif', null, null, '8a8a8ae64627cd19014628271dce0603', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462be73101462c1b144302bd', '三维渐进柱形图', '已加深的柱形图将绘出一个度量在单个数据序列上的增长及减少，第一个柱形图的顶部表示第二个柱形图的起始值。', 'chart', null, '2014-08-07 14:58:56', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_column_progressive.gif', null, null, '8a8a8ae64627cd19014628271dce0603', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462be73101462c1c71ec02d3', '渐进条形图', '已加深的条形图将绘出一个度量在单个数据序列的增长及减少，第一个条形图的顶部表示第二个条形图的起始值。', 'chart', null, '2014-08-07 14:58:55', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_bar_progressive_flat.gif', null, null, '8a8a8ae64627cd19014628271dce0603', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462be73101462c1de9c902eb', '三维渐进条形图', '已加深的条形图将绘出一个度量在单个数据序列的增长及减少，第一个条形图的顶部表示第二个条形图的起始值。', 'chart', null, '2014-08-07 14:58:55', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_bar_progressive.gif', null, null, '8a8a8ae64627cd19014628271dce0603', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c51dc02012e', '堆积排列柱形图', '堆积柱形图组合了表示序列值累加合计的线条。', 'chart', null, '2014-08-07 14:58:52', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_pareto_column_stacked_flat.gif', null, null, '8a8a8ae64627cd19014628278c74060d', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c527a53013a', '三维堆积排列柱形图', '已加深的堆积柱形图组合了表示序列值累加合计的线条。', 'chart', null, '2014-08-07 14:58:51', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_pareto_column_stacked.gif', null, null, '8a8a8ae64627cd19014628278c74060d', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c52e0850144', '堆积排列条形图', '堆积条形图组合了表示序列值累加合计的线条。', 'chart', null, '2014-08-07 14:58:51', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_pareto_bar_stacked_flat.gif', null, null, '8a8a8ae64627cd19014628278c74060d', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5348c8014d', '三维堆积排列条形图', '已加深的堆积条形图组合了表示序列值累加合计的线条。', 'chart', null, '2014-08-07 14:58:50', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_pareto_bar_stacked.gif', null, null, '8a8a8ae64627cd19014628278c74060d', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c559df8017d', '带标记的折线图', '多个数据序列在每个数据值处显示成带标记的彩色折线图。 此图表比较实际值。', 'chart', null, '2014-08-07 14:58:50', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_line_clustered_flat_markers.gif', null, null, '8a8a8ae64627cd1901462827cee60615', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5684cd018f', '折线图', '多个数据序列显示成彩色折线图。 此图表用于比较实际值。', 'chart', null, '2014-08-07 14:58:49', null, '<script language=\"javascript\">\r\n	$(function () {\r\n		var xAxis_${dt.id!\'\'} = {\r\n			categories:[<#if pkg?? && dt.chart?? && dt.chart.series??><#list dt.chart.series as series><#if series_index ==0 ><#if reportModel.reportData?? && reportModel.reportData.data??><#list series.values(reportModel.reportData) as value><#if value_index gt 0>,</#if>\'<#if value.row?? && value.row.parent??>${value.row.parent.name!\'\'}</#if>\'</#list></#if></#if></#list></#if>]\r\n			,labels: {\r\n				rotation: -45,\r\n				style: {\r\n					fontSize: \'13px\',\r\n					fontFamily: \'Verdana, sans-serif\'\r\n				}\r\n			}\r\n		};\r\n		var series_${dt.id!\'\'} = [\r\n			<#if pkg?? && dt.chart?? && dt.chart.series??>\r\n			<#list dt.chart.series as series>\r\n			<#if series_index gt 0>,</#if>{\r\n				name: \'${series.name!\"\"}\',\r\n				data: [\r\n				    <#if reportModel.reportData?? && reportModel.reportData.data??><#list series.values(reportModel.reportData) as value><#if value_index gt 0>,</#if>${value.value!\'0\'}</#list></#if>\r\n				]\r\n			}\r\n			</#list>\r\n			</#if>\r\n		];\r\n		ChartAction.lineChart(\'#chart_${dt.id!\'\'}\', xAxis_${dt.id!\'\'} , \'<#if reportModel.displaytitle == true>${reportModel.title!report.name!\"\"}</#if>\' , series_${dt.id!\'\'});\r\n	});\r\n</script>', 'chart', 'rivues', '/assets/images/templeticon/type_line_clustered_flat.gif', null, null, '8a8a8ae64627cd1901462827cee60615', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c56d3f60197', '三维折线图', '多个数据序列显示成已加深的彩色折线图。 此图表比较实际值。', 'chart', null, '2014-08-07 14:58:49', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_line_clustered.gif', null, null, '8a8a8ae64627cd1901462827cee60615', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c573f2601a1', '带标记的台阶折线图', '多个数据序列显示成彩色折线，其中，点与水平要素以及每个数据值处的标记连接起来。', 'chart', null, '2014-08-07 14:58:48', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_line_stepAtPoint_clustered_flat_markers.gif', null, null, '8a8a8ae64627cd1901462827cee60615', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c57911c01a9', '台阶折线图', '多个数据序列显示成彩色折线，其中，点与水平要素连接起来。', 'chart', null, '2014-08-07 14:58:48', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_line_stepAtPoint_clustered_flat.gif', null, null, '8a8a8ae64627cd1901462827cee60615', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c58020701b3', '带标记的堆积折线图', '多个数据序列在每个数据值处显示成带标记的彩色折线图。 顶部序列表示组合序列的总值。 此图表比较各个值占序列总值的比例。', 'chart', null, '2014-08-07 14:58:47', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_line_stacked_flat_markers.gif', null, null, '8a8a8ae64627cd1901462827cee60615', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c58708201bd', '堆积折线图', '多个数据序列显示成彩色折线图。 顶部序列表示组合序列的总值。 此图表比较各个值占序列总值的比例。', 'chart', null, '2014-08-07 14:58:47', null, '<script language=\"javascript\">\r\n	$(function () {\r\n		var xAxis_${dt.id!\'\'} = {\r\n			categories:[<#if pkg?? && dt.chart?? && dt.chart.series??><#list dt.chart.series as series><#if series_index ==0 ><#if reportModel.reportData?? && reportModel.reportData.data??><#list series.values(reportModel.reportData) as value><#if value_index gt 0>,</#if>\'<#if value.row?? && value.row.parent??>${value.row.parent.name!\'\'}</#if>\'</#list></#if></#if></#list></#if>]\r\n		    ,labels: {\r\n				rotation: -45,\r\n				style: {\r\n					fontSize: \'13px\',\r\n					fontFamily: \'Verdana, sans-serif\'\r\n				}\r\n			}\r\n		};\r\n		var series_${dt.id!\'\'} = [\r\n			<#if pkg?? && dt.chart?? && dt.chart.series??>\r\n			<#list dt.chart.series as series>\r\n			<#if series_index gt 0>,</#if>{\r\n				name: \'${series.name!\"\"}\',\r\n				data: [\r\n				    <#if reportModel.reportData?? && reportModel.reportData.data??><#list series.values(reportModel.reportData) as value><#if value_index gt 0>,</#if>${value.value!\'0\'}</#list></#if>\r\n				]\r\n			}\r\n			</#list>\r\n			</#if>\r\n		];\r\n		ChartAction.areaChart(\'#chart_${dt.id!\'\'}\', xAxis_${dt.id!\'\'} , \'<#if reportModel.displaytitle == true>${reportModel.title!report.name!\"\"}</#if>\' , series_${dt.id!\'\'});\r\n	});\r\n</script>', 'chart', 'rivues', '/assets/images/templeticon/type_line_stacked_flat.gif', null, null, '8a8a8ae64627cd1901462827cee60615', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c58c72901c5', '三维堆积折线图', '多个数据序列显示成已加深的彩色折线图。 顶部序列表示组合序列的总值。 此图表比较各个值占序列总值的比例。', 'chart', null, '2014-08-07 14:58:46', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_line_stacked.gif', null, null, '8a8a8ae64627cd1901462827cee60615', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5936fb01cf', '带标记的百分堆积折线图', '多个数据序列显示成彩色堆积折线图，且每个数据值处均带有标记。 折线图的组合值加在一起为 100%。 此图表用于比较各个值占总值的百分比。', 'chart', null, '2014-08-07 14:58:46', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_line_percent_flat_markers.gif', null, null, '8a8a8ae64627cd1901462827cee60615', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c59adfe01d9', '百分堆积折线图', '多个数据系列显示成彩色堆积折线图，其值之和为 100%。 此图表比较各个值占总值的百分比。', 'chart', null, '2014-08-07 14:58:45', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_line_percent_flat.gif', null, null, '8a8a8ae64627cd1901462827cee60615', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5a063b01e1', '三维百分堆积折线图', '多个数据序列显示成已加深的彩色堆积折线图，其值之和为 100%。 此图表比较各个值占总值的百分比。', 'chart', null, '2014-08-07 14:58:45', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_line_percent.gif', null, null, '8a8a8ae64627cd1901462827cee60615', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5a601d01ea', '三维轴折线图', '具有三个轴的三维折线图，它沿着第三个轴均匀地分布数据序列。此图表根据一个或多个度量以三维方式绘出多个变量。', 'chart', null, '2014-08-07 14:58:44', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_line_3daxis.gif', null, null, '8a8a8ae64627cd1901462827cee60615', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5ad39701f5', '饼形图', '使用扇区绘出数据序列的一个度量。 每个扇区的大小与总值成正比。 此图表比较各个值占总值的百分比。', 'chart', null, '2014-08-07 14:58:40', null, '<script language=\"javascript\">\r\n	$(function () {\r\n		var series_${dt.id!\'\'} = [\r\n			<#if pkg?? && dt.chart?? && dt.chart.series??>\r\n			<#list dt.chart.series as series>			\r\n			<#if series_index == 0>\r\n			{\r\n				type: \'pie\',\r\n				name: \'${series.name!\"\"}\',\r\n				data: [\r\n					<#if reportModel.reportData?? && reportModel.reportData.data??>\r\n					<#list series.values(reportModel.reportData) as value>\r\n						<#if value_index gt 0>,</#if>[\'<#if value.row?? && value.row.parent??>${value.row.parent.name!\'\'}</#if>\', ${value.value!\'0\'}]\r\n					</#list>\r\n					</#if>\r\n				]\r\n			}\r\n			</#if>\r\n			</#list>\r\n			</#if>\r\n		]\r\n		ChartAction.pieChart(\'#chart_${dt.id!\'\'}\' , \'<#if reportModel.displaytitle == true>${reportModel.title!report.name!\"\"}</#if>\' , true , series_${dt.id!\'\'} , false);\r\n	});\r\n</script>', 'chart', 'rivues', '/assets/images/templeticon/type_pie_flat.gif', null, null, '8a8a8ae64627cd190146282897ba0624', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5b35a301fe', '圆环图', '中心空白的饼形图。 每个扇区的大小与总值成正比。 此图表比较各个值占总值的百分比。', 'chart', null, '2014-08-07 14:58:39', null, '<script language=\"javascript\">\r\n	$(function () {\r\n		var series_${dt.id!\'\'} = [\r\n			<#if pkg?? && dt.chart?? && dt.chart.series??>\r\n			<#list dt.chart.series as series>			\r\n			<#if series_index == 0>\r\n			{\r\n				type: \'pie\',\r\n				name: \'${series.name!\"\"}\',\r\n				innerSize: \'40%\',\r\n				data: [\r\n					<#if reportModel.reportData?? && reportModel.reportData.data??>\r\n					<#list series.values(reportModel.reportData) as value>\r\n						<#if value_index gt 0>,</#if>[\'<#if value.row?? && value.row.parent??>${value.row.parent.name!\'\'}</#if>\', ${value.value!\'0\'}]\r\n					</#list>\r\n					</#if>\r\n				]\r\n			}\r\n			</#if>\r\n			</#list>\r\n			</#if>\r\n		]\r\n		ChartAction.pieChart(\'#chart_${dt.id!\'\'}\' , \'<#if reportModel.displaytitle == true>${reportModel.title!report.name!\"\"}</#if>\' , true , series_${dt.id!\'\'} , false);\r\n	});\r\n</script>', 'chart', 'rivues', '/assets/images/templeticon/type_pie_flat_hole.gif', null, null, '8a8a8ae64627cd190146282897ba0624', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5b89db0206', '三维饼形图', '使用已加深的扇区绘出数据序列的一个度量。 每个扇区的大小与总值成正比。 此图表比较各个值占总值的百分比。', 'chart', null, '2014-08-07 14:58:38', null, '<script language=\"javascript\">\r\n	$(function () {\r\n		var series_${dt.id!\'\'} = [\r\n			<#if pkg?? && dt.chart?? && dt.chart.series??>\r\n			<#list dt.chart.series as series>			\r\n			<#if series_index == 0>\r\n			{\r\n				type: \'pie\',\r\n				name: \'${series.name!\"\"}\',\r\n				data: [\r\n					<#if reportModel.reportData?? && reportModel.reportData.data??>\r\n					<#list series.values(reportModel.reportData) as value>\r\n						<#if value_index gt 0>,</#if>[\'<#if value.row?? && value.row.parent??>${value.row.parent.name!\'\'}</#if>\', ${value.value!\'0\'}]\r\n					</#list>\r\n					</#if>\r\n				]\r\n			}\r\n			</#if>\r\n			</#list>\r\n			</#if>\r\n		]\r\n		ChartAction.pieChart(\'#chart_${dt.id!\'\'}\' , \'<#if reportModel.displaytitle == true>${reportModel.title!report.name!\"\"}</#if>\' , true , series_${dt.id!\'\'} , true);\r\n	});\r\n</script>', 'chart', 'rivues', '/assets/images/templeticon/type_pie.gif', null, null, '8a8a8ae64627cd190146282897ba0624', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5bf79a0210', '三维圆环图', '已加深且中心空白的饼形图。 每个扇区的大小与总值成正比。 此图表比较各个值占总值的百分比。', 'chart', null, '2014-08-07 14:58:38', null, '<script language=\"javascript\">\r\n	$(function () {\r\n		var series_${dt.id!\'\'} = [\r\n			<#if pkg?? && dt.chart?? && dt.chart.series??>\r\n			<#list dt.chart.series as series>			\r\n			<#if series_index == 0>\r\n			{\r\n				type: \'pie\',\r\n				name: \'${series.name!\"\"}\',\r\n				innerSize: \'40%\',\r\n				data: [\r\n					<#if reportModel.reportData?? && reportModel.reportData.data??>\r\n					<#list series.values(reportModel.reportData) as value>\r\n						<#if value_index gt 0>,</#if>[\'<#if value.row?? && value.row.parent??>${value.row.parent.name!\'\'}</#if>\', ${value.value!\'0\'}]\r\n					</#list>\r\n					</#if>\r\n				]\r\n			}\r\n			</#if>\r\n			</#list>\r\n			</#if>\r\n		]\r\n		ChartAction.pieChart(\'#chart_${dt.id!\'\'}\' , \'<#if reportModel.displaytitle == true>${reportModel.title!report.name!\"\"}</#if>\' , true , series_${dt.id!\'\'} , true);\r\n	});\r\n</script>', 'chart', 'rivues', '/assets/images/templeticon/type_pie_hole.gif', null, null, '8a8a8ae64627cd190146282897ba0624', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5c514e0219', '面积图', '多个数据序列显示成彩色面积图。 此图表比较实际值。', 'chart', null, '2014-08-07 14:58:37', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_area_clustered_flat.gif', null, null, '8a8a8ae64627cd190146282985d20637', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5c96780221', '三维面积图', '多个数据序列显示成已加深的彩色面积图。 此图表比较实际值。', 'chart', null, '2014-08-07 14:58:36', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_area_clustered.gif', null, null, '8a8a8ae64627cd190146282985d20637', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5ce85e0229', '堆积面积图', '多个数据序列显示成彩色堆积面积图。 每个面积图的顶部表示上部面积图的基线。 上部面积图的顶部表示组合数据序列的总值。 此图表比较各个值占序列总值的比例。', 'chart', null, '2014-08-07 14:58:37', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_area_stacked_flat.gif', null, null, '8a8a8ae64627cd190146282985d20637', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5d43970232', '三维堆积面积图', '多个数据序列显示成彩色堆积面积图。 每个面积图的顶部表示上部面积图的基线。 上部面积图的顶部表示组合数据序列的总值。 此图表比较各个值占序列总值的比例。', 'chart', null, '2014-08-07 14:58:35', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_area_stacked.gif', null, null, '8a8a8ae64627cd190146282985d20637', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5d96b6023a', '百分堆积面积图', '多个数据序列显示成彩色堆积面积图，其值之和为 100%。 每个数据序列表示占总值的百分比。 此图表比较各个值占总值的百分比。', 'chart', null, '2014-08-07 14:58:35', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_area_percent_flat.gif', null, null, '8a8a8ae64627cd190146282985d20637', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5df4dc0243', '三维百分堆积面积图', '多个数据序列显示成已加深的彩色堆积面积图，其值之和为 100%。 每个数据序列表示占总值的百分比。 此图表比较各个值占总值的百分比。', 'chart', null, '2014-08-07 14:58:32', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_area_percent.gif', null, null, '8a8a8ae64627cd190146282985d20637', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5e48a6024b', '三维轴面积图', '具有三个轴的三维面积图，它沿着第三个轴均匀地分布数据序列。此图表根据一个或多个度量以三维方式绘出多个变量。', 'chart', null, '2014-08-07 14:58:31', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_area_3daxis.gif', null, null, '8a8a8ae64627cd190146282985d20637', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5ea5500255', '组合图', '组合图使用两个或多个面积图、柱形图或折线图绘出多个度量。', 'chart', null, '2014-08-07 14:58:31', null, '<script language=\"javascript\">\r\n	$(function() {\r\n	    $(\'#chart_${dt.id!\'\'}\').highcharts({\r\n			chart: {\r\n				zoomType: \'xy\'\r\n			},\r\n			title: {\r\n				text: \'<#if reportModel.displaytitle == true>${reportModel.title!report.name!\"\"}</#if>\'\r\n			},\r\n			xAxis: {\r\n				type: \'category\',\r\n				labels: {\r\n					rotation: -45,\r\n					style: {\r\n						fontSize: \'13px\',\r\n						fontFamily: \'Verdana, sans-serif\'\r\n					}\r\n				}\r\n			},\r\n			yAxis: [\r\n				<#if pkg?? && dt.chart?? && dt.chart.series??>\r\n				<#list dt.chart.series as series>\r\n				<#if series_index gt 0>,</#if>	{ \r\n					labels: {\r\n						format: \'{value}\',\r\n						style: {\r\n							color: Highcharts.getOptions().colors[${series_index}]\r\n						}\r\n					},\r\n					title: {\r\n						text: \'${series.name!\"\"}\',\r\n						style: {\r\n							color: Highcharts.getOptions().colors[${series_index}]\r\n						}\r\n					}\r\n					<#if series_index gt 0>,opposite: true</#if>\r\n				}\r\n				</#list>\r\n				</#if>\r\n			],\r\n			tooltip: {\r\n				shared: true\r\n			},\r\n			exporting: {\r\n				enabled: false\r\n			},\r\n			credits:{\r\n				enabled:false\r\n			},\r\n			legend: {\r\n				layout: \'vertical\',\r\n				align: \'left\',\r\n				x: 50,\r\n				verticalAlign: \'top\',\r\n				y: 10,\r\n				floating: true,\r\n				backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || \'#FFFFFF\'\r\n			},\r\n			series: [\r\n				<#if pkg?? && dt.chart?? && dt.chart.series??>\r\n				<#list dt.chart.series as series>			\r\n				<#if series_index gt 0>,</#if>\r\n				{\r\n					name: \'${series.name!\"\"}\',\r\n					type: \'<#if series_index == 0>column<#else>spline</#if>\',\r\n					data: [\r\n						<#if reportModel.reportData?? && reportModel.reportData.data??>\r\n						<#list reportModel.reportData.data as data>\r\n						<#list data as value>\r\n							 <#if series.name == value.name>\r\n							 <#if data_index gt 0>,</#if>[\'<#if value.row?? && value.row.parent??>${value.row.parent.name!\'\'}</#if>\', ${value.value!\'0\'}]\r\n							 <#break/>\r\n							 </#if>\r\n						</#list>\r\n						</#list>\r\n						</#if>\r\n					]\r\n				}\r\n				</#list>\r\n				</#if>\r\n			]\r\n	    });\r\n	});\r\n</script>\r\n\r\n\r\n', 'chart', 'rivues', '/assets/images/templeticon/type_combo_column_area_line_clustered_flat.gif', null, null, '8a8a8ae64627cd1901462829b382063d', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5ee964025c', '三维组合图', '组合图使用两个或多个已加深的面积图、柱形图或折线图绘出多个度量。', 'chart', null, '2014-08-07 14:58:30', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_combo_column_area_line_clustered.gif', null, null, '8a8a8ae64627cd1901462829b382063d', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5f2f1d0264', '堆积组合图', '堆积组合图使用两个或多个堆积面积图、堆积柱形图或堆积折线图绘出多个度量。', 'chart', null, '2014-08-07 14:58:30', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_combo_column_area_line_stacked_flat.gif', null, null, '8a8a8ae64627cd1901462829b382063d', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5f8509026c', '三维堆积组合图', '堆积组合图使用两个或多个已加深的堆积面积图、堆积柱形图或堆积折线图绘出多个度量。', 'chart', null, '2014-08-07 14:58:29', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_combo_column_area_line_stacked.gif', null, null, '8a8a8ae64627cd1901462829b382063d', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c5fda400275', '三维轴组合图', '具有三个座标轴的三维图，它使用面积图、柱形图和折线图的任意组合画出多个度量。', 'chart', null, '2014-08-07 14:58:29', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_combo_3daxis.gif', null, null, '8a8a8ae64627cd1901462829b382063d', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c604c850281', '散点图', '散点图将多个数据序列的两个度量分别标绘成 X 值和 Y 值。', 'chart', null, '2014-08-07 14:58:28', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_scatter.gif', null, null, '8a8a8ae64627cd190146282ac64d0650', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c609a250289', '泡形图', '泡形图将多个数据序列的三个度量分别标绘成 X 值、Y 值和表示第三个度量的相对大小的气泡。最小的气泡分配给最小的数据值。', 'chart', null, '2014-08-07 14:58:28', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_bubble.gif', null, null, '8a8a8ae64627cd190146282ac64d0650', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c612d900294', '气泡随Excel气泡调整大小', '泡形图将多个数据序列的三个度量分别标绘成 X 值、Y 值和表示第三个度量的相对大小的气泡。最小的气泡对应 0。', 'chart', null, '2014-08-07 14:58:27', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_bubble_zeroBased.gif', null, null, '8a8a8ae64627cd190146282ac64d0650', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c61a039029e', '象限图', '象限图将三个度量分别标绘成 X 值、Y 值和表示相对大小的气泡。 象限图分成四个相等的区域。 X 轴和 Y 轴的原点在中间。', 'chart', null, '2014-08-07 14:58:26', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_quadrant.gif', null, null, '8a8a8ae64627cd190146282ac64d0650', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c620c9202a8', '点状图', '点状图用于绘出一个或多个数据序列的一个度量。', 'chart', null, '2014-08-07 14:58:26', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_point_clustered.gif', null, null, '8a8a8ae64627cd190146282ac64d0650', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c627c3502b1', '带变化标记的散点图', '散点图用于绘出一个或多个数据序列的一个度量。 数据序列以不同的点形状表示。', 'chart', null, '2014-08-07 14:58:25', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_point_clustered_markers.gif', null, null, '8a8a8ae64627cd190146282ac64d0650', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c62d35902ba', '三维散点图', '用于绘出多个数据序列的三个度量的三轴图。', 'chart', null, '2014-08-07 14:58:24', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_scatter_3daxis.gif', null, null, '8a8a8ae64627cd190146282ac64d0650', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c66fcf602f5', '带标记的雷达图', '雷达图是用于绘出每个轴以及一个或多个数据序列的一个度量的圆形图。 每个值处的标记均通过连线与相邻的轴相连。', 'chart', null, '2014-08-07 14:58:25', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_radar_markers.gif', null, null, '8a8a8ae64627cd190146282bab790660', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c67584e02fe', '雷达图', '雷达图将多个轴组合成一个放射状的图形。 每个轴代表不同的变量。 数据序列画在每个轴上，并通过连线与相邻的轴相连。', 'chart', null, '2014-08-07 14:58:24', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_radar.gif', null, null, '8a8a8ae64627cd190146282bab790660', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c679f3d0306', '面积雷达图', '面积雷达图将多个轴组合成一个放射状的图形。 每个轴代表不同的变量。 数据序列画在每个轴上，并通过直线与相邻的轴相连。 由此形成的多边形使用表示各个数据序列的不同颜色来填充。', 'chart', null, '2014-08-07 14:58:23', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_radar_area.gif', null, null, '8a8a8ae64627cd190146282bab790660', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c67f3e1030e', '堆积面积雷达图', '堆积雷达图将多个轴组合成一个放射状的图形。 每个轴代表不同的变量。 数据序列堆积在一起，即每个轴上的数据序列使用前一序列的标记作为基线。', 'chart', null, '2014-08-07 14:58:22', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_radar_stacked.gif', null, null, '8a8a8ae64627cd190146282bab790660', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c68a120031b', '带变化标记的极坐标图', '用于画出多个数据序列的两个度量的圆形图。 圆的半径决定一个度量的大小刻度。 值在圆心角的位置决定第二个度量的大小刻度。 标记颜色和形状各不相同。', 'chart', null, '2014-08-07 14:58:23', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_polar_categorymarkers.gif', null, null, '8a8a8ae64627cd190146282bab790660', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c691aa20327', '仪表盘', '仪表板图将一个度量绘成表盘，并将数据序列绘成表盘上的指针。', 'chart', null, '2014-08-07 14:58:22', null, '<script language=\"javascript\">\r\n	$(function () {\r\n		var xAxis_${dt.id!\'\'} = {\r\n			categories:[<#if pkg?? && dt.chart?? && dt.chart.series??><#list dt.chart.series as series><#if series_index ==0 ><#if reportModel.reportData?? && reportModel.reportData.data??><#list series.values(reportModel.reportData) as value><#if value_index gt 0>,</#if>\'<#if value.row?? && value.row.parent??>${value.row.parent.name!\'\'}</#if>\'</#list></#if></#if></#list></#if>]\r\n			,labels: {\r\n				rotation: -45,\r\n				style: {\r\n					fontSize: \'13px\',\r\n					fontFamily: \'Verdana, sans-serif\'\r\n				}\r\n			}\r\n		};\r\n		var series_${dt.id!\'\'} = [\r\n			<#if pkg?? && dt.chart?? && dt.chart.series??>\r\n			<#list dt.chart.series as series>\r\n			<#if series_index gt 0>,</#if>{\r\n				name: \'${series.name!\"\"}\',\r\n				data: [\r\n				    <#if reportModel.reportData?? && reportModel.reportData.data??><#list series.values(reportModel.reportData) as value><#if value_index == 0>${value.value!\'0\'}</#if></#list></#if>\r\n				]\r\n			}\r\n			</#list>\r\n			</#if>\r\n		];\r\n		ChartAction.gaugeChart(\'#chart_${dt.id!\'\'}\', \'${pkg.id!\"\"}\' , \'<#if reportModel.displaytitle == true>${reportModel.title!report.name!\"\"}</#if>\' , series_${dt.id!\'\'} , {\'title\':\'积分\',\'unit\':\'分\',\'min\':1,\'max\':100});\r\n	});\r\n</script>', 'chart', 'rivues', '/assets/images/templeticon/type_gauge_dial.gif', null, null, '8a8a8ae64627cd190146282beb970667', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c69e2210336', '折线图', '一种 Microchart 图，在该图中数据显示成折线。', 'chart', null, '2014-08-07 14:58:21', null, '<script language=\"javascript\">\r\n	$(function () {\r\n		var xAxis_${dt.id!\'\'} = {\r\n			categories:[<#if pkg?? && dt.chart?? && dt.chart.series??><#list dt.chart.series as series><#if series_index ==0 ><#if reportModel.reportData?? && reportModel.reportData.data??><#list series.values(reportModel.reportData) as value><#if value_index gt 0>,</#if>\'<#if value.row?? && value.row.parent??>${value.row.parent.name!\'\'}</#if>\'</#list></#if></#if></#list></#if>]\r\n			,labels: {\r\n				rotation: -45,\r\n				style: {\r\n					fontSize: \'13px\',\r\n					fontFamily: \'Verdana, sans-serif\'\r\n				}\r\n			}\r\n		};\r\n		var series_${dt.id!\'\'} = [\r\n			<#if pkg?? && dt.chart?? && dt.chart.series??>\r\n			<#list dt.chart.series as series>\r\n			<#if series_index gt 0>,</#if>{\r\n				name: \'${series.name!\"\"}\',\r\n				data: [\r\n				    <#if reportModel.reportData?? && reportModel.reportData.data??><#list series.values(reportModel.reportData) as value><#if value_index gt 0>,</#if>${value.value!\'0\'}</#list></#if>\r\n				]\r\n			}\r\n			</#list>\r\n			</#if>\r\n		];\r\n		ChartAction.lineMicroChart(\'#chart_${dt.id!\'\'}\', xAxis_${dt.id!\'\'} , \'<#if reportModel.displaytitle == true>${reportModel.title!report.name!\"\"}</#if>\' , series_${dt.id!\'\'});\r\n	});\r\n</script>', 'chart', 'rivues', '/assets/images/templeticon/type_micro_line.gif', null, null, '8a8a8ae64627cd190146282ceaa6067c', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c6a76310342', '具有结束值标记的折线图', '一种 Microchart 图，在该图中，数据显示成具有表示结束值的标记的折线。', 'chart', null, '2014-08-07 14:58:21', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_micro_line_close.gif', null, null, '8a8a8ae64627cd190146282ceaa6067c', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c6b1d2e034e', '具有开始、结束、最小值和最大值标记的折线图', '一种 Microchart 图，在该图中，数据显示成具有表示开始值和结束值以及最高值和最低值的标记的折线。', 'chart', null, '2014-08-07 14:58:20', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_micro_line_opencloseminmax.gif', null, null, '8a8a8ae64627cd190146282ceaa6067c', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c6b7c4d0357', '具有参考线的折线图', '一种 Microchart 图，在该图中，数据显示成具有基线 (将从该基线比较、引用或度量值) 的折线。', 'chart', null, '2014-08-07 14:58:19', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_micro_line_baseline.gif', null, null, '8a8a8ae64627cd190146282ceaa6067c', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c6bcd0b035f', '柱形图', '一种 Microchart 图，在该图中数据显示成柱。每个柱的顶部都表示数据值', 'chart', null, '2014-08-07 14:58:19', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_micro_column.gif', null, null, '8a8a8ae64627cd190146282ceaa6067c', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c6c465f036a', '堆积柱形图', '一种 Microchart 图，在该图中，数据序列在柱内显示成彩色区域。每个柱的顶部表示序列合计。此图表用于比较各个值占序列合计的比例。', 'chart', null, '2014-08-07 14:58:18', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_micro_column_stacked.gif', null, null, '8a8a8ae64627cd190146282ceaa6067c', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c6ca1e60372', '百分堆积柱形图', '一种 Microchart 图，在该图中，数据序列在柱内显示成彩色区域。每个柱都表示百分之百的序列合计。此图表用于比较各个值占合计的百分比。', 'chart', null, '2014-08-07 14:58:18', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_micro_column_stacked_percent.gif', null, null, '8a8a8ae64627cd190146282ceaa6067c', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c6cebe1037a', '条形图', '一种 Microchart 图，在该图中数据显示成条。每个条的末端都表示数据值。', 'chart', null, '2014-08-07 14:58:17', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_micro_bar.gif', null, null, '8a8a8ae64627cd190146282ceaa6067c', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c6d43910383', '堆积条形图', '一种 Microchart 图，在该图中，数据序列在条内显示成彩色区域。每个条的末端表示序列合计。此图表用于比较各个值占序列合计的比例。', 'chart', null, '2014-08-07 14:58:17', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_micro_bar_stacked.gif', null, null, '8a8a8ae64627cd190146282ceaa6067c', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c6d9dc1038b', '百分堆积条形图', '一种 Microchart 图，在该图中，数据序列在条内显示成彩色区域。每个条表示百分之百的序列合计。此图表用于比较各个值占合计的百分比。', 'chart', null, '2014-08-07 14:58:17', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_micro_bar_stacked_percent.gif', null, null, '8a8a8ae64627cd190146282ceaa6067c', null);
INSERT INTO `rivu5_searchresulttemplet` VALUES ('8a8a8ae9462c412c01462c6df18b0393', '盈亏', '一种 Microchart 图，在该图中，每个列的值要么是 1 要么是 -1，通常表示赢利或亏损。', 'chart', null, '2014-08-07 14:58:16', null, null, 'chart', 'rivues', '/assets/images/templeticon/type_micro_win_loss.gif', null, null, '8a8a8ae64627cd190146282ceaa6067c', null);



-- ----------------------------
-- Records of rivu5_typecategory
-- ----------------------------
INSERT INTO `rivu5_typecategory` VALUES ('297e1e8750feef7d0150ff22b7e502b1', '期货数据', 'pub', null, 'rivues', 'cube', '0', '', null, null, null, null, '4028d481428add1801428ae6be120003', '2015-11-13 12:37:56', null);
INSERT INTO `rivu5_typecategory` VALUES ('297e8c7b4518175d0145181d45c4017e', '话务', 'pub', 'RIVU_1396268942788', 'rivues', null, null, null, null, null, null, null, '402881f044aa0c6d0144aade76c80199', '2014-03-31 20:29:02', null);
INSERT INTO `rivu5_typecategory` VALUES ('297e8c7b456159a90145615ffa5e0191', 'AAATEST', 'pub', 'RIVU_1397498051164', 'rivues', null, null, null, null, null, null, null, '402881f044aa0c6d0144aade76c80199', '2014-04-15 01:54:11', null);
INSERT INTO `rivu5_typecategory` VALUES ('297e8c7b456e5a7301456e5e100003ab', '运营监控模型', 'pub', null, 'rivues', 'cube', '0', '', null, null, null, null, '402881f044aa0c6d0144aade76c80199', '2014-04-17 14:27:09', null);
INSERT INTO `rivu5_typecategory` VALUES ('297e8c7b47f8b4030147f8eb7b6a0427', '组件属性', 'pub', null, 'rivues', 'properties', null, '组件属性', null, '/assets/images/design/table.png', null, null, '4028d481428add1801428ae6be120003', '2014-08-21 22:14:47', '2014-08-21 22:15:38');
INSERT INTO `rivu5_typecategory` VALUES ('297e8c7b47f8b4030147f8ebd6e50432', '图表属性', 'pub', null, 'rivues', 'properties', null, '图表属性', null, '/assets/images/design/table.png', null, null, '4028d481428add1801428ae6be120003', '2014-08-21 22:15:11', '2014-08-21 22:15:50');
INSERT INTO `rivu5_typecategory` VALUES ('402881e74874c22b014874df8e6d01fa', '地图', 'pub', null, 'rivues', 'chart', null, '', null, '/assets/images/templeticon/map.png', null, null, '4028d481428add1801428ae6be120003', '2014-09-14 23:54:41', '2014-09-14 23:55:33');
INSERT INTO `rivu5_typecategory` VALUES ('4028ab4e3ba6b29d013ba6b3ca200056', '投诉处理', 'pub', null, 'rivues', 'cube', '0', null, null, '', '', 'cube', null, null, null);
INSERT INTO `rivu5_typecategory` VALUES ('4028ce9a4893cc42014893fbebd30400', '漏斗图', 'pub', null, 'rivues', 'chart', null, '漏斗图', null, '/assets/images/templeticon/funnel.png', null, null, '4028d481428add1801428ae6be120003', '2014-09-21 00:53:53', '2014-09-21 00:56:18');
INSERT INTO `rivu5_typecategory` VALUES ('8a8a8ae64627cd190146282577df05e4', '柱形图', 'pub', null, 'rivues', 'chart', null, '柱形图', null, '/assets/images/templeticon/imgIco-1.png', null, null, '402881f044aa0c6d0144aade76c80199', '2014-05-23 16:14:42', '2014-05-23 17:34:15');
INSERT INTO `rivu5_typecategory` VALUES ('8a8a8ae64627cd190146282604c805f0', '条形图', 'pub', null, 'rivues', 'chart', null, '条形图', null, '/assets/images/templeticon/imgIco-2.png', null, null, '402881f044aa0c6d0144aade76c80199', '2014-05-23 16:15:18', '2014-05-23 17:34:32');
INSERT INTO `rivu5_typecategory` VALUES ('8a8a8ae64627cd19014628271dce0603', '渐进', 'pub', null, 'rivues', 'chart', null, '渐进', null, '/assets/images/templeticon/imgIco-3.png', null, null, '402881f044aa0c6d0144aade76c80199', '2014-05-23 16:16:30', '2014-05-23 17:34:47');
INSERT INTO `rivu5_typecategory` VALUES ('8a8a8ae64627cd19014628278c74060d', '排列图', 'pub', null, 'rivues', 'chart', null, '排列图', null, '/assets/images/templeticon/imgIco-4.png', null, null, '402881f044aa0c6d0144aade76c80199', '2014-05-23 16:16:58', '2014-05-23 17:35:07');
INSERT INTO `rivu5_typecategory` VALUES ('8a8a8ae64627cd1901462827cee60615', '折线图', 'pub', null, 'rivues', 'chart', null, '折线图', null, '/assets/images/templeticon/imgIco-5.png', null, null, '402881f044aa0c6d0144aade76c80199', '2014-05-23 16:17:16', '2014-05-23 17:35:46');
INSERT INTO `rivu5_typecategory` VALUES ('8a8a8ae64627cd19014628281dce0603', '报表组件', 'pub', null, 'rivues', 'reportmodule', null, '报表组件', null, '/assets/images/templeticon/imgIco-3.png', null, null, '402881f044aa0c6d0144aade76c80199', '2014-05-23 16:16:30', '2014-05-23 17:34:47');
INSERT INTO `rivu5_typecategory` VALUES ('8a8a8ae64627cd190146282897ba0624', '饼形图、圆环图', 'pub', null, 'rivues', 'chart', null, '饼形图、圆环图', null, '/assets/images/templeticon/imgIco-6.png', null, null, '402881f044aa0c6d0144aade76c80199', '2014-05-23 16:18:07', '2014-05-23 17:35:59');
INSERT INTO `rivu5_typecategory` VALUES ('8a8a8ae64627cd190146282985d20637', '面积图', 'pub', null, 'rivues', 'chart', null, '面积图', null, '/assets/images/templeticon/imgIco-7.png', null, null, '402881f044aa0c6d0144aade76c80199', '2014-05-23 16:19:08', '2014-05-23 17:36:15');
INSERT INTO `rivu5_typecategory` VALUES ('8a8a8ae64627cd1901462829b382063d', '组合图', 'pub', null, 'rivues', 'chart', null, '组合图', null, '/assets/images/templeticon/imgIco-8.png', null, null, '402881f044aa0c6d0144aade76c80199', '2014-05-23 16:19:20', '2014-05-23 17:36:37');
INSERT INTO `rivu5_typecategory` VALUES ('8a8a8ae64627cd190146282ac64d0650', '散点图、泡形图、点状图', 'pub', null, 'rivues', 'chart', null, '散点图、泡形图、点状图', null, '/assets/images/templeticon/imgIco-9.png', null, null, '402881f044aa0c6d0144aade76c80199', '2014-05-23 16:20:30', '2014-05-23 17:36:55');
INSERT INTO `rivu5_typecategory` VALUES ('8a8a8ae64627cd190146282bab790660', '雷达图、极座标图', 'pub', null, 'rivues', 'chart', null, '雷达图、极座标图', null, '/assets/images/templeticon/imgIco-10.png', null, null, '402881f044aa0c6d0144aade76c80199', '2014-05-23 16:21:29', '2014-05-23 17:37:10');
INSERT INTO `rivu5_typecategory` VALUES ('8a8a8ae64627cd190146282beb970667', '仪表板', 'pub', null, 'rivues', 'chart', null, '仪表板', null, '/assets/images/templeticon/imgIco-11.png', null, null, '402881f044aa0c6d0144aade76c80199', '2014-05-23 16:21:45', '2014-05-23 17:37:22');
INSERT INTO `rivu5_typecategory` VALUES ('8a8a8ae64627cd190146282ceaa6067c', 'Microchart图', 'pub', null, 'rivues', 'chart', null, 'Microchart图', null, '/assets/images/templeticon/imgIco-13.png', null, null, '402881f044aa0c6d0144aade76c80199', '2014-05-23 16:22:50', '2014-05-23 17:37:43');
INSERT INTO `rivu5_typecategory` VALUES ('8a8a8ae74627463f0146274f06910061', '运行报表', 'pub', null, 'rivues', 'task', '0', null, null, null, null, 'report', null, null, null);
INSERT INTO `rivu5_typecategory` VALUES ('8a8a8ae74627463f01462795b65c058c', '本地执行', 'pub', null, 'rivues', 'task', '0', null, null, null, null, 'process', null, null, null);
INSERT INTO `rivu5_typecategory` VALUES ('8a8a8ae74627463f0146279d3c38047f', '结算模型', 'pub', null, 'rivues', 'task', '0', null, null, null, null, 'table', null, null, null);
INSERT INTO `rivu5_typecategory` VALUES ('8a8a8ae74627463f0146279e2176048a', '数据迁移', 'pub', null, 'rivues', 'task', '0', null, null, null, null, 'dataex', null, null, null);
INSERT INTO `rivu5_typecategory` VALUES ('8a8a8ae74627463f0146279e5e360490', '事件任务', 'pub', null, 'rivues', 'task', '0', null, null, null, null, 'event', null, null, null);
